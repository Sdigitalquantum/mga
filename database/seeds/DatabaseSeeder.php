<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' 		=> 'PT. Starindo Anugerah Abadi',
            'email' 	=> 'saa@pt-saa.co.id',
            'password' 	=> '$2y$10$YJLT2jS.eN5bGMY4sOYAkO5Dm0AK.P7QnEv5aqIhC3ByQofAqNe2i',
        ]);
    }
}
