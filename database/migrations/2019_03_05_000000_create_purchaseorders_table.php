<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchaseorders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('no_inc');
            $table->string('no_po');
            $table->date('date_order');
            $table->tinyInteger('tax');
            $table->text('notice')->nullable();
            $table->text('reason')->nullable();
            $table->tinyInteger('approve')->default('1');
            $table->tinyInteger('status_approve')->default('0');
            $table->tinyInteger('status_release')->default('0');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->timestamp('approved_at')->nullable();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->integer('approved_user')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchaseorders');
    }
}
