<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasedetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchasedetails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchaseorder')->unsigned();
            $table->integer('product')->unsigned();
            $table->integer('supplierdetail')->unsigned();
            $table->integer('warehouse')->unsigned();
            $table->integer('qty_order');
            $table->integer('qty_buffer');
            $table->integer('price');
            $table->integer('qty_qc')->default('0');
            $table->tinyInteger('status_qc')->default('0');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('purchaseorder')->references('id')->on('purchaseorders');
            $table->foreign('product')->references('id')->on('products');
            $table->foreign('supplierdetail')->references('id')->on('supplierdetails');
            $table->foreign('warehouse')->references('id')->on('warehouses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchasedetails');
    }
}
