<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKonversisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('konversis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product')->unsigned();
            $table->integer('unit_src')->unsigned();
            $table->integer('unit_dst')->unsigned();
            $table->integer('qty_result');
            $table->string('keterangan');
            $table->string('keterangan1');
            $table->string('keterangan2');
            $table->integer('qty1');
            $table->integer('qty2');
            $table->tinyInteger('general');
            $table->tinyInteger('status_group');
            $table->tinyInteger('status_record');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('unit_src')->references('id')->on('units');       
            $table->foreign('unit_dst')->references('id')->on('units');       
            $table->foreign('product')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('konversis');
    }
}
