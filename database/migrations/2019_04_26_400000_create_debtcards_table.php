<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDebtcardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debtcards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supplierdetail')->unsigned();
            $table->integer('year');
            $table->tinyInteger('month');
            $table->bigInteger('debit');
            $table->bigInteger('kredit');
            $table->timestamps();
            $table->foreign('supplierdetail')->references('id')->on('supplierdetails');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debtcards');
    }
}
