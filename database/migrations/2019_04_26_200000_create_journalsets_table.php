<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalsetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journalsets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code_journal');
            $table->string('name_journal');
            $table->string('code_accounting');
            $table->string('name_accounting');
            $table->string('no_is')->nullable();
            $table->string('no_qc')->nullable();
            $table->string('no_inv')->nullable();
            $table->bigInteger('price');
            $table->string('status');
            $table->timestamps();
            $table->integer('created_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journalsets');
    }
}
