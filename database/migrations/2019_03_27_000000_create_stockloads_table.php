<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stockloads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stuff')->unsigned();
            $table->integer('stockin')->unsigned();
            $table->integer('no_inc');
            $table->string('nomor');
            $table->integer('qty_load');
            $table->timestamps();
            $table->integer('created_user');
            $table->foreign('stuff')->references('id')->on('stuffs');
            $table->foreign('stockin')->references('id')->on('stockins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stockloads');
    }
}
