<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulebooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedulebooks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quotationsplit')->unsigned();
            $table->string('pol');
            $table->string('pod');
            $table->string('lines');
            $table->date('etd');
            $table->date('eta');
            $table->text('notice')->nullable();
            $table->text('reason')->nullable();
            $table->tinyInteger('status_approve')->default('0');
            $table->tinyInteger('status_stuff')->default('0');
            $table->tinyInteger('status_stuff_acc')->default('0');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('quotationsplit')->references('id')->on('quotationsplits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedulebooks');
    }
}
