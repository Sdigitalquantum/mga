<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationsplitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotationsplits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quotation')->unsigned();
            $table->integer('pol')->unsigned();
            $table->integer('pod')->unsigned();
            $table->integer('no_inc');
            $table->string('no_split');
            $table->date('etd');
            $table->date('eta');
            $table->integer('qty_print')->default('0');
            $table->integer('qty_invoice')->default('0');
            $table->tinyInteger('status_request')->default('0');
            $table->tinyInteger('status_load')->default('0');
            $table->tinyInteger('status_stuff')->default('0');
            $table->tinyInteger('status_delivery')->default('0');
            $table->tinyInteger('status_send')->default('0');
            $table->tinyInteger('status_pack')->default('0');
            $table->tinyInteger('status_document')->default('0');
            $table->tinyInteger('status_confirm')->default('0');
            $table->tinyInteger('status_reroute')->default('0');
            $table->tinyInteger('status_book')->default('0');
            $table->tinyInteger('status_journal')->default('0');
            $table->tinyInteger('status_release')->default('0');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->timestamp('sent_at')->nullable();
            $table->timestamp('packed_at')->nullable();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->integer('sent_user')->nullable();
            $table->integer('packed_user')->nullable();
            $table->foreign('quotation')->references('id')->on('quotations');
            $table->foreign('pol')->references('id')->on('ports');
            $table->foreign('pod')->references('id')->on('ports');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotationsplits');
    }
}
