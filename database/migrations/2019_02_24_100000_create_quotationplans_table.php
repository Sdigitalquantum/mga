<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationplansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotationplans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quotationsplit')->unsigned();
            $table->integer('plan')->unsigned();
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('quotationsplit')->references('id')->on('quotationsplits');
            $table->foreign('plan')->references('id')->on('plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotationplans');
    }
}
