<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('accounting')->unsigned();
            $table->integer('department')->unsigned();
            $table->integer('variant');
            $table->integer('merk');
            $table->integer('typesub');
            $table->integer('size');
            $table->integer('unit')->unsigned();
            $table->string('code')->unique();
            $table->string('name');
            $table->tinyInteger('general');
            $table->tinyInteger('status_group');
            $table->tinyInteger('status_record');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('accounting')->references('id')->on('accounts');
            $table->foreign('department')->references('id')->on('departments');
            $table->foreign('unit')->references('id')->on('units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
