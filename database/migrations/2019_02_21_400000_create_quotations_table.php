<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customerdetail')->unsigned();
            $table->integer('companyaccount')->unsigned();
            $table->integer('paymentterm')->unsigned();
            $table->integer('container')->unsigned();
            $table->integer('pol')->unsigned();
            $table->integer('pod')->unsigned();
            $table->integer('currency')->unsigned();
            $table->integer('sales')->unsigned();
            $table->tinyInteger('tax');
            $table->tinyInteger('status_flow');
            $table->tinyInteger('status_sp');
            $table->integer('no_inc');
            $table->string('no_sp');
            $table->date('date_sp');
            $table->string('no_poc')->nullable();
            $table->tinyInteger('disc_type');
            $table->string('disc_value')->nullable();
            $table->text('notice')->nullable();
            $table->date('etd');
            $table->date('eta');
            $table->integer('qty_gross');
            $table->integer('qty_cbm');
            $table->string('kurs')->nullable();
            $table->string('vessel')->nullable();
            $table->integer('payment_term')->default('0');
            $table->integer('qty_print')->default('0');
            $table->tinyInteger('qty_product')->default('0');
            $table->tinyInteger('qty_amount')->default('0');
            $table->tinyInteger('status_product')->default('0');
            $table->tinyInteger('status_close')->default('0');
            $table->tinyInteger('status_release')->default('0');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->foreign('customerdetail')->references('id')->on('customerdetails');
            $table->foreign('companyaccount')->references('id')->on('companyaccounts');
            $table->foreign('paymentterm')->references('id')->on('paymentterms');
            $table->foreign('container')->references('id')->on('containers');
            $table->foreign('pol')->references('id')->on('ports');
            $table->foreign('pod')->references('id')->on('ports');
            $table->foreign('currency')->references('id')->on('currencies');
            $table->foreign('sales')->references('id')->on('sales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotations');
    }
}
