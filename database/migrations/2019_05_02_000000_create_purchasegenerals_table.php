<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasegeneralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchasegenerals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('no_inc');
            $table->string('no_prg');
            $table->date('date_order');
            $table->text('notice')->nullable();
            $table->text('reason')->nullable();
            $table->tinyInteger('approve')->default('1');
            $table->tinyInteger('status_approve')->default('0');
            $table->tinyInteger('status_order')->default('0');
            $table->tinyInteger('status_release')->default('0');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
            $table->timestamp('approved_at')->nullable();
            $table->integer('created_user');
            $table->integer('updated_user');
            $table->integer('approved_user')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchasegenerals');
    }
}
