<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Material;
use App\Http\Models\Materialdetail;
use App\Http\Models\Materialother;
use App\Http\Models\Product;
use App\Http\Models\Company;
use App\Http\Models\Employeeset;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $materials = Material::orderBy('created_at','desc')->get();
        $materialdetails = Materialdetail::where('status', 1)->get();
        $materialothers = Materialother::where('status', 1)->get();

        return view('vendor/adminlte/material.index', compact('materials','materialdetails','materialothers','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $products = Product::where('status', 1)->orderBy('name')->get();

        return view('vendor/adminlte/material.create', compact('products','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $material = new Material([
            'product'       => $request->get('product'),
            'status_group'  => ($request->get('status_group') == 1) ? 1 : 0,
            'status_record' => ($request->get('status_record') == 1) ? 1 : 0,
            'created_user'  => $session_id,
            'updated_user'  => $session_id
        ]);
	    $material->save();

	    return redirect('/materialdetail/'.$material->id)->with('success', 'Bill of Material has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $material  = Material::find($id);
        $materialdetails = Materialdetail::where('material', $id)->get();
        $materialothers = Materialother::where('material', $id)->get();
        $products = Product::where('status', 1)->orderBy('name')->get();
        $company = Company::first();

        return view('vendor/adminlte/material.show', compact('id','material','materialdetails','materialothers','products','company','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $material  = Material::find($id);
        $products = Product::where('status', 1)->orderBy('name')->get();

        return view('vendor/adminlte/material.edit', compact('id','material','products','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        
        $material = Material::find($id);
        $material->product       = $request->get('product');
        $material->status_group  = ($request->get('status_group') == 1) ? 1 : 0;
        $material->status_record = ($request->get('status_record') == 1) ? 1 : 0;
	    $material->updated_user  = $session_id;
	    $material->save();

	    return redirect('/material')->with('success', 'Bill of Material has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $material = Material::find($id);
        $status = $material->status;

        if ($status == 1) {
        	$material->status       = 0;
            $material->updated_user = $session_id;
	        $material->save();

		    return redirect('/material')->with('error', 'Bill of Material has been deleted');
        } elseif ($status == 0) {
        	$material->status       = 1;
            $material->updated_user = $session_id;
	        $material->save();

		    return redirect('/material')->with('success', 'Bill of Material has been activated');
        }
    }
}
