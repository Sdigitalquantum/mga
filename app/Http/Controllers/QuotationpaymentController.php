<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Quotation;
use App\Http\Models\Quotationdetail;
use App\Http\Models\Quotationpayment;
use App\Http\Models\Payment;
use App\Http\Models\Employeeset;

class QuotationpaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Quotationpayment::where([['quotation', $request->get('quotation')], ['payment', $request->get('payment')]])->first();

        if (!empty($cek)) {
            return redirect()->back()->withErrors('Quotation Payment already exists');
        } else {
            $payment = Payment::where('id', $request->get('payment'))->first();
            $quotation = Quotation::where('id', $request->get('quotation'))->first();
            if ($quotation->payment_term+$payment->value > 100) {
                return redirect()->back()->withErrors('Quotation Payment greater than 100%');
            } else {
                $quotationpayment = new Quotationpayment([
                    'quotation'     => $request->get('quotation'),
                    'payment'       => $request->get('payment'),
                    'created_user'  => $session_id,
                    'updated_user'  => $session_id
                ]);
                $quotationpayment->save();

                $quotation->payment_term = $quotation->payment_term+$payment->value;
                $quotation->save();

                return redirect('/quotationpayment/'.$request->get('quotation'))->with('success', 'Quotation Payment has been added');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $quotation = Quotation::find($id);
        $quotationdetails = Quotationdetail::where([['quotation', $id], ['status', 1]])->orderBy('created_at','desc')->get();
        $quotationpayments = Quotationpayment::where('quotation', $id)->orderBy('created_at','desc')->get();
        $payments = Payment::where('status', 1)->get();

        return view('vendor/adminlte/quotationpayment.show', compact('quotation','quotationdetails','quotationpayments','payments','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $quotationpayment = Quotationpayment::find($id);
        $quotationdetails = Quotationdetail::where([['quotation', $quotationpayment->quotation], ['status', 1]])->orderBy('created_at','desc')->get();
        $quotationpayments = Quotationpayment::where('quotation', $quotationpayment->quotation)->orderBy('created_at','desc')->get();
        $payments = Payment::where('status', 1)->get();

        return view('vendor/adminlte/quotationpayment.edit', compact('quotationpayment','quotationdetails','quotationpayments','payments','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $quotationpayment = Quotationpayment::find($id);

        if ($quotationpayment->payment != $request->get('payment')) {
            $cek = Quotationpayment::where([['quotation', $request->get('quotation')], ['payment', $request->get('payment')]])->first();

            if (!empty($cek)) {
                return redirect()->back()->withErrors('Quotation Payment already exists');
            } else {
                $payment = Payment::where('id', $request->get('payment'))->first();
                $quotation = Quotation::where('id', $request->get('quotation'))->first();
                if ($quotation->payment_term-$quotationpayment->fkPayment->value+$payment->value > 100) {
                    return redirect()->back()->withErrors('Quotation Payment greater than 100%');
                } else {
                    $quotationpayment->quotation     = $request->get('quotation');
                    $quotationpayment->payment       = $request->get('payment');
                    $quotationpayment->updated_user  = $session_id;
                    $quotationpayment->save();

                    $quotation->payment_term = $quotation->payment_term-$quotationpayment->fkPayment->value+$payment->value;
                    $quotation->save();

                    return redirect('/quotationpayment/'.$request->get('quotation'))->with('success', 'Quotation Payment has been updated');
                }
            }
        } else {
            $quotationpayment->quotation     = $request->get('quotation');
            $quotationpayment->payment       = $request->get('payment');
            $quotationpayment->updated_user  = $session_id;
            $quotationpayment->save();

            return redirect('/quotationpayment/'.$request->get('quotation'))->with('success', 'Quotation Payment has been updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $quotationpayment = Quotationpayment::find($id);
        $status = $quotationpayment->status;

        if ($status == 1) {
        	$quotationpayment->status       = 0;
            $quotationpayment->updated_user = $session_id;
	        $quotationpayment->save();

            $quotation = Quotation::where('id', $quotationpayment->quotation)->first();
            $quotation->payment_term = $quotation->payment_term-$quotationpayment->fkPayment->value;
            $quotation->save();

		    return redirect('/quotationpayment/'.$quotationpayment->quotation)->with('error', 'Quotation Payment has been deleted');
        } elseif ($status == 0) {
        	$quotationpayment->status       = 1;
            $quotationpayment->updated_user = $session_id;
	        $quotationpayment->save();

            $quotation = Quotation::where('id', $quotationpayment->quotation)->first();
            $quotation->payment_term = $quotation->payment_term+$quotationpayment->fkPayment->value;
            $quotation->save();

		    return redirect('/quotationpayment/'.$quotationpayment->quotation)->with('success', 'Quotation Payment has been activated');
        }
    }
}
