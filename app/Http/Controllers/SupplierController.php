<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Supplier;
use App\Http\Models\Supplierdetail;
use App\Http\Models\Supplieraccount;
use App\Http\Models\Account;
use App\Http\Models\City;
use App\Http\Models\Company;
use App\Http\Models\Employeeset;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $suppliers = Supplier::orderBy('created_at','desc')->get();
        $supplierdetails = Supplierdetail::where('status', 1)->get();

        return view('vendor/adminlte/supplier.index', compact('suppliers','supplierdetails','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $accounts = Account::where('status', 1)->get();
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/supplier.create', compact('accounts','citys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        if (!empty($request->get('email'))) {
            $request->validate([
                'email' => 'email:true'
            ]);
        }
    	
        $no = Supplier::select('no_inc')->max('no_inc');
        if (empty($no)) {
            $no_inc = 1;
        } else {
            $no_inc = $no + 1;
        }
        $code = "S".sprintf("%05s", $no_inc);

        $supplier = new Supplier([
            'no_inc'        => $no_inc,
            'code'          => $code,
            'accounting'    => $request->get('account'),
            'fax'           => !empty($request->get('fax')) ? $request->get('fax') : '',
            'email'         => !empty($request->get('email')) ? $request->get('email') : '',
            'tax'           => ($request->get('tax') == 1) ? 1 : 0,
            'npwp_no'       => !empty($request->get('npwp_no')) ? $request->get('npwp_no') : '',
            'npwp_name'     => !empty($request->get('npwp_name')) ? $request->get('npwp_name') : '',
            'npwp_address'  => !empty($request->get('npwp_address')) ? $request->get('npwp_address') : '',
            'npwp_city'     => !empty($request->get('npwp_city')) ? $request->get('npwp_city') : 0,
            'limit'         => $request->get('limit'),
            'note1'         => !empty($request->get('note1')) ? $request->get('note1') : '',
            'note2'         => !empty($request->get('note2')) ? $request->get('note2') : '',
            'note3'         => !empty($request->get('note3')) ? $request->get('note3') : '',
            'notice'        => !empty($request->get('notice')) ? $request->get('notice') : '',
            'status_group'  => ($request->get('status_group') == 1) ? 1 : 0,
            'status_record' => ($request->get('status_record') == 1) ? 1 : 0,
            'created_user'  => $session_id,
            'updated_user'  => $session_id
	    ]);
	    $supplier->save();

	    return redirect('/supplierdetail/'.$supplier->id)->with('success', 'Supplier has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $supplier = Supplier::find($id);
        $supplierdetails = Supplierdetail::where('supplier', $id)->get();
        $supplieraccounts = Supplieraccount::where('supplier', $id)->get();
        $accounts = Account::where('status', 1)->get();
        $citys = City::where('status', 1)->get();
        $company = Company::first();

        return view('vendor/adminlte/supplier.show', compact('id','supplier','supplierdetails','supplieraccounts','accounts','citys','company','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $supplier = Supplier::find($id);
        $accounts = Account::where('status', 1)->get();
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/supplier.edit', compact('id','supplier','accounts','citys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        if (!empty($request->get('email'))) {
            $request->validate([
                'email' => 'email:true'
            ]);
        }

    	$supplier = Supplier::find($id);
        $supplier->accounting    = $request->get('account');
        $supplier->fax           = !empty($request->get('fax')) ? $request->get('fax') : '';
        $supplier->email         = !empty($request->get('email')) ? $request->get('email') : '';
        $supplier->tax           = ($request->get('tax') == 1) ? 1 : 0;
        $supplier->npwp_no       = !empty($request->get('npwp_no')) ? $request->get('npwp_no') : '';
        $supplier->npwp_name     = !empty($request->get('npwp_name')) ? $request->get('npwp_name') : '';
        $supplier->npwp_address  = !empty($request->get('npwp_address')) ? $request->get('npwp_address') : '';
        $supplier->npwp_city     = !empty($request->get('npwp_city')) ? $request->get('npwp_city') : 0;
        $supplier->limit         = $request->get('limit');
        $supplier->note1         = !empty($request->get('note1')) ? $request->get('note1') : '';
        $supplier->note2         = !empty($request->get('note2')) ? $request->get('note2') : '';
        $supplier->note3         = !empty($request->get('note3')) ? $request->get('note3') : '';
        $supplier->notice        = !empty($request->get('notice')) ? $request->get('notice') : '';
        $supplier->status_group  = ($request->get('status_group') == 1) ? 1 : 0;
        $supplier->status_record = ($request->get('status_record') == 1) ? 1 : 0;
	    $supplier->updated_user  = $session_id;
	    $supplier->save();

	    return redirect('/supplier')->with('success', 'Supplier has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $supplier = Supplier::find($id);
        $status = $supplier->status;

        if ($status == 1) {
        	$supplier->status       = 0;
            $supplier->updated_user = $session_id;
	        $supplier->save();

		    return redirect('/supplier')->with('error', 'Supplier has been deleted');
        } elseif ($status == 0) {
        	$supplier->status       = 1;
            $supplier->updated_user = $session_id;
	        $supplier->save();

		    return redirect('/supplier')->with('success', 'Supplier has been activated');
        }
    }
}
