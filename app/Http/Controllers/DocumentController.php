<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Document;
use App\Http\Models\Employeeset;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $documents = Document::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/document.index', compact('documents','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        return view('vendor/adminlte/document.create', compact('disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	
        $request->validate([
		    'name' => 'unique:documents'
		]);

        $no = Document::select('no_inc')->max('no_inc');
        if (empty($no)) {
            $no_inc = 1;
        } else {
            $no_inc = $no + 1;
        }
        $code = "DOC".sprintf("%05s", $no_inc);

        $document = new Document([
            'no_inc'        => $no_inc,
            'code'          => $code,
            'name' 		    => $request->get('name'),
            'export'        => ($request->get('export') == 1) ? 1 : 0,
            'local'         => ($request->get('local') == 1) ? 1 : 0,
            'internal'      => ($request->get('internal') == 1) ? 1 : 0,
            'created_user'  => $session_id,
            'updated_user'  => $session_id
	    ]);
	    $document->save();

	    return redirect('/document')->with('success', 'Document has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $document = Document::find($id);

        return view('vendor/adminlte/document.edit', compact('document','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

    	$document = Document::find($id);
        $name = $document->name;

        if ($name != $request->get('name')) {
            $request->validate([
                'name' => 'unique:documents'
            ]);
        }
        
        $document->name 	        = $request->get('name');
        $document->export           = ($request->get('export') == 1) ? 1 : 0;
        $document->local            = ($request->get('local') == 1) ? 1 : 0;
        $document->internal         = ($request->get('internal') == 1) ? 1 : 0;
	    $document->updated_user 	= $session_id;
	    $document->save();

	    return redirect('/document')->with('success', 'Document has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $document = Document::find($id);
        $status = $document->status;

        if ($status == 1) {
        	$document->status       = 0;
            $document->updated_user = $session_id;
	        $document->save();

		    return redirect('/document')->with('error', 'Document has been deleted');
        } elseif ($status == 0) {
        	$document->status       = 1;
            $document->updated_user = $session_id;
	        $document->save();

		    return redirect('/document')->with('success', 'Document has been activated');
        }
    }
}
