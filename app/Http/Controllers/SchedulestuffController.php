<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Schedulestuff;
use App\Http\Models\Schedulebook;
use App\Http\Models\Scheduleship;
use App\Http\Models\Quotationsplit;
use App\Http\Models\Employeeset;

class SchedulestuffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $schedulebooks = Schedulebook::where('status_approve','<>',0)->orWhere([['status_approve', 2], ['status_stuff', 1]])->orderBy('created_at','desc')->get();
        $schedulestuffs = Schedulestuff::where('status_approve', 0)->get();

        return view('vendor/adminlte/schedulestuff.index', compact('schedulebooks','schedulestuffs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $quotationsplits = Quotationsplit::where('status_book', 0)->orderBy('no_split')->get();
        $scheduleship = Scheduleship::find($id);
        $schedulebooks = Schedulebook::where([['pol', $scheduleship->pol], ['pod', $scheduleship->pod], ['lines', $scheduleship->lines], ['etd', $scheduleship->etd], ['eta', $scheduleship->eta]])->get();

        return view('vendor/adminlte/schedulebook.create', compact('id','quotationsplits','schedulebooks','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $schedulestuffs = Schedulestuff::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/schedulestuff.list', compact('schedulestuffs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Schedulestuff::where('schedulebook', $id)->first();

        $request->validate([
            'qty_fcl'   => 'numeric|min:0|not_in:0'
        ]);

        if ($request->get('date') > $request->get('etd')) {
            return redirect()->back()->withErrors('Date greater than ETD');
        } else {
            if (empty($cek)) {
                $schedulestuff = new Schedulestuff([
                    'schedulebook'      => $id,
                    'date'              => $request->get('date'),
                    'qty_fcl'           => $request->get('qty_fcl'),
                    'person'            => $request->get('person'),
                    'notice'            => !empty($request->get('notice')) ? $request->get('notice') : '',
                    'freight'           => 0,
                    'vessel'            => '',
                    'reason'            => '',
                    'created_user'      => $session_id,
                    'updated_user'      => $session_id
                ]);
                $schedulestuff->save();

                $schedulebook = Schedulebook::find($id);
                $schedulebook->status_stuff = 1;
                $schedulebook->updated_user = $session_id;
                $schedulebook->save();

                return redirect('/schedulestufflist')->with('success', 'Stuffing Schedule has been added');
            } else {
                $cek->date          = $request->get('date');
                $cek->qty_fcl       = $request->get('qty_fcl');
                $cek->person        = $request->get('person');
                $cek->notice        = !empty($request->get('notice')) ? $request->get('notice') : '';
                $cek->freight       = 0;
                $cek->updated_user  = $session_id;
                $cek->save();

                return redirect('/schedulestufflist')->with('success', 'Stuffing Schedule has been updated');
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function approval(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $schedulestuffs = Schedulestuff::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/schedulestuff.approval', compact('schedulestuffs','disable','employeeroots','employeemenus','employeesubs'));
    }
    
    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        if ($request->get('status_reschedule') == 1) {
            $schedulestuff = Schedulestuff::find($id);
            $schedulestuff->status_approve    = 1;
            $schedulestuff->reason            = $request->get('reason');
            $schedulestuff->updated_user      = $session_id;
            $schedulestuff->save();

            return redirect('/approvalstuff')->with('success', 'Stuffing Re-Schedule has been approved');
        } else {
            if (!empty($request->get('qty_approve')) && !empty($request->get('freight'))) {
                $request->validate([
                    'qty_approve'   => 'numeric|min:0|not_in:0',
                    'freight'       => 'numeric|min:0|not_in:0'
                ]);
            } elseif (!empty($request->get('qty_approve'))) {
                $request->validate([
                    'qty_approve'   => 'numeric|min:0|not_in:0'
                ]);
            } elseif (!empty($request->get('freight'))) {
                $request->validate([
                    'freight'       => 'numeric|min:0|not_in:0'
                ]);
            }

            $schedulestuff = Schedulestuff::find($id);
            $schedulestuff->status_approve    = 1;
            $schedulestuff->qty_approve       = !empty($request->get('qty_approve')) ? $request->get('qty_approve') : $request->get('qty_fcl');
            $schedulestuff->freight           = !empty($request->get('freight')) ? $request->get('freight') : 0;
            $schedulestuff->vessel            = $request->get('vessel');
            $schedulestuff->reason            = $request->get('reason');
            $schedulestuff->updated_user      = $session_id;
            $schedulestuff->save();

            $schedulebook = Schedulebook::where('id', $schedulestuff->schedulebook)->first();
            $schedulebook->status_stuff_acc   = 1;
            $schedulebook->updated_user       = $session_id;
            $schedulebook->save();

            return redirect('/approvalstuff')->with('success', 'Stuffing Schedule has been approved');
        }
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $schedulestuff = Schedulestuff::find($id);
        $schedulestuff->status_approve    = 2;
        $schedulestuff->reason            = $request->get('reason');
        $schedulestuff->updated_user      = $session_id;
        $schedulestuff->save();

        $schedulebook = Schedulebook::where('id', $schedulestuff->schedulebook)->first();
        $schedulebook->status_approve     = 2;
        $schedulebook->status_stuff_acc   = 2;
        $schedulebook->updated_user       = $session_id;
        $schedulebook->save();

        $scheduleship = Scheduleship::where([['pol', $schedulebook->pol], ['pod', $schedulebook->pod], ['lines', $schedulebook->lines], ['etd', $schedulebook->etd], ['eta', $schedulebook->eta]])->first();
        $scheduleship->status_book = 0;
        $scheduleship->save();

        $quotationsplit = Quotationsplit::where('id', $schedulebook->quotationsplit)->first();
        $quotationsplit->status_book = 0;
        $quotationsplit->save();

        return redirect('/approvalstuff')->with('error', 'Stuffing Schedule has been rejected');
    }
}
