<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Materialdetail;
use App\Http\Models\Product;
use App\Http\Models\Unit;
use App\Http\Models\Employeeset;

class MaterialdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Materialdetail::where([['material', $request->get('material')], ['product', $request->get('product')]])->first();

        if (!empty($cek)) {
            return redirect()->back()->withErrors('Material Detail already exists');
        } else {
            $materialdetail = new Materialdetail([
                'material'          => $request->get('material'),
                'product'           => $request->get('product'),
                'unit'              => $request->get('unit'),
                'qty'               => $request->get('qty'),
                'created_user'      => $session_id,
                'updated_user'      => $session_id
            ]);
            $materialdetail->save();

            return redirect('/materialdetail/'.$request->get('material'))->with('success', 'Material Detail has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $materialdetails = Materialdetail::where('material', $id)->orderBy('created_at','desc')->get();
        $products = Product::where('status', 1)->get();
        $units = Unit::where('status', 1)->get();

        return view('vendor/adminlte/materialdetail.show', compact('id','materialdetails','products','units','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $materialdetail = Materialdetail::find($id);
        $materialdetails = Materialdetail::where('material', $materialdetail->material)->orderBy('created_at','desc')->get();
        $products = Product::where('status', 1)->get();
        $units = Unit::where('status', 1)->get();

        return view('vendor/adminlte/materialdetail.edit', compact('materialdetail','materialdetails','products','units','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $materialdetail = Materialdetail::find($id);

        if ($materialdetail->product != $request->get('product')) {
            $cek = Materialdetail::where([['material', $request->get('material')], ['product', $request->get('product')]])->first();

            if (!empty($cek)) {
                return redirect()->back()->withErrors('Material Detail already exists');
            } else {
                $materialdetail->material         = $request->get('material');
                $materialdetail->product          = $request->get('product');
                $materialdetail->unit             = $request->get('unit');
                $materialdetail->qty              = $request->get('qty');
                $materialdetail->updated_user     = $session_id;
                $materialdetail->save();

                return redirect('/materialdetail/'.$request->get('material'))->with('success', 'Material Detail has been updated');
            }
        } else {
            $materialdetail->material         = $request->get('material');
            $materialdetail->product          = $request->get('product');
            $materialdetail->unit             = $request->get('unit');
            $materialdetail->qty              = $request->get('qty');
            $materialdetail->updated_user     = $session_id;
            $materialdetail->save();

            return redirect('/materialdetail/'.$request->get('material'))->with('success', 'Material Detail has been updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $materialdetail = Materialdetail::find($id);
        $status = $materialdetail->status;

        if ($status == 1) {
        	$materialdetail->status       = 0;
            $materialdetail->updated_user = $session_id;
	        $materialdetail->save();

		    return redirect('/materialdetail/'.$materialdetail->material)->with('error', 'Material Detail has been deleted');
        } elseif ($status == 0) {
        	$materialdetail->status       = 1;
            $materialdetail->updated_user = $session_id;
	        $materialdetail->save();

		    return redirect('/materialdetail/'.$materialdetail->material)->with('success', 'Material Detail has been activated');
        }
    }
}
