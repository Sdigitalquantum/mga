<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Quotation;
use App\Http\Models\Quotationcharge;
use App\Http\Models\Employeeset;

class QuotationchargeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Quotationcharge::where([['quotation', $request->get('quotation')], ['name', $request->get('name')]])->first();

        if (!empty($cek)) {
            return redirect()->back()->withErrors('Quotation Charges already exists');
        } else {
            $quotationcharge = new Quotationcharge([
                'quotation'     => $request->get('quotation'),
                'name'          => $request->get('name'),
                'type'          => $request->get('type'),
                'value'         => $request->get('value'),
                'created_user'  => $session_id,
                'updated_user'  => $session_id
            ]);
            $quotationcharge->save();

            return redirect('/quotationcharge/'.$request->get('quotation'))->with('success', 'Quotation Charges has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $quotation = Quotation::find($id);
        $quotationcharges = Quotationcharge::where('quotation', $id)->orderBy('created_at','desc')->get();

        return view('vendor/adminlte/quotationcharge.show', compact('quotation','quotationcharges','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $quotationcharge = Quotationcharge::find($id);
        $quotationcharges = Quotationcharge::where('quotation', $quotationcharge->quotation)->orderBy('created_at','desc')->get();

        return view('vendor/adminlte/quotationcharge.edit', compact('quotationcharge','quotationcharges','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $quotationcharge = Quotationcharge::find($id);

        if ($quotationcharge->name != $request->get('name')) {
            $cek = Quotationcharge::where([['quotation', $request->get('quotation')], ['name', $request->get('name')]])->first();

            if (!empty($cek)) {
                return redirect()->back()->withErrors('Quotation Charges already exists');
            } else {
                $quotationcharge->name          = $request->get('name');
                $quotationcharge->type          = $request->get('type');
                $quotationcharge->value         = $request->get('value');
                $quotationcharge->updated_user  = $session_id;
                $quotationcharge->save();

                return redirect('/quotationcharge/'.$request->get('quotation'))->with('success', 'Quotation Charges has been updated');
            }
        } else {
            $quotationcharge->type          = $request->get('type');
            $quotationcharge->value         = $request->get('value');
            $quotationcharge->updated_user  = $session_id;
            $quotationcharge->save();

            return redirect('/quotationcharge/'.$request->get('quotation'))->with('success', 'Quotation Charges has been updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $quotationcharge = Quotationcharge::find($id);
        $status = $quotationcharge->status;

        if ($status == 1) {
        	$quotationcharge->status       = 0;
            $quotationcharge->updated_user = $session_id;
	        $quotationcharge->save();

		    return redirect('/quotationcharge/'.$quotationcharge->quotation)->with('error', 'Quotation Charges has been deleted');
        } elseif ($status == 0) {
        	$quotationcharge->status       = 1;
            $quotationcharge->updated_user = $session_id;
	        $quotationcharge->save();

		    return redirect('/quotationcharge/'.$quotationcharge->quotation)->with('success', 'Quotation Charges has been activated');
        }
    }
}
