<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Paymentterm;
use App\Http\Models\Employeeset;

class PaymenttermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $paymentterms = Paymentterm::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/paymentterm.index', compact('paymentterms','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        return view('vendor/adminlte/paymentterm.create', compact('disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	
        $request->validate([
		    'code' => 'unique:paymentterms'
		]);

        $paymentterm = new Paymentterm([
            'code'          => $request->get('code'),
	        'name' 		    => $request->get('name'),
            'created_user'  => $session_id,
            'updated_user'  => $session_id
	    ]);
	    $paymentterm->save();

	    return redirect('/paymentterm')->with('success', 'Payment Term has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $paymentterm = Paymentterm::find($id);

        return view('vendor/adminlte/paymentterm.edit', compact('paymentterm','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

    	$paymentterm = Paymentterm::find($id);
        $code = $paymentterm->code;

        if ($code != $request->get('code')) {
            $request->validate([
                'code' => 'unique:paymentterms'
            ]);
        }
        
        $paymentterm->code          = $request->get('code');
        $paymentterm->name 	        = $request->get('name');
	    $paymentterm->updated_user 	= $session_id;
	    $paymentterm->save();

	    return redirect('/paymentterm')->with('success', 'Payment Term has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $paymentterm = Paymentterm::find($id);
        $status = $paymentterm->status;

        if ($status == 1) {
        	$paymentterm->status       = 0;
            $paymentterm->updated_user = $session_id;
	        $paymentterm->save();

		    return redirect('/paymentterm')->with('error', 'Payment Term has been deleted');
        } elseif ($status == 0) {
        	$paymentterm->status       = 1;
            $paymentterm->updated_user = $session_id;
	        $paymentterm->save();

		    return redirect('/paymentterm')->with('success', 'Payment Term has been activated');
        }
    }
}
