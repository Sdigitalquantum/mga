<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Kurs;
use App\Http\Models\Currency;
use App\Http\Models\Employeeset;

class KursController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $kurss = Kurs::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/kurs.index', compact('kurss','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $currencys = Currency::where('status', 1)->get();

        return view('vendor/adminlte/kurs.create', compact('currencys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	
        if ($request->get('end') < $request->get('start')) {
            return redirect()->back()->withErrors('End lower than start');
        } else {
            $kurs = new Kurs([
                'currency'      => $request->get('currency'),
                'value'         => $request->get('value'),
                'start'         => $request->get('start'),
                'end'           => $request->get('end'),
                'created_user'  => $session_id,
                'updated_user'  => $session_id
            ]);
            $kurs->save();

            return redirect('/kurs')->with('success', 'Kurs has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $kurs = Kurs::find($id);
        $currencys = Currency::where('status', 1)->get();

        return view('vendor/adminlte/kurs.edit', compact('kurs','currencys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        if ($request->get('end') < $request->get('start')) {
            return redirect()->back()->withErrors('End lower than start');
        } else {
            $kurs = Kurs::find($id);
            $kurs->currency        = $request->get('currency');
            $kurs->value           = $request->get('value');
            $kurs->start           = $request->get('start');
            $kurs->end             = $request->get('end');
            $kurs->updated_user    = $session_id;
            $kurs->save();

            return redirect('/kurs')->with('success', 'Kurs has been updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $kurs = Kurs::find($id);
        $status = $kurs->status;

        if ($status == 1) {
        	$kurs->status       = 0;
            $kurs->updated_user = $session_id;
	        $kurs->save();

		    return redirect('/kurs')->with('error', 'Kurs has been deleted');
        } elseif ($status == 0) {
        	$kurs->status       = 1;
            $kurs->updated_user = $session_id;
	        $kurs->save();

		    return redirect('/kurs')->with('success', 'Kurs has been activated');
        }
    }
}
