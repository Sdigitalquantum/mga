<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Customer;
use App\Http\Models\Customerdetail;
use App\Http\Models\Customeraccount;
use App\Http\Models\Account;
use App\Http\Models\City;
use App\Http\Models\Company;
use App\Http\Models\Employeeset;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $customers = Customer::orderBy('created_at','desc')->get();
        $customerdetails = Customerdetail::where('status', 1)->get();

        return view('vendor/adminlte/customer.index', compact('customers','customerdetails','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $accounts = Account::where('status', 1)->get();
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/customer.create', compact('accounts','citys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        if (!empty($request->get('email'))) {
            $request->validate([
                'email' => 'email:true'
            ]);
        }
    	
        $no = Customer::select('no_inc')->max('no_inc');
        if (empty($no)) {
            $no_inc = 1;
        } else {
            $no_inc = $no + 1;
        }
        $code = "CST".sprintf("%05s", $no_inc);

        $customer = new Customer([
            'no_inc'        => $no_inc,
            'code'          => $code,
            'accounting'    => $request->get('account'),
            'fax'           => !empty($request->get('fax')) ? $request->get('fax') : '',
            'email'         => !empty($request->get('email')) ? $request->get('email') : '',
            'tax'           => ($request->get('tax') == 1) ? 1 : 0,
            'npwp_no'       => !empty($request->get('npwp_no')) ? $request->get('npwp_no') : '',
            'npwp_name'     => !empty($request->get('npwp_name')) ? $request->get('npwp_name') : '',
            'npwp_address'  => !empty($request->get('npwp_address')) ? $request->get('npwp_address') : '',
            'npwp_city'     => !empty($request->get('npwp_city')) ? $request->get('npwp_city') : 0,
            'limit'         => $request->get('limit'),
            'note1'         => !empty($request->get('note1')) ? $request->get('note1') : '',
            'note2'         => !empty($request->get('note2')) ? $request->get('note2') : '',
            'note3'         => !empty($request->get('note3')) ? $request->get('note3') : '',
            'notice'        => !empty($request->get('notice')) ? $request->get('notice') : '',
            'status_group'  => ($request->get('status_group') == 1) ? 1 : 0,
            'status_record' => ($request->get('status_record') == 1) ? 1 : 0,
            'created_user'  => $session_id,
            'updated_user'  => $session_id
	    ]);
	    $customer->save();

	    return redirect('/customerdetail/'.$customer->id)->with('success', 'Customer has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $customer = Customer::find($id);
        $customerdetails = Customerdetail::where('customer', $id)->get();
        $customeraccounts = Customeraccount::where('customer', $id)->get();
        $accounts = Account::where('status', 1)->get();
        $citys = City::where('status', 1)->get();
        $company = Company::first();

        return view('vendor/adminlte/customer.show', compact('id','customer','customerdetails','customeraccounts','accounts','citys','company','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $customer = Customer::find($id);
        $accounts = Account::where('status', 1)->get();
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/customer.edit', compact('id','customer','accounts','citys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        if (!empty($request->get('email'))) {
            $request->validate([
                'email' => 'email:true'
            ]);
        }

    	$customer = Customer::find($id);
        $customer->accounting    = $request->get('account');
        $customer->fax           = !empty($request->get('fax')) ? $request->get('fax') : '';
        $customer->email         = !empty($request->get('email')) ? $request->get('email') : '';
        $customer->tax           = ($request->get('tax') == 1) ? 1 : 0;
        $customer->npwp_no       = !empty($request->get('npwp_no')) ? $request->get('npwp_no') : '';
        $customer->npwp_name     = !empty($request->get('npwp_name')) ? $request->get('npwp_name') : '';
        $customer->npwp_address  = !empty($request->get('npwp_address')) ? $request->get('npwp_address') : '';
        $customer->npwp_city     = !empty($request->get('npwp_city')) ? $request->get('npwp_city') : 0;
        $customer->limit         = $request->get('limit');
        $customer->note1         = !empty($request->get('note1')) ? $request->get('note1') : '';
        $customer->note2         = !empty($request->get('note2')) ? $request->get('note2') : '';
        $customer->note3         = !empty($request->get('note3')) ? $request->get('note3') : '';
        $customer->notice        = !empty($request->get('notice')) ? $request->get('notice') : '';
        $customer->status_group  = ($request->get('status_group') == 1) ? 1 : 0;
        $customer->status_record = ($request->get('status_record') == 1) ? 1 : 0;
	    $customer->updated_user  = $session_id;
	    $customer->save();

	    return redirect('/customer')->with('success', 'Customer has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $customer = Customer::find($id);
        $status = $customer->status;

        if ($status == 1) {
        	$customer->status       = 0;
            $customer->updated_user = $session_id;
	        $customer->save();

		    return redirect('/customer')->with('error', 'Customer has been deleted');
        } elseif ($status == 0) {
        	$customer->status       = 1;
            $customer->updated_user = $session_id;
	        $customer->save();

		    return redirect('/customer')->with('success', 'Customer has been activated');
        }
    }
}
