<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Product;
use App\Http\Models\Productalias;
use App\Http\Models\Account;
use App\Http\Models\Department;
use App\Http\Models\Variant;
use App\Http\Models\Merk;
use App\Http\Models\Typesub;
use App\Http\Models\Size;
use App\Http\Models\Unit;
use App\Http\Models\Company;
use App\Http\Models\Employeeset;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $products = Product::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/product.index', compact('products','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $accounts = Account::where('status', 1)->orderBy('name')->get();
        $departments = Department::where('status', 1)->orderBy('name')->get();
        $variants = Variant::where('status', 1)->orderBy('name')->get();
        $merks    = Merk::where('status', 1)->orderBy('name')->get();
        $typesubs = Typesub::where('status', 1)->orderBy('name')->get();
        $sizes    = Size::where('status', 1)->orderBy('name')->get();
        $units    = Unit::where('status', 1)->orderBy('name')->get();

        return view('vendor/adminlte/product.create', compact('accounts','departments','variants','merks','typesubs','sizes','units','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $request->validate([
            'code' => 'unique:products'
        ]);
    	
        $product = new Product([
            'code'          => $request->get('code'),
            'name'          => $request->get('name'),
            'accounting'    => $request->get('account'),
            'department'    => $request->get('department'),
            'variant'       => !empty($request->get('variant')) ? $request->get('variant') : 0,
            'merk'          => !empty($request->get('merk')) ? $request->get('merk') : 0,
            'typesub'       => !empty($request->get('typesub')) ? $request->get('typesub') : 0,
            'size'          => !empty($request->get('size')) ? $request->get('size') : 0,
            'unit'          => $request->get('unit'),
            'general'       => ($request->get('general') == 1) ? 1 : 0,
            'status_group'  => ($request->get('status_group') == 1) ? 1 : 0,
            'status_record' => ($request->get('status_record') == 1) ? 1 : 0,
            'created_user'  => $session_id,
            'updated_user'  => $session_id
        ]);
	    $product->save();

	    return redirect('/productalias/'.$product->id)->with('success', 'Product has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $product  = Product::find($id);
        $productaliass = Productalias::where('product', $id)->get();
        $accounts = Account::where('status', 1)->orderBy('name')->get();
        $departments = Department::where('status', 1)->orderBy('name')->get();
        $variants = Variant::where('status', 1)->orderBy('name')->get();
        $merks    = Merk::where('status', 1)->orderBy('name')->get();
        $typesubs = Typesub::where('status', 1)->orderBy('name')->get();
        $sizes    = Size::where('status', 1)->orderBy('name')->get();
        $units    = Unit::where('status', 1)->orderBy('name')->get();
        $company = Company::first();

        return view('vendor/adminlte/product.show', compact('id','product','productaliass','accounts','departments','variants','merks','typesubs','sizes','units','company','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $product  = Product::find($id);
        $accounts = Account::where('status', 1)->orderBy('name')->get();
        $departments = Department::where('status', 1)->orderBy('name')->get();
        $variants = Variant::where('status', 1)->orderBy('name')->get();
        $merks    = Merk::where('status', 1)->orderBy('name')->get();
        $typesubs = Typesub::where('status', 1)->orderBy('name')->get();
        $sizes    = Size::where('status', 1)->orderBy('name')->get();
        $units    = Unit::where('status', 1)->orderBy('name')->get();

        return view('vendor/adminlte/product.edit', compact('id','product','accounts','departments','variants','merks','typesubs','sizes','units','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $product = Product::find($id);

        if ($product->code != $request->get('code')) {
            $request->validate([
                'code' => 'unique:products'
            ]);
        }

    	$product->code          = $request->get('code');
        $product->name          = $request->get('name');
        $product->accounting    = $request->get('account');
        $product->department    = $request->get('department');
        $product->variant       = $request->get('variant');
        $product->merk          = $request->get('merk');
        $product->typesub       = $request->get('typesub');
        $product->size          = $request->get('size');
        $product->unit          = $request->get('unit');
        $product->general       = ($request->get('general') == 1) ? 1 : 0;
        $product->status_group  = ($request->get('status_group') == 1) ? 1 : 0;
        $product->status_record = ($request->get('status_record') == 1) ? 1 : 0;
	    $product->updated_user  = $session_id;
	    $product->save();

	    return redirect('/product')->with('success', 'Product has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $product = Product::find($id);
        $status = $product->status;

        if ($status == 1) {
        	$product->status       = 0;
            $product->updated_user = $session_id;
	        $product->save();

		    return redirect('/product')->with('error', 'Product has been deleted');
        } elseif ($status == 0) {
        	$product->status       = 1;
            $product->updated_user = $session_id;
	        $product->save();

		    return redirect('/product')->with('success', 'Product has been activated');
        }
    }
}
