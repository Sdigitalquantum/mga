<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Company;
use App\Http\Models\Companyaccount;
use App\Http\Models\City;
use App\Http\Models\Employeeset;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $companys = Company::orderBy('created_at','desc')->get();
        $company = Company::first();

        return view('vendor/adminlte/company.index', compact('companys','company','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/company.create', compact('citys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        if (!empty($request->get('email'))) {
            $request->validate([
                'name'  => 'unique:companies',
                'email' => 'email:true'
            ]);
        } else {
            $request->validate([
                'name'  => 'unique:companies'
            ]);
        }
    	
        $company = new Company([
            'city'          => $request->get('city'),
            'code'          => $request->get('code'),
            'name'          => $request->get('name'),
            'address'       => $request->get('address'),
            'email'         => $request->get('email'),
            'mobile'        => $request->get('mobile'),
            'fax'           => $request->get('fax'),
            'created_user'  => $session_id,
            'updated_user'  => $session_id
	    ]);
	    $company->save();

	    return redirect('/companyaccount/'.$company->id)->with('success', 'Company has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $company = Company::find($id);
        $companyaccounts = Companyaccount::where('company', $id)->get();
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/company.show', compact('id','company','companyaccounts','citys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $company = Company::find($id);
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/company.edit', compact('id','company','citys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $company = Company::find($id);

        if ($company->name != $request->get('name')) {
            if (!empty($request->get('email'))) {
                $request->validate([
                    'name'  => 'unique:companies',
                    'email' => 'email:true'
                ]);
            } else {
                $request->validate([
                    'name'  => 'unique:companies'
                ]);
            }
        } else {
            if (!empty($request->get('email'))) {
                $request->validate([
                    'email' => 'email:true'
                ]);
            }
        }

        $company->city          = $request->get('city');
        $company->code          = $request->get('code');
        $company->name          = $request->get('name');
        $company->address       = $request->get('address');
        $company->email         = $request->get('email');
        $company->mobile        = $request->get('mobile');
        $company->fax           = $request->get('fax');
	    $company->updated_user  = $session_id;
	    $company->save();

	    return redirect('/company')->with('success', 'Company has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $company = Company::find($id);
        $status = $company->status;

        if ($status == 1) {
        	$company->status       = 0;
            $company->updated_user = $session_id;
	        $company->save();

		    return redirect('/company')->with('error', 'Company has been deleted');
        } elseif ($status == 0) {
        	$company->status       = 1;
            $company->updated_user = $session_id;
	        $company->save();

		    return redirect('/company')->with('success', 'Company has been activated');
        }
    }
}
