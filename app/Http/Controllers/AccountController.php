<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Account;
use App\Http\Models\Accountgroup;
use App\Http\Models\Accounttype;
use App\Http\Models\Currency;
use App\Http\Models\Employeeset;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $accounts = Account::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/account.index', compact('accounts','countrys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $groups = Accountgroup::where('status', 1)->get();
        $types  = Accounttype::where('status', 1)->get();
        $currencys = Currency::where('status', 1)->get();

        return view('vendor/adminlte/account.create', compact('groups','types','currencys','countrys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	
        $request->validate([
		    'code' => 'unique:accounts'
		]);

        $account = new Account([
            'currency'      => $request->get('currency'),
            'group'         => $request->get('group'),
            'type'          => $request->get('type'),
            'code'          => $request->get('code'),
	        'name' 		    => $request->get('name'),
            'level1'        => !empty($request->get('level1')) ? $request->get('level1') : 0,
            'level2'        => !empty($request->get('level2')) ? $request->get('level2') : 0,
            'level3'        => !empty($request->get('level3')) ? $request->get('level3') : 0,
            'level4'        => !empty($request->get('level4')) ? $request->get('level4') : 0,
            'created_user'  => $session_id,
            'updated_user'  => $session_id
	    ]);
	    $account->save();

	    return redirect('/account')->with('success', 'Accounting has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $account = Account::find($id);
        $groups  = Accountgroup::where('status', 1)->get();
        $types   = Accounttype::where('status', 1)->get();
        $currencys = Currency::where('status', 1)->get();

        return view('vendor/adminlte/account.edit', compact('account','groups','types','currencys','countrys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

    	$account = Account::find($id);
        $code = $account->code;

        if ($code != $request->get('code')) {
            $request->validate([
                'code' => 'unique:accounts'
            ]);
        }
        
        $account->currency      = $request->get('currency');
        $account->group         = $request->get('group');
        $account->type          = $request->get('type');
        $account->code          = $request->get('code');
        $account->name 	        = $request->get('name');
        $account->level1        = !empty($request->get('level1')) ? $request->get('level1') : 0;
        $account->level2        = !empty($request->get('level2')) ? $request->get('level2') : 0;
        $account->level3        = !empty($request->get('level3')) ? $request->get('level3') : 0;
        $account->level4        = !empty($request->get('level4')) ? $request->get('level4') : 0;
	    $account->updated_user 	= $session_id;
	    $account->save();

	    return redirect('/account')->with('success', 'Accounting has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $account = Account::find($id);
        $status = $account->status;

        if ($status == 1) {
        	$account->status       = 0;
            $account->updated_user = $session_id;
	        $account->save();

		    return redirect('/account')->with('error', 'Accounting has been deleted');
        } elseif ($status == 0) {
        	$account->status       = 1;
            $account->updated_user = $session_id;
	        $account->save();

		    return redirect('/account')->with('success', 'Accounting has been activated');
        }
    }
}
