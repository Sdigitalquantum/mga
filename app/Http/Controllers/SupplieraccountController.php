<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Supplieraccount;
use App\Http\Models\Currency;
use App\Http\Models\Bank;
use App\Http\Models\Employeeset;

class SupplieraccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $supplieraccount = new Supplieraccount([
            'supplier'      => $request->get('supplier'),
            'currency'      => $request->get('currency'),
            'bank'          => $request->get('bank'),
            'account_no'    => $request->get('account_no'),
            'account_swift' => !empty($request->get('account_swift')) ? $request->get('account_swift') : '-',
            'account_name'  => $request->get('account_name'),
            'alias'         => !empty($request->get('alias')) ? $request->get('alias') : '',
            'created_user'  => $session_id,
            'updated_user'  => $session_id
	    ]);
	    $supplieraccount->save();

	    return redirect('/supplieraccount/'.$request->get('supplier'))->with('success', 'Supplier Account has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $supplieraccounts = Supplieraccount::where('supplier', $id)->orderBy('created_at','desc')->get();
        $currencys = Currency::where('status', 1)->get();
        $banks = Bank::where('status', 1)->get();

        return view('vendor/adminlte/supplieraccount.show', compact('id','supplieraccounts','currencys','banks','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $supplieraccount = Supplieraccount::find($id);
        $supplieraccounts = Supplieraccount::where('supplier', $supplieraccount->supplier)->orderBy('created_at','desc')->get();
        $currencys = Currency::where('status', 1)->get();
        $banks = Bank::where('status', 1)->get();

        return view('vendor/adminlte/supplieraccount.edit', compact('supplieraccount','supplieraccounts','currencys','banks','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $supplieraccount = Supplieraccount::find($id);
        $supplieraccount->supplier      = $request->get('supplier');
        $supplieraccount->currency      = $request->get('currency');
        $supplieraccount->bank          = $request->get('bank');
        $supplieraccount->account_no    = $request->get('account_no');
        $supplieraccount->account_swift = !empty($request->get('account_swift')) ? $request->get('account_swift') : '-';
        $supplieraccount->account_name  = $request->get('account_name');
        $supplieraccount->alias         = !empty($request->get('alias')) ? $request->get('alias') : '';
	    $supplieraccount->updated_user  = $session_id;
	    $supplieraccount->save();

	    return redirect('/supplieraccount/'.$request->get('supplier'))->with('success', 'Supplier Account has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $supplieraccount = Supplieraccount::find($id);
        $status = $supplieraccount->status;

        if ($status == 1) {
        	$supplieraccount->status       = 0;
            $supplieraccount->updated_user = $session_id;
	        $supplieraccount->save();

		    return redirect('/supplieraccount/'.$supplieraccount->supplier)->with('error', 'Supplier Account has been deleted');
        } elseif ($status == 0) {
        	$supplieraccount->status       = 1;
            $supplieraccount->updated_user = $session_id;
	        $supplieraccount->save();

		    return redirect('/supplieraccount/'.$supplieraccount->supplier)->with('success', 'Supplier Account has been activated');
        }
    }
}
