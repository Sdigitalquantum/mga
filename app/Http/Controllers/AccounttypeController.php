<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Accounttype;
use App\Http\Models\Employeeset;

class AccounttypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $accounttypes = Accounttype::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/accounttype.index', compact('accounttypes','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        return view('vendor/adminlte/accounttype.create', compact('disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	
        $request->validate([
		    'code' => 'unique:accounttypes'
		]);

        $accounttype = new Accounttype([
            'code'          => $request->get('code'),
	        'name' 		    => $request->get('name'),
            'created_user'  => $session_id,
            'updated_user'  => $session_id
	    ]);
	    $accounttype->save();

	    return redirect('/accounttype')->with('success', 'Account Type has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $accounttype = Accounttype::find($id);

        return view('vendor/adminlte/accounttype.edit', compact('accounttype','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

    	$accounttype = Accounttype::find($id);
        $code = $accounttype->code;

        if ($code != $request->get('code')) {
            $request->validate([
                'code' => 'unique:accounttypes'
            ]);
        }
        
        $accounttype->code          = $request->get('code');
        $accounttype->name 	        = $request->get('name');
	    $accounttype->updated_user 	= $session_id;
	    $accounttype->save();

	    return redirect('/accounttype')->with('success', 'Account Type has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $accounttype = Accounttype::find($id);
        $status = $accounttype->status;

        if ($status == 1) {
        	$accounttype->status       = 0;
            $accounttype->updated_user = $session_id;
	        $accounttype->save();

		    return redirect('/accounttype')->with('error', 'Account Type has been deleted');
        } elseif ($status == 0) {
        	$accounttype->status       = 1;
            $accounttype->updated_user = $session_id;
	        $accounttype->save();

		    return redirect('/accounttype')->with('success', 'Account Type has been activated');
        }
    }
}
