<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Menuroot;
use App\Http\Models\Menu;
use App\Http\Models\Menusub;
use App\Http\Models\Employeeset;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $menuroots = Menuroot::orderBy('nomor','asc')->get();
        $menus = Menu::orderBy('nomor','asc')->get();
        $menusubs = Menusub::orderBy('nomor','asc')->get();

        return view('vendor/adminlte/menu.index', compact('menuroots','menus','menusubs','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $menuroot = Menuroot::where('id', $id)->first();
        $menus = Menu::where('menuroot', $id)->orderBy('nomor','asc')->get();

        $no = Menu::select('nomor')->where('menuroot', $id)->max('nomor');
        if (empty($no)) {
            $nomor = 1;
        } else {
            $nomor = $no + 1;
        }

        return view('vendor/adminlte/menu.create', compact('menuroot','menus','nomor','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Menu::where([['menuroot', $request->get('menuroot')], ['nomor', $request->get('nomor')]])->first();

        if ($request->get('url') != '#') {
            $request->validate([
                'url' => 'unique:menus'
            ]);
        }

        $request->validate([
            'nomor' => 'numeric|min:0|not_in:0'
        ]);

        if (!empty($cek)) {
            return redirect()->back()->withErrors('Nomor already exist');
        } else {
            $menu = new Menu([
                'menuroot'      => $request->get('menuroot'),
                'name'          => $request->get('name'),
                'url'           => !empty($request->get('url')) ? $request->get('url') : '#',
                'icon'          => 'chevron-right',
                'icon_color'    => !empty($request->get('icon_color')) ? $request->get('icon_color') : 'yellow',
                'nomor'         => $request->get('nomor'),
                'created_user'  => $session_id,
                'updated_user'  => $session_id
            ]);
            $menu->save();

            return redirect('/menu/create/'.$request->get('menuroot'))->with('success', 'Menu has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $menu = Menu::find($id);
        $menuroots = Menuroot::where('status', 1)->orderBy('nomor','asc')->get();
        $menus = Menu::where('menuroot', $menu->menuroot)->orderBy('nomor','asc')->get();

        return view('vendor/adminlte/menu.edit', compact('menu','menuroots','menus','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
    	$menu = Menu::find($id);

        if ($menu->url != $request->get('url')) {
            if ($request->get('url') != '#') {
                $request->validate([
                    'url' => 'unique:menus'
                ]);
            }
        }

        $request->validate([
            'nomor' => 'numeric|min:0|not_in:0'
        ]);

        if ($request->get('menuroot') == $menu->menuroot) {
            if ($menu->nomor != $request->get('nomor')) {
                $ceks = Menu::where([['menuroot', $menu->menuroot], ['id','<>',$id], ['nomor','>=',$request->get('nomor')]])->orderBy('nomor','asc')->get();
                foreach ($ceks as $cek) {
                    $cek->nomor = $cek->nomor+1;
                    $cek->save();
                }

                $menu->name            = $request->get('name');
                $menu->url             = !empty($request->get('url')) ? $request->get('url') : '#';
                $menu->icon            = 'chevron-right';
                $menu->icon_color      = !empty($request->get('icon_color')) ? $request->get('icon_color') : 'yellow';
                $menu->nomor           = $request->get('nomor');
                $menu->updated_user    = $session_id;
                $menu->save();

                return redirect('/menu/create/'.$menu->menuroot)->with('success', 'Menu has been updated');
            } else {
                $menu->name            = $request->get('name');
                $menu->url             = !empty($request->get('url')) ? $request->get('url') : '#';
                $menu->icon            = 'chevron-right';
                $menu->icon_color      = !empty($request->get('icon_color')) ? $request->get('icon_color') : 'yellow';
                $menu->updated_user    = $session_id;
                $menu->save();

                return redirect('/menu/create/'.$menu->menuroot)->with('success', 'Menu has been updated');
            }
        } else {
            $cek = Menu::where('menuroot', $request->get('menuroot'))->orderBy('nomor','desc')->first();
            $nomor = $cek->nomor+1;

            $menu->menuroot        = $request->get('menuroot');
            $menu->nomor           = $nomor;
            $menu->updated_user    = $session_id;
            $menu->save();

            $employeesets = Employeeset::where('menu', $menu->id)->get();
            foreach ($employeesets as $employeeset) {
                $employeeset->delete();
            }

            return redirect('/menu/create/'.$menu->menuroot)->with('success', 'Menu has been updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $menu = Menu::find($id);
        $status = $menu->status;

        if ($status == 1) {
            $cek = Employeeset::where('menu', $id)->first();
            if (!empty($cek)) {
                return redirect()->back()->withErrors('Menu already used');
            } else {
                $menu->status       = 0;
                $menu->updated_user = $session_id;
                $menu->save();

                return redirect('/menu/create/'.$menu->menuroot)->with('error', 'Menu has been deleted');
            }
        } elseif ($status == 0) {
        	$menu->status       = 1;
            $menu->updated_user = $session_id;
	        $menu->save();

		    return redirect('/menu/create/'.$menu->menuroot)->with('success', 'Menu has been activated');
        }
    }
}
