<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Letter;
use App\Http\Models\Employeeset;

class LetterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $letters = Letter::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/letter.index', compact('letters','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        return view('vendor/adminlte/letter.create', compact('disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
    	
        $request->validate([
		    'code' => 'unique:letters'
		]);

        $letter = new Letter([
            'menu' 		    => $request->get('menu'),
            'company'       => $request->get('company'),
            'code'          => $request->get('code'),
            'romawi'        => ($request->get('romawi') == 1) ? 1 : 0,
            'year'          => ($request->get('year') == 1) ? 1 : 0,
            'created_user'  => $session_id,
            'updated_user'  => $session_id
	    ]);
	    $letter->save();

	    return redirect('/letter')->with('success', 'Letter has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $letter = Letter::find($id);

        return view('vendor/adminlte/letter.edit', compact('letter','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

    	$letter = Letter::find($id);
        $code = $letter->code;

        if ($code != $request->get('code')) {
            $request->validate([
                'code' => 'unique:letters'
            ]);
        }
        
        $letter->menu 	        = $request->get('menu');
        $letter->company        = $request->get('company');
        $letter->code           = $request->get('code');
        $letter->romawi         = ($request->get('romawi') == 1) ? 1 : 0;
        $letter->year           = ($request->get('year') == 1) ? 1 : 0;
	    $letter->updated_user 	= $session_id;
	    $letter->save();

	    return redirect('/letter')->with('success', 'Letter has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $letter = Letter::find($id);
        $status = $letter->status;

        if ($status == 1) {
        	$letter->status       = 0;
            $letter->updated_user = $session_id;
	        $letter->save();

		    return redirect('/letter')->with('error', 'Letter has been deleted');
        } elseif ($status == 0) {
        	$letter->status       = 1;
            $letter->updated_user = $session_id;
	        $letter->save();

		    return redirect('/letter')->with('success', 'Letter has been activated');
        }
    }
}
