<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Product;
use App\Http\Models\Konversi;
use App\Http\Models\Department;
use App\Http\Models\Unit;
use App\Http\Models\Company;
use App\Http\Models\Employeeset;

class KonversiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $konversis = Konversi::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/konversi.index', compact('konversis','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $units    = Unit::where('status', 1)->orderBy('name')->get();
        $products = Product::where('status', 1)->orderBy('name')->get();

        return view('vendor/adminlte/konversi.create', compact('products','units','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $request->validate([
            'name' => 'unique:product'
        ]);
    	
        $konversi = new Konversi([
            'product'          => $request->get('product'),
            'unit_src'          => $request->get('unit_src'),
            'unit_dst'          => $request->get('unit_dst'),
            'qty_result'          => $request->get('qty_result'),
            'keterangan'          => $request->get('keterangan'),
            'keterangan1'          => !empty($request->get('keterangan1')) ? $request->get('keterangan1') : 0,
            'keterangan2'          => !empty($request->get('keterangan2')) ? $request->get('keterangan2') : 0,
            'qty1'          => !empty($request->get('qty1')) ? $request->get('qty1') : 0,
            'qty2'          => !empty($request->get('qty2')) ? $request->get('qty2') : 0,
            'general'       => ($request->get('general') == 1) ? 1 : 0,
            'status_group'  => ($request->get('status_group') == 1) ? 1 : 0,
            'status_record' => ($request->get('status_record') == 1) ? 1 : 0,
            'created_user'  => $session_id,
            'updated_user'  => $session_id
        ]);
	    $konversi->save();

	    return redirect('/konversi/'.$konversi->id)->with('success', 'konversi has been added');

    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
    
        $konversis = Konversi::find($id);
        $products = Product::where('status', 1)->orderBy('name')->get();
        $units    = Unit::where('status', 1)->orderBy('name')->get();

        return view('vendor/adminlte/konversi.edit', compact('konversis','products','units','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $konversi = Konversi::find($id);

        if ($konversi->id != $request->get('id')) {
            $request->validate([
                'id' => 'unique:konversis'
            ]);
        }

            $konversi->product       = $request->get('product');
            $konversi->unit_src      = $request->get('unit_src');
            $konversi->unit_dst      = $request->get('unit_dst');
            $konversi->qty_result    = $request->get('qty_result');
            $konversi->keterangan    = $request->get('keterangan');
            $konversi->keterangan1   = !empty($request->get('keterangan1')) ? $request->get('keterangan1') : 0;
            $konversi->keterangan2   = !empty($request->get('keterangan2')) ? $request->get('keterangan2') : 0;
            $konversi->qty1          = !empty($request->get('qty1')) ? $request->get('qty1') : 0;
            $konversi->qty2          = !empty($request->get('qty2')) ? $request->get('qty2') : 0;
            $konversi->general       = ($request->get('general') == 1) ? 1 : 0;
            $konversi->status_group  = ($request->get('status_group') == 1) ? 1 : 0;
            $konversi->status_record = ($request->get('status_record') == 1) ? 1 : 0;
            $konversi->created_user  = $session_id;
            $konversi->updated_user  = $session_id;
      	    $konversi->save();

        return redirect('/konversi')->with('success', 'Konversi has been updated');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $konversi = Konversi::find($id);
        $status = $konversi->status;

        if ($status == 1) {
        	$konversi->status       = 0;
            $konversi->updated_user = $session_id;
	        $konversi->save();

		    return redirect('/konversi')->with('error', 'Konversi has been deleted');
        } elseif ($status == 0) {
        	$konversi->status       = 1;
            $konversi->updated_user = $session_id;
	        $konversi->save();

		    return redirect('/konversi')->with('success', 'Konversi has been activated');
        }
    }
}
