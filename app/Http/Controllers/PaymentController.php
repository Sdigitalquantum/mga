<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Payment;
use App\Http\Models\Employeeset;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $payments = Payment::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/payment.index', compact('payments','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        return view('vendor/adminlte/payment.create', compact('disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $payment = new Payment([
            'value' 		=> $request->get('value'),
	        'condition'	    => $request->get('condition'),
            'created_user'  => $session_id,
            'updated_user'  => $session_id
	    ]);
	    $payment->save();

	    return redirect('/payment')->with('success', 'Payment has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $payment = Payment::find($id);

        return view('vendor/adminlte/payment.edit', compact('payment','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

    	$payment = Payment::find($id);
        $payment->value 	    = $request->get('value');
	    $payment->condition     = $request->get('condition');
	    $payment->updated_user 	= $session_id;
	    $payment->save();

	    return redirect('/payment')->with('success', 'Payment has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $payment = Payment::find($id);
        $status = $payment->status;

        if ($status == 1) {
        	$payment->status       = 0;
            $payment->updated_user = $session_id;
	        $payment->save();

		    return redirect('/payment')->with('error', 'Payment has been deleted');
        } elseif ($status == 0) {
        	$payment->status       = 1;
            $payment->updated_user = $session_id;
	        $payment->save();

		    return redirect('/payment')->with('success', 'Payment has been activated');
        }
    }
}
