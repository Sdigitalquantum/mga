<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Quotation;
use App\Http\Models\Quotationdetail;
use App\Http\Models\Quotationsplit;
use App\Http\Models\Quotationplan;
use App\Http\Models\Quotationpayment;
use App\Http\Models\Quotationcharge;
use App\Http\Models\Plan;
use App\Http\Models\Company;
use App\Http\Models\Employeeset;
use App\Http\Models\Employeewarehouse;

class QuotationplanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $quotations = Quotationsplit::where([['status','<>',0], ['status_release', 1]])->orderBy('created_at','desc')->get();
        
        return view('vendor/adminlte/quotationplan.index', compact('quotations','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $quotationsplit = Quotationsplit::find($id);
        $quotationsplit->qty_print = $quotationsplit->qty_print+1;
        $quotationsplit->save();

        $quotationplan = Quotationplan::where('quotationsplit', $id)->first();
        $plan = Plan::where('id', $quotationplan->plan)->first();
        $quotationdetail = Quotationdetail::where('id', $plan->quotationdetail)->first();

        $quotationplans = Quotationplan::where('quotationsplit', $id)->get();
        $plans = Plan::where('status_split', 1)->get();

        $quotation = Quotation::where('id', $quotationsplit->quotation)->first();
        $quotationdetails = Quotationdetail::where([['id', $plan->quotationdetail], ['status', 1]])->get();
        $quotationpayments = Quotationpayment::where([['quotation', $quotation->id], ['status', 1]])->orderBy('payment','desc')->get();
        $quotationcharges = Quotationcharge::where([['quotation', $quotation->id], ['status', 1]])->get();
        $company = Company::where('status', 1)->first();
        
        if ($quotationdetail->qty_amount != 0) {
            return redirect()->back()->withErrors('Qty Need is not complete');
        } else {
            return view('vendor/adminlte/quotationplan.show', compact('id','quotationsplit','quotationplans','plans','quotation','quotationdetails','quotationpayments','quotationcharges','company','disable','employeeroots','employeemenus','employeesubs'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $quotationsplit = Quotationsplit::where('id', $id)->first();
        $quotationplans = Quotationplan::where('quotationsplit', $id)->get();
        $quotationdetails = Quotationdetail::where('quotation', $quotationsplit->quotation)->get();
        $plans = Plan::where('status_split', 0)->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/quotationplan.edit', compact('quotationsplit','quotationplans','quotationdetails','plans','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $quotationplan = new Quotationplan([
            'quotationsplit'    => $request->get('quotationsplit'),
            'plan'              => $request->get('plan'),
            'created_user'      => $session_id,
            'updated_user'      => $session_id
        ]);
        $quotationplan->save();

        $plan = Plan::find($request->get('plan'));
        $plan->status_split = 1;
        $plan->save();

	    return redirect('/quotationplan/'.$id.'/edit')->with('success', 'Quotation Plan has been added');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quotationplan = Quotationplan::find($id);
        $quotationplan->delete();

        $plan = Plan::where('id', $quotationplan->plan)->first();
        $plan->status_split = 0;
        $plan->save();

	    return redirect('/quotationplan/'.$quotationplan->quotationsplit.'/edit')->with('error', 'Quotation Plan has been deleted');
    }
}
