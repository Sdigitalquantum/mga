<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Customerdetail;
use App\Http\Models\City;
use App\Http\Models\Employeeset;

class CustomerdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $customerdetail = new Customerdetail([
            'customer'      => $request->get('customer'),
            'city'          => $request->get('city'),
            'name'          => $request->get('name'),
            'address'       => $request->get('address'),
            'mobile'        => $request->get('mobile'),
            'alias_name'    => !empty($request->get('alias_name')) ? $request->get('alias_name') : '',
            'alias_mobile'  => !empty($request->get('alias_mobile')) ? $request->get('alias_mobile') : '',
            'created_user'  => $session_id,
            'updated_user'  => $session_id
	    ]);
	    $customerdetail->save();

	    return redirect('/customerdetail/'.$request->get('customer'))->with('success', 'Customer Detail has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $customerdetails = Customerdetail::where('customer', $id)->orderBy('created_at','desc')->get();
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/customerdetail.show', compact('id','customerdetails','citys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $customerdetail = Customerdetail::find($id);
        $customerdetails = Customerdetail::where('customer', $customerdetail->customer)->orderBy('created_at','desc')->get();
        $citys = City::where('status', 1)->get();

        return view('vendor/adminlte/customerdetail.edit', compact('customerdetail','customerdetails','citys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $customerdetail = Customerdetail::find($id);
        $customerdetail->customer      = $request->get('customer');
        $customerdetail->city          = $request->get('city');
        $customerdetail->name          = $request->get('name');
        $customerdetail->address       = $request->get('address');
        $customerdetail->mobile        = $request->get('mobile');
        $customerdetail->alias_name    = !empty($request->get('alias_name')) ? $request->get('alias_name') : '';
        $customerdetail->alias_mobile  = !empty($request->get('alias_mobile')) ? $request->get('alias_mobile') : '';
	    $customerdetail->updated_user  = $session_id;
	    $customerdetail->save();

	    return redirect('/customerdetail/'.$request->get('customer'))->with('success', 'Customer Detail has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $customerdetail = Customerdetail::find($id);
        $status = $customerdetail->status;

        if ($status == 1) {
        	$customerdetail->status       = 0;
            $customerdetail->updated_user = $session_id;
	        $customerdetail->save();

		    return redirect('/customerdetail/'.$customerdetail->customer)->with('error', 'Customer Detail has been deleted');
        } elseif ($status == 0) {
        	$customerdetail->status       = 1;
            $customerdetail->updated_user = $session_id;
	        $customerdetail->save();

		    return redirect('/customerdetail/'.$customerdetail->customer)->with('success', 'Customer Detail has been activated');
        }
    }
}
