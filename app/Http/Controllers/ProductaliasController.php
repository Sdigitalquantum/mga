<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Productalias;
use App\Http\Models\Customerdetail;
use App\Http\Models\Employeeset;

class ProductaliasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Productalias::where([['customerdetail', $request->get('customerdetail')], ['product', $request->get('product')]])->first();

        if (!empty($cek)) {
            return redirect()->back()->withErrors('Product Alias already exists');
        } else {
            $productalias = new Productalias([
                'product'           => $request->get('product'),
                'customerdetail'    => $request->get('customerdetail'),
                'alias_name'        => !empty($request->get('alias_name')) ? $request->get('alias_name') : '',
                'created_user'      => $session_id,
                'updated_user'      => $session_id
            ]);
            $productalias->save();

            return redirect('/productalias/'.$request->get('product'))->with('success', 'Product Alias has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $productaliass = Productalias::where('product', $id)->orderBy('created_at','desc')->get();
        $customerdetails = Customerdetail::where('status', 1)->get();

        return view('vendor/adminlte/productalias.show', compact('id','productaliass','customerdetails','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $productalias = Productalias::find($id);
        $productaliass = Productalias::where('product', $productalias->product)->orderBy('created_at','desc')->get();
        $customerdetails = Customerdetail::where('status', 1)->get();

        return view('vendor/adminlte/productalias.edit', compact('productalias','productaliass','customerdetails','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $productalias = Productalias::find($id);

        if ($productalias->customerdetail != $request->get('customerdetail')) {
            $cek = Productalias::where([['customerdetail', $request->get('customerdetail')], ['product', $request->get('product')]])->first();

            if (!empty($cek)) {
                return redirect()->back()->withErrors('Product Alias already exists');
            } else {
                $productalias->product          = $request->get('product');
                $productalias->customerdetail   = $request->get('customerdetail');
                $productalias->alias_name       = !empty($request->get('alias_name')) ? $request->get('alias_name') : '';
                $productalias->updated_user     = $session_id;
                $productalias->save();

                return redirect('/productalias/'.$request->get('product'))->with('success', 'Product Alias has been updated');
            }
        } else {
            $productalias->product          = $request->get('product');
            $productalias->customerdetail   = $request->get('customerdetail');
            $productalias->alias_name       = !empty($request->get('alias_name')) ? $request->get('alias_name') : '';
            $productalias->updated_user     = $session_id;
            $productalias->save();

            return redirect('/productalias/'.$request->get('product'))->with('success', 'Product Alias has been updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $productalias = Productalias::find($id);
        $status = $productalias->status;

        if ($status == 1) {
        	$productalias->status       = 0;
            $productalias->updated_user = $session_id;
	        $productalias->save();

		    return redirect('/productalias/'.$productalias->product)->with('error', 'Product Alias has been deleted');
        } elseif ($status == 0) {
        	$productalias->status       = 1;
            $productalias->updated_user = $session_id;
	        $productalias->save();

		    return redirect('/productalias/'.$productalias->product)->with('success', 'Product Alias has been activated');
        }
    }
}
