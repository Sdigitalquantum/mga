<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Pricelist;
use App\Http\Models\Product;
use App\Http\Models\Currency;
use App\Http\Models\Employeeset;

class PricelistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $pricelists = Pricelist::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/pricelist.index', compact('pricelists','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $products = Product::where('status', 1)->get();
        $currencys = Currency::where('status', 1)->get();

        return view('vendor/adminlte/pricelist.create', compact('products','currencys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Pricelist::where('product', $request->get('product'))->first();
        
        if (!empty($cek)) {
            return redirect()->back()->withErrors('Price List already exist');
        } else {
            $pricelist = new Pricelist([
                'product'       => $request->get('product'),
                'currency'      => $request->get('currency'),
                'price'         => $request->get('price'),
                'date'          => $request->get('date'),
                'created_user'  => $session_id,
                'updated_user'  => $session_id
            ]);
            $pricelist->save();

            return redirect('/pricelist')->with('success', 'Price List has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $pricelist = Pricelist::find($id);
        $products = Product::where('status', 1)->get();
        $currencys = Currency::where('status', 1)->get();

        return view('vendor/adminlte/pricelist.edit', compact('pricelist','products','currencys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
    	$pricelist = Pricelist::find($id);

        if ($pricelist->product == $request->get('product')) {
            $pricelist->currency        = $request->get('currency');
            $pricelist->price           = $request->get('price');
            $pricelist->date            = $request->get('date');
            $pricelist->updated_user    = $session_id;
            $pricelist->save();

            return redirect('/pricelist')->with('success', 'Price List has been updated');
        } else {
            $cek = Pricelist::where('product', $request->get('product'))->first();
        
            if (!empty($cek)) {
                return redirect()->back()->withErrors('Price List already exist');
            } else {
                $pricelist->product         = $request->get('product');
                $pricelist->currency        = $request->get('currency');
                $pricelist->price           = $request->get('price');
                $pricelist->date            = $request->get('date');
                $pricelist->updated_user    = $session_id;
                $pricelist->save();

                return redirect('/pricelist')->with('success', 'Price List has been updated');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $pricelist = Pricelist::find($id);
        $status = $pricelist->status;

        if ($status == 1) {
        	$pricelist->status       = 0;
            $pricelist->updated_user = $session_id;
	        $pricelist->save();

		    return redirect('/pricelist')->with('error', 'Price List has been deleted');
        } elseif ($status == 0) {
        	$pricelist->status       = 1;
            $pricelist->updated_user = $session_id;
	        $pricelist->save();

		    return redirect('/pricelist')->with('success', 'Price List has been activated');
        }
    }
}
