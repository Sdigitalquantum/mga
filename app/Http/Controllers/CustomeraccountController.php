<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Customeraccount;
use App\Http\Models\Currency;
use App\Http\Models\Bank;
use App\Http\Models\Employeeset;

class CustomeraccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $customeraccount = new Customeraccount([
            'customer'      => $request->get('customer'),
            'currency'      => $request->get('currency'),
            'bank'          => $request->get('bank'),
            'account_no'    => $request->get('account_no'),
            'account_swift' => $request->get('account_swift'),
            'account_name'  => $request->get('account_name'),
            'alias'         => !empty($request->get('alias')) ? $request->get('alias') : '',
            'created_user'  => $session_id,
            'updated_user'  => $session_id
	    ]);
	    $customeraccount->save();

	    return redirect('/customeraccount/'.$request->get('customer'))->with('success', 'Customer Account has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $customeraccounts = Customeraccount::where('customer', $id)->orderBy('created_at','desc')->get();
        $currencys = Currency::where('status', 1)->get();
        $banks = Bank::where('status', 1)->get();

        return view('vendor/adminlte/customeraccount.show', compact('id','customeraccounts','currencys','banks','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $customeraccount = Customeraccount::find($id);
        $customeraccounts = Customeraccount::where('customer', $customeraccount->customer)->orderBy('created_at','desc')->get();
        $currencys = Currency::where('status', 1)->get();
        $banks = Bank::where('status', 1)->get();

        return view('vendor/adminlte/customeraccount.edit', compact('customeraccount','customeraccounts','currencys','banks','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $customeraccount = Customeraccount::find($id);
        $customeraccount->customer      = $request->get('customer');
        $customeraccount->currency      = $request->get('currency');
        $customeraccount->bank          = $request->get('bank');
        $customeraccount->account_no    = $request->get('account_no');
        $customeraccount->account_swift = $request->get('account_swift');
        $customeraccount->account_name  = $request->get('account_name');
        $customeraccount->alias         = !empty($request->get('alias')) ? $request->get('alias') : '';
	    $customeraccount->updated_user  = $session_id;
	    $customeraccount->save();

	    return redirect('/customeraccount/'.$request->get('customer'))->with('success', 'Customer Account has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $customeraccount = Customeraccount::find($id);
        $status = $customeraccount->status;

        if ($status == 1) {
        	$customeraccount->status       = 0;
            $customeraccount->updated_user = $session_id;
	        $customeraccount->save();

		    return redirect('/customeraccount/'.$customeraccount->customer)->with('error', 'Customer Account has been deleted');
        } elseif ($status == 0) {
        	$customeraccount->status       = 1;
            $customeraccount->updated_user = $session_id;
	        $customeraccount->save();

		    return redirect('/customeraccount/'.$customeraccount->customer)->with('success', 'Customer Account has been activated');
        }
    }
}
