<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Purchasedetail;
use App\Http\Models\Stockqc;
use App\Http\Models\Stockin;
use App\Http\Models\Product;
use App\Http\Models\Warehouse;
use App\Http\Models\Supplierdetail;
use App\Http\Models\Letter;
use App\Http\Models\Employeeset;
use App\Http\Models\Employeewarehouse;

class StockqcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $purchasedetails = Purchasedetail::orderBy('created_at','desc')->get();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/stockqc.index', compact('purchasedetails','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $purchasedetail = Purchasedetail::find($id);
        $stockqcs = Stockqc::where('purchasedetail', $id)->get();
        $products = Product::where('status', 1)->get();
        $warehouses = Warehouse::where('status', 1)->get();
        $supplierdetail = Supplierdetail::where('id', $purchasedetail->supplierdetail)->first();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/stockqc.create', compact('purchasedetail','stockqcs','products','warehouses','supplierdetail','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Stockqc::where([['purchasedetail', $request->get('purchasedetail')], ['product', $request->get('product')], ['warehouse', $request->get('warehouse')], ['supplierdetail', $request->get('supplierdetail')], ['date_in', $request->get('date_in')]])->first();
        $purchasedetail = Purchasedetail::find($request->get('purchasedetail'));

        $request->validate([
            'qty_bag' => 'numeric|min:0|not_in:0',
            'qty_pcs' => 'numeric|min:0|not_in:0',
            'qty_kg'  => 'numeric|min:0|not_in:0'
        ]);

        if (!empty($cek)) {
            return redirect()->back()->withErrors('Quality Control already exist');
        } elseif ($request->get('qty_kg') > ($purchasedetail->qty_order+$purchasedetail->qty_buffer-$purchasedetail->qty_qc)) {
            return redirect()->back()->withErrors('Qty Kg greater than Qty QC');
        } else {
            if ($request->get('product') == $purchasedetail->product) {
                $no_inc = 0;
                $no_po  = $purchasedetail->fkPurchaseorder->no_po; 
            } else {
                $no = Stockqc::select('no_inc')->where('purchasedetail', $request->get('purchasedetail'))->max('no_inc');
                $no_inc = $no + 1;

                if ($no_inc == 1) { $code = "A"; }
                elseif ($no_inc == 2) { $code = "B"; }
                elseif ($no_inc == 3) { $code = "C"; }
                elseif ($no_inc == 4) { $code = "D"; }
                elseif ($no_inc == 5) { $code = "E"; }
                elseif ($no_inc == 6) { $code = "F"; }
                elseif ($no_inc == 7) { $code = "G"; }
                elseif ($no_inc == 8) { $code = "H"; }
                elseif ($no_inc == 9) { $code = "I"; }
                elseif ($no_inc == 10) { $code = "J"; }
                elseif ($no_inc == 11) { $code = "K"; }
                elseif ($no_inc == 12) { $code = "L"; }
                elseif ($no_inc == 13) { $code = "M"; }
                elseif ($no_inc == 14) { $code = "N"; }
                elseif ($no_inc == 15) { $code = "O"; }
                elseif ($no_inc == 16) { $code = "P"; }
                elseif ($no_inc == 17) { $code = "Q"; }
                elseif ($no_inc == 18) { $code = "R"; }
                elseif ($no_inc == 19) { $code = "S"; }
                elseif ($no_inc == 20) { $code = "T"; }
                elseif ($no_inc == 21) { $code = "U"; }
                elseif ($no_inc == 22) { $code = "V"; }
                elseif ($no_inc == 23) { $code = "W"; }
                elseif ($no_inc == 24) { $code = "X"; }
                elseif ($no_inc == 25) { $code = "Y"; }
                elseif ($no_inc == 26) { $code = "Z"; }

                $no_po = $purchasedetail->fkPurchaseorder->no_po.'-'.$code;
            }
            
            $stockqc = new Stockqc([
                'purchasedetail'    => $request->get('purchasedetail'),
                'product'           => $request->get('product'),
                'warehouse'         => $request->get('warehouse'),
                'supplierdetail'    => $request->get('supplierdetail'),
                'no_inc'            => $no_inc,
                'no_po'             => $no_po,
                'date_in'           => $request->get('date_in'),
                'qty_bag'           => $request->get('qty_bag'),
                'qty_pcs'           => $request->get('qty_pcs'),
                'qty_kg'            => $request->get('qty_kg'),
                'qty_avg'           => $request->get('qty_kg')/$request->get('qty_bag'),
                'price'             => $purchasedetail->price,
                'notice'            => !empty($request->get('notice')) ? $request->get('notice') : '',
                'reason'            => '',
                'created_user'      => $session_id,
                'updated_user'      => $session_id
            ]);
            $stockqc->save();

            $purchasedetail->qty_qc     = $purchasedetail->qty_qc+$request->get('qty_kg');
            $purchasedetail->status_qc  = 1;
            $purchasedetail->save();

            if ($no_inc == 0) {
                $no = Stockin::select('no_inc')->whereYear('created_at', date('Y'))->max('no_inc');
                if (empty($no)) {
                    $no_stockin = 1;
                } else {
                    $no_stockin = $no + 1;
                }

                $letter = Letter::where('menu', 'Incoming Stock')->first();
                if (empty($letter->company)) {
                    $company = "";
                } else {
                    $company = $letter->company."/";
                }

                if (empty($letter->code)) {
                    $code = "";
                } else {
                    $code = $letter->code;
                }

                if ($letter->romawi == 0) {
                    $romawi = "";
                } else {
                    if (date("m") == 1) {
                        $romawi = "I/";
                    } elseif (date("m") == 2) {
                        $romawi = "II/";
                    } elseif (date("m") == 3) {
                        $romawi = "III/";
                    } elseif (date("m") == 4) {
                        $romawi = "IV/";
                    } elseif (date("m") == 5) {
                        $romawi = "V/";
                    } elseif (date("m") == 6) {
                        $romawi = "VI/";
                    } elseif (date("m") == 7) {
                        $romawi = "VII/";
                    } elseif (date("m") == 8) {
                        $romawi = "VIII/";
                    } elseif (date("m") == 9) {
                        $romawi = "IX/";
                    } elseif (date("m") == 10) {
                        $romawi = "X/";
                    } elseif (date("m") == 11) {
                        $romawi = "XI/";
                    } elseif (date("m") == 12) {
                        $romawi = "XII/";
                    }
                }

                if ($letter->year == 0) {
                    $year = "";
                } else {
                    $year = date("Y");
                }

                $nomor = $company.$code.sprintf("%05s", $no_stockin)."/".$romawi.$year;

                $stockin = new Stockin([
                    'transaction'       => 1,
                    'product'           => $request->get('product'),
                    'warehouse'         => $request->get('warehouse'),
                    'supplierdetail'    => $request->get('supplierdetail'),
                    'no_inc'            => $no_stockin,
                    'nomor'             => $nomor,
                    'date_in'           => $request->get('date_in'),
                    'price'             => $purchasedetail->price,
                    'qty_bag'           => $request->get('qty_bag'),
                    'qty_pcs'           => $request->get('qty_pcs'),
                    'qty_kg'            => $request->get('qty_kg'),
                    'qty_available'     => $request->get('qty_kg'),
                    'noref_in'          => $no_po,
                    'transaction_prod'  => 0,
                    'status_qc'         => 1,
                    'status_move'       => 0,
                    'qty_qc'            => $request->get('qty_kg'),
                    'created_user'      => $session_id,
                    'updated_user'      => $session_id
                ]);
                $stockin->save();
            }

            return redirect('/stockqc/create/'.$request->get('purchasedetail'))->with('success', 'Quality Control has been added');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $stockqc = Stockqc::find($id);
        $stockqcs = Stockqc::where('purchasedetail', $stockqc->purchasedetail)->get();
        $products = Product::where('status', 1)->get();
        $warehouses = Warehouse::where('status', 1)->get();
        $supplierdetail = Supplierdetail::where('id', $stockqc->supplierdetail)->first();
        $employeewarehouses = Employeewarehouse::where('employee', $session_id)->get();

        return view('vendor/adminlte/stockqc.edit', compact('stockqc','stockqcs','products','warehouses','supplierdetail','employeewarehouses','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $stockqc = Stockqc::find($id);
        $purchasedetail = Purchasedetail::find($request->get('purchasedetail'));
        
        $request->validate([
            'qty_bag' => 'numeric|min:0|not_in:0',
            'qty_pcs' => 'numeric|min:0|not_in:0',
            'qty_kg'  => 'numeric|min:0|not_in:0'
        ]);

        if ($request->get('purchasedetail') == $stockqc->purchasedetail && $request->get('product') == $stockqc->product && $request->get('warehouse') == $stockqc->warehouse && $request->get('supplierdetail') == $stockqc->supplierdetail && $request->get('date_in') == $stockqc->date_in) {
            if ($request->get('qty_kg') > ($purchasedetail->qty_order+$purchasedetail->qty_buffer-$purchasedetail->qty_qc+$stockqc->qty_kg)) {
                return redirect()->back()->withErrors('Qty Kg greater than Qty QC');
            } else {
                $stockqc->product           = $request->get('product');
                $stockqc->warehouse         = $request->get('warehouse');
                $stockqc->supplierdetail    = $request->get('supplierdetail');
                $stockqc->date_in           = $request->get('date_in');
                $stockqc->qty_bag           = $request->get('qty_bag');
                $stockqc->qty_pcs           = $request->get('qty_pcs');
                $stockqc->qty_kg            = $request->get('qty_kg');
                if ($stockqc->detail_count == 0) {
                    $stockqc->qty_avg = $request->get('qty_kg') / $request->get('qty_bag');
                }
                $stockqc->notice            = !empty($request->get('notice')) ? $request->get('notice') : '';
                $stockqc->updated_user      = $session_id;
                $stockqc->save();

                $purchasedetail->qty_qc     = $purchasedetail->qty_qc+$request->get('qty_kg');
                $purchasedetail->save();

                if ($stockqc->no_inc == 0) {
                    $stockin = Stockin::where([['product', $request->get('product')], ['warehouse', $request->get('warehouse')], ['supplierdetail', $request->get('supplierdetail')], ['date_in', $request->get('date_in')]])->first();
                    $stockin->qty_bag           = $request->get('qty_bag');
                    $stockin->qty_pcs           = $request->get('qty_pcs');
                    $stockin->qty_kg            = $request->get('qty_kg');
                    $stockin->qty_available     = $request->get('qty_kg');
                    $stockin->status_qc         = 1;
                    $stockin->qty_qc            = $request->get('qty_kg');
                    $stockin->updated_user      = $session_id;
                    $stockin->save();
                }

                return redirect('/stockqc/create/'.$stockqc->purchasedetail)->with('success', 'Quality Control has been updated');
            }
        } else {
            $cek = Stockqc::where([['purchasedetail', $request->get('purchasedetail')], ['product', $request->get('product')], ['warehouse', $request->get('warehouse')], ['supplierdetail', $request->get('supplierdetail')], ['date_in', $request->get('date_in')]])->first();

            if (!empty($cek)) {
                return redirect()->back()->withErrors('Quality Control already exist');
            } elseif ($request->get('qty_kg') > ($purchasedetail->qty_order+$purchasedetail->qty_buffer-$purchasedetail->qty_qc+$stockqc->qty_kg)) {
                return redirect()->back()->withErrors('Qty Kg greater than Qty QC');
            } else {
                if ($request->get('product') == $purchasedetail->product) {
                    $no_inc = 0;
                    $no_po  = $purchasedetail->fkPurchaseorder->no_po; 
                } else {
                    if ($request->get('product') == $stockqc->product) {
                        $no_inc = $stockqc->no_inc;
                        $no_po  = $stockqc->no_po;
                    } else {
                        $no = Stockqc::select('no_inc')->where('purchasedetail', $request->get('purchasedetail'))->max('no_inc');
                        $no_inc = $no + 1;

                        if ($no_inc == 1) { $code = "A"; }
                        elseif ($no_inc == 2) { $code = "B"; }
                        elseif ($no_inc == 3) { $code = "C"; }
                        elseif ($no_inc == 4) { $code = "D"; }
                        elseif ($no_inc == 5) { $code = "E"; }
                        elseif ($no_inc == 6) { $code = "F"; }
                        elseif ($no_inc == 7) { $code = "G"; }
                        elseif ($no_inc == 8) { $code = "H"; }
                        elseif ($no_inc == 9) { $code = "I"; }
                        elseif ($no_inc == 10) { $code = "J"; }
                        elseif ($no_inc == 11) { $code = "K"; }
                        elseif ($no_inc == 12) { $code = "L"; }
                        elseif ($no_inc == 13) { $code = "M"; }
                        elseif ($no_inc == 14) { $code = "N"; }
                        elseif ($no_inc == 15) { $code = "O"; }
                        elseif ($no_inc == 16) { $code = "P"; }
                        elseif ($no_inc == 17) { $code = "Q"; }
                        elseif ($no_inc == 18) { $code = "R"; }
                        elseif ($no_inc == 19) { $code = "S"; }
                        elseif ($no_inc == 20) { $code = "T"; }
                        elseif ($no_inc == 21) { $code = "U"; }
                        elseif ($no_inc == 22) { $code = "V"; }
                        elseif ($no_inc == 23) { $code = "W"; }
                        elseif ($no_inc == 24) { $code = "X"; }
                        elseif ($no_inc == 25) { $code = "Y"; }
                        elseif ($no_inc == 26) { $code = "Z"; }

                        $no_po = $purchasedetail->fkPurchaseorder->no_po.'-'.$code;
                    }
                }

                $stockqc->product           = $request->get('product');
                $stockqc->warehouse         = $request->get('warehouse');
                $stockqc->supplierdetail    = $request->get('supplierdetail');
                $stockqc->no_inc            = $no_inc;
                $stockqc->no_po             = $no_po;
                $stockqc->date_in           = $request->get('date_in');
                $stockqc->qty_bag           = $request->get('qty_bag');
                $stockqc->qty_pcs           = $request->get('qty_pcs');
                $stockqc->qty_kg            = $request->get('qty_kg');
                if ($stockqc->detail_count == 0) {
                    $stockqc->qty_avg = $request->get('qty_kg') / $request->get('qty_bag');
                }
                $stockqc->notice            = !empty($request->get('notice')) ? $request->get('notice') : '';
                $stockqc->updated_user      = $session_id;
                $stockqc->save();

                $purchasedetail->qty_qc     = $purchasedetail->qty_qc+$request->get('qty_kg');
                $purchasedetail->save();

                if ($stockqc->no_inc == 0) {
                    $stockin = Stockin::where([['product', $request->get('product')], ['warehouse', $request->get('warehouse')], ['supplierdetail', $request->get('supplierdetail')], ['date_in', $request->get('date_in')]])->first();
                    $stockin->product           = $request->get('product');
                    $stockin->warehouse         = $request->get('warehouse');
                    $stockin->supplierdetail    = $request->get('supplierdetail');
                    $stockin->date_in           = $request->get('date_in');
                    $stockin->qty_bag           = $request->get('qty_bag');
                    $stockin->qty_pcs           = $request->get('qty_pcs');
                    $stockin->qty_kg            = $request->get('qty_kg');
                    $stockin->qty_available     = $request->get('qty_kg');
                    $stockin->status_qc         = 1;
                    $stockin->qty_qc            = $request->get('qty_kg');
                    $stockin->noref_in          = $no_po;
                    $stockin->updated_user      = $session_id;
                    $stockin->save();
                }

                return redirect('/stockqc/create/'.$stockqc->purchasedetail)->with('success', 'Quality Control has been updated');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $stockqc = Stockqc::find($id);
        $status = $stockqc->status;

        if ($status == 1) {
        	$stockqc->status       = 0;
            $stockqc->updated_user = $session_id;
	        $stockqc->save();

            $stockin = Stockin::where([['product', $stockqc->product], ['warehouse', $stockqc->warehouse], ['supplierdetail', $stockqc->supplierdetail], ['date_in', $stockqc->date_in]])->first();
            if (!empty($stockin)) {
                $stockin->status       = 0;
                $stockin->updated_user = $session_id;
                $stockin->save();
            }

		    return redirect('/stockqc/create/'.$stockqc->purchasedetail)->with('error', 'Quality Control has been deleted');
        } elseif ($status == 0) {
        	$stockqc->status       = 1;
            $stockqc->updated_user = $session_id;
	        $stockqc->save();

            $stockin = Stockin::where([['product', $stockqc->product], ['warehouse', $stockqc->warehouse], ['supplierdetail', $stockqc->supplierdetail], ['date_in', $stockqc->date_in]])->first();
            if (!empty($stockin)) {
                $stockin->status       = 1;
                $stockin->updated_user = $session_id;
                $stockin->save();
            }

		    return redirect('/stockqc/create/'.$stockqc->purchasedetail)->with('success', 'Quality Control has been activated');
        }
    }
}
