<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Supplierdetail;
use App\Http\Models\City;
use App\Http\Models\Employeeset;

class SupplierdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $supplierdetail = new Supplierdetail([
            'supplier'      => $request->get('supplier'),
            'city'          => $request->get('city'),
            'name'          => $request->get('name'),
            'address'       => $request->get('address'),
            'mobile'        => $request->get('mobile'),
            'alias_name'    => !empty($request->get('alias_name')) ? $request->get('alias_name') : '',
            'alias_mobile'  => !empty($request->get('alias_mobile')) ? $request->get('alias_mobile') : '',
            'created_user'  => $session_id,
            'updated_user'  => $session_id
	    ]);
	    $supplierdetail->save();

	    return redirect('/supplierdetail/'.$request->get('supplier'))->with('success', 'Supplier Detail has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $supplierdetails = Supplierdetail::where('supplier', $id)->orderBy('created_at','desc')->get();
        $citys = City::where('status', 1)->orderBy('name', 'asc')->get();

        return view('vendor/adminlte/supplierdetail.show', compact('id','supplierdetails','citys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $supplierdetail = Supplierdetail::find($id);
        $supplierdetails = Supplierdetail::where('supplier', $supplierdetail->supplier)->orderBy('created_at','desc')->get();
        $citys = City::where('status', 1)->orderBy('name', 'asc')->get();

        return view('vendor/adminlte/supplierdetail.edit', compact('supplierdetail','supplierdetails','citys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $supplierdetail = Supplierdetail::find($id);
        $supplierdetail->supplier      = $request->get('supplier');
        $supplierdetail->city          = $request->get('city');
        $supplierdetail->name          = $request->get('name');
        $supplierdetail->address       = $request->get('address');
        $supplierdetail->mobile        = $request->get('mobile');
        $supplierdetail->alias_name    = !empty($request->get('alias_name')) ? $request->get('alias_name') : '';
        $supplierdetail->alias_mobile  = !empty($request->get('alias_mobile')) ? $request->get('alias_mobile') : '';
	    $supplierdetail->updated_user  = $session_id;
	    $supplierdetail->save();

	    return redirect('/supplierdetail/'.$request->get('supplier'))->with('success', 'Supplier Detail has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $supplierdetail = Supplierdetail::find($id);
        $status = $supplierdetail->status;

        if ($status == 1) {
        	$supplierdetail->status       = 0;
            $supplierdetail->updated_user = $session_id;
	        $supplierdetail->save();

		    return redirect('/supplierdetail/'.$supplierdetail->supplier)->with('error', 'Supplier Detail has been deleted');
        } elseif ($status == 0) {
        	$supplierdetail->status       = 1;
            $supplierdetail->updated_user = $session_id;
	        $supplierdetail->save();

		    return redirect('/supplierdetail/'.$supplierdetail->supplier)->with('success', 'Supplier Detail has been activated');
        }
    }
}
