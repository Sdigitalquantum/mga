<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Materialother;
use App\Http\Models\Product;
use App\Http\Models\Unit;
use App\Http\Models\Employeeset;

class MaterialotherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $cek = Materialother::where([['material', $request->get('material')], ['product', $request->get('product')]])->first();

        if (!empty($cek)) {
            return redirect()->back()->withErrors('Material Other already exists');
        } else {
            $materialother = new Materialother([
                'material'          => $request->get('material'),
                'product'           => $request->get('product'),
                'unit'              => $request->get('unit'),
                'qty'               => $request->get('qty'),
                'created_user'      => $session_id,
                'updated_user'      => $session_id
            ]);
            $materialother->save();

            return redirect('/materialother/'.$request->get('material'))->with('success', 'Material Other has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();

        $materialothers = Materialother::where('material', $id)->orderBy('created_at','desc')->get();
        $products = Product::where('status', 1)->get();
        $units = Unit::where('status', 1)->get();

        return view('vendor/adminlte/materialother.show', compact('id','materialothers','products','units','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $materialother = Materialother::find($id);
        $materialothers = Materialother::where('material', $materialother->material)->orderBy('created_at','desc')->get();
        $products = Product::where('status', 1)->get();
        $units = Unit::where('status', 1)->get();

        return view('vendor/adminlte/materialother.edit', compact('materialother','materialothers','products','units','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $materialother = Materialother::find($id);

        if ($materialother->product != $request->get('product')) {
            $cek = Materialother::where([['material', $request->get('material')], ['product', $request->get('product')]])->first();

            if (!empty($cek)) {
                return redirect()->back()->withErrors('Material Other already exists');
            } else {
                $materialother->material         = $request->get('material');
                $materialother->product          = $request->get('product');
                $materialother->unit             = $request->get('unit');
                $materialother->qty              = $request->get('qty');
                $materialother->updated_user     = $session_id;
                $materialother->save();

                return redirect('/materialother/'.$request->get('material'))->with('success', 'Material Other has been updated');
            }
        } else {
            $materialother->material         = $request->get('material');
            $materialother->product          = $request->get('product');
            $materialother->unit             = $request->get('unit');
            $materialother->qty              = $request->get('qty');
            $materialother->updated_user     = $session_id;
            $materialother->save();

            return redirect('/materialother/'.$request->get('material'))->with('success', 'Material Other has been updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $materialother = Materialother::find($id);
        $status = $materialother->status;

        if ($status == 1) {
        	$materialother->status       = 0;
            $materialother->updated_user = $session_id;
	        $materialother->save();

		    return redirect('/materialother/'.$materialother->material)->with('error', 'Material Other has been deleted');
        } elseif ($status == 0) {
        	$materialother->status       = 1;
            $materialother->updated_user = $session_id;
	        $materialother->save();

		    return redirect('/materialother/'.$materialother->material)->with('success', 'Material Other has been activated');
        }
    }
}
