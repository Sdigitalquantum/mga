<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Sales;
use App\Http\Models\Account;
use App\Http\Models\City;
use App\Http\Models\Employeeset;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 0;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        $saless = Sales::orderBy('created_at','desc')->get();

        return view('vendor/adminlte/sales.index', compact('saless','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $accounts = Account::where('status', 1)->orderBy('name')->get();
        $citys = City::where('status', 1)->orderBy('name')->get();

        return view('vendor/adminlte/sales.create', compact('accounts','citys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session_id = $request->session()->get('session_login');

        $request->validate([
            'code' => 'unique:sales'
        ]);
    	
        $sales = new Sales([
            'accounting'    => $request->get('account'),
            'city'          => $request->get('city'),
            'code'          => $request->get('code'),
            'name'          => $request->get('name'),
            'address'       => $request->get('address'),
            'mobile'        => $request->get('mobile'),
            'birthday'      => $request->get('birthday'),
            'status_group'  => ($request->get('status_group') == 1) ? 1 : 0,
            'status_record' => ($request->get('status_record') == 1) ? 1 : 0,
            'created_user'  => $session_id,
            'updated_user'  => $session_id
        ]);
	    $sales->save();

	    return redirect('/sales')->with('success', 'Sales has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $disable = 1;

        $employeeroots = Employeeset::select('menuroot')->where('employee', $session_id)->groupBy('menuroot','nomorroot')->orderBy('nomorroot','asc')->get();
        $employeemenus = Employeeset::select('menuroot','menu')->where([['employee', $session_id], ['menu','<>',0]])->groupBy('menuroot','menu','nomormenu')->orderBy('nomormenu','asc')->get();
        $employeesubs = Employeeset::select('menuroot','menu','menusub')->where([['employee', $session_id], ['menusub','<>',0]])->groupBy('menuroot','menu','menusub','nomorsub')->orderBy('nomorsub','asc')->get();
        
        $sales  = Sales::find($id);
        $accounts = Account::where('status', 1)->orderBy('name')->get();
        $citys = City::where('status', 1)->orderBy('name')->get();

        return view('vendor/adminlte/sales.edit', compact('id','sales','accounts','citys','disable','employeeroots','employeemenus','employeesubs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');
        $sales = Sales::find($id);

        if ($sales->code != $request->get('code')) {
            $request->validate([
                'code' => 'unique:sales'
            ]);
        }

        $sales->accounting    = $request->get('account');
        $sales->city          = $request->get('city');
    	$sales->code          = $request->get('code');
        $sales->name          = $request->get('name');
        $sales->address       = $request->get('address');
        $sales->mobile        = $request->get('mobile');
        $sales->birthday      = $request->get('birthday');
        $sales->status_group  = ($request->get('status_group') == 1) ? 1 : 0;
        $sales->status_record = ($request->get('status_record') == 1) ? 1 : 0;
	    $sales->updated_user  = $session_id;
	    $sales->save();

	    return redirect('/sales')->with('success', 'Sales has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $session_id = $request->session()->get('session_login');

        $sales = Sales::find($id);
        $status = $sales->status;

        if ($status == 1) {
        	$sales->status       = 0;
            $sales->updated_user = $session_id;
	        $sales->save();

		    return redirect('/sales')->with('error', 'Sales has been deleted');
        } elseif ($status == 0) {
        	$sales->status       = 1;
            $sales->updated_user = $session_id;
	        $sales->save();

		    return redirect('/sales')->with('success', 'Sales has been activated');
        }
    }
}
