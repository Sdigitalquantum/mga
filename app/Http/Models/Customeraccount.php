<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Customeraccount extends Model
{
    protected $fillable = [
	    'customer',
	    'currency',
	    'bank',
	    'account_no',
	    'account_swift',
	    'account_name',
	    'alias',
	    'created_user',
	    'updated_user'
  	];

  	public function fkCustomer(){
	    return $this->belongsTo('\App\Http\Models\Customer', 'customer', 'id');
	}

	public function fkCurrency(){
	    return $this->belongsTo('\App\Http\Models\Currency', 'currency', 'id');
	}

	public function fkBank(){
	    return $this->belongsTo('\App\Http\Models\Bank', 'bank', 'id');
	}
}
