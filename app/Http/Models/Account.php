<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
	    'currency',
	    'group',
	    'type',
	    'code',
	    'name',
	    'level1',
	    'level2',
	    'level3',
	    'level4',
	    'created_user',
	    'updated_user'
  	];

  	public function fkCurrency(){
	    return $this->belongsTo('\App\Http\Models\Currency', 'currency', 'id');
	}

	public function fkAccountgroup(){
	    return $this->belongsTo('\App\Http\Models\Accountgroup', 'group', 'id');
	}

	public function fkAccounttype(){
	    return $this->belongsTo('\App\Http\Models\Accounttype', 'type', 'id');
	}
}
