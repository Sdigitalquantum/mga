<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Receivable extends Model
{
    protected $fillable = [
	    'no_sp',
	    'code_customer',
	    'name_customer',
	    'code_product',
	    'name_product',
	    'date_sp',
	    'date_sj',
	    'qty',
	    'price',
	    'total',
	    'payment',
	    'adjustment',
	    'created_user',
	    'updated_user'
  	];
}
