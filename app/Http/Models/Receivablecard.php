<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Receivablecard extends Model
{
    protected $fillable = [
	    'customerdetail',
	    'year',
	    'month',
	    'debit',
	    'kredit'
  	];

  	public function fkCustomerdetail(){
	    return $this->belongsTo('\App\Http\Models\Customerdetail', 'customerdetail', 'id');
	}
}
