<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $fillable = [
	    'accounting',
	    'city',
	    'code',
	    'name',
	    'address',
	    'mobile',
	    'birthday',
	    'status_group',
	    'status_record',
	    'created_user',
	    'updated_user'
  	];

  	public function fkAccount(){
	    return $this->belongsTo('\App\Http\Models\Account', 'accounting', 'id');
	}

	public function fkCity(){
	    return $this->belongsTo('\App\Http\Models\City', 'city', 'id');
	}
}
