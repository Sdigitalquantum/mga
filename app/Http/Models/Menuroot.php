<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Menuroot extends Model
{
    protected $fillable = [
	    'name',
	    'url',
	    'icon',
	    'icon_color',
	    'nomor',
	    'created_user',
	    'updated_user'
  	];
}
