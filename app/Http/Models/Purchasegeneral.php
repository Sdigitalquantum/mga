<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Purchasegeneral extends Model
{
    protected $fillable = [
	    'no_inc',
	    'no_prg',
	    'date_order',
	    'notice',
	    'reason',
	    'created_user',
	    'updated_user',
	    'approved_at',
	    'approved_user'
  	];
}
