<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
	    'city',
	    'code',
	    'name',
	    'address',
	    'email',
	    'email',
	    'mobile',
	    'fax',
	    'created_user',
	    'updated_user'
  	];

  	public function fkCity(){
	    return $this->belongsTo('\App\Http\Models\City', 'city', 'id');
	}
}
