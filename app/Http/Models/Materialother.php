<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Materialother extends Model
{
    protected $fillable = [
	    'material',
	    'product',
	    'unit',
	    'qty',
	    'created_user',
	    'updated_user'
  	];

	public function fkMaterial(){
	    return $this->belongsTo('\App\Http\Models\Material', 'material', 'id');
	}

  	public function fkProduct(){
	    return $this->belongsTo('\App\Http\Models\Product', 'product', 'id');
	}

	public function fkUnit(){
	    return $this->belongsTo('\App\Http\Models\Unit', 'unit', 'id');
	}
}
