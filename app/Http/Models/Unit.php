<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = [
	    'name',
	    'created_user',
	    'updated_user'
  	];
}
