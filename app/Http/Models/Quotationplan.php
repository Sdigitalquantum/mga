<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Quotationplan extends Model
{
    protected $fillable = [
	    'quotationsplit',
	    'plan',
	    'created_user',
	    'updated_user'
  	];

  	public function fkQuotationsplit(){
	    return $this->belongsTo('\App\Http\Models\Quotationsplit', 'quotationsplit', 'id');
	}

	public function fkPlan(){
	    return $this->belongsTo('\App\Http\Models\Plan', 'plan', 'id');
	}
}
