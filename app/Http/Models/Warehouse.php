<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $fillable = [
	    'city',
	    'no_inc',
	    'code',
	    'name',
	    'address',
	    'created_user',
	    'updated_user'
  	];

  	public function fkCity(){
	    return $this->belongsTo('\App\Http\Models\City', 'city', 'id');
	}
}
