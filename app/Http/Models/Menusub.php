<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Menusub extends Model
{
    protected $fillable = [
	    'menu',
	    'name',
	    'url',
	    'icon',
	    'icon_color',
	    'nomor',
	    'created_user',
	    'updated_user'
  	];

  	public function fkMenu(){
	    return $this->belongsTo('\App\Http\Models\Menu', 'menu', 'id');
	}
}
