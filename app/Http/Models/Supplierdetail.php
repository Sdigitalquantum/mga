<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Supplierdetail extends Model
{
    protected $fillable = [
	    'supplier',
	    'city',
	    'name',
	    'address',
	    'mobile',
	    'alias_name',
	    'alias_mobile',
	    'created_user',
	    'updated_user'
  	];

  	public function fkSupplier(){
	    return $this->belongsTo('\App\Http\Models\Supplier', 'supplier', 'id');
	}

	public function fkCity(){
	    return $this->belongsTo('\App\Http\Models\City', 'city', 'id');
	}
}
