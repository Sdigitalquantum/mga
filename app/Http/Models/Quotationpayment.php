<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Quotationpayment extends Model
{
    protected $fillable = [
	    'quotation',
	    'payment',
	    'created_user',
	    'updated_user'
  	];

  	public function fkQuotation(){
	    return $this->belongsTo('\App\Http\Models\Quotation', 'quotation', 'id');
	}

	public function fkPayment(){
	    return $this->belongsTo('\App\Http\Models\Payment', 'payment', 'id');
	}
}
