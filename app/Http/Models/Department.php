<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
	    'no_inc',
	    'code',
	    'name',
	    'created_user',
	    'updated_user'
  	];
}
