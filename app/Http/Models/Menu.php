<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = [
	    'menuroot',
	    'name',
	    'url',
	    'icon',
	    'icon_color',
	    'nomor',
	    'created_user',
	    'updated_user'
  	];

  	public function fkMenuroot(){
	    return $this->belongsTo('\App\Http\Models\Menuroot', 'menuroot', 'id');
	}
}
