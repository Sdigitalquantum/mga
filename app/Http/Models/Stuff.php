<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Stuff extends Model
{
    protected $fillable = [
	    'schedulestuff',
	    'quotationsplit',
	    'pol',
	    'pod',
	    'container',
	    'no_inc',
	    'nomor',
	    'etd',
	    'eta',
	    'vehicle',
	    'seal',
	    'qty_fcl',
	    'qty_bag',
	    'qty_pcs',
	    'qty_kg',
	    'nopol',
	    'driver',
	    'mobile',
	    'notice',
	    'container_empty',
	    'container_bruto',
	    'container_netto',
	    'created_user',
	    'updated_user'
  	];

  	public function fkSchedulestuff(){
	    return $this->belongsTo('\App\Http\Models\Schedulestuff', 'schedulestuff', 'id');
	}

	public function fkQuotationsplit(){
	    return $this->belongsTo('\App\Http\Models\Quotationsplit', 'quotationsplit', 'id');
	}

	public function fkPortl(){
	    return $this->belongsTo('\App\Http\Models\Port', 'pol', 'id');
	}
	
	public function fkPortd(){
	    return $this->belongsTo('\App\Http\Models\Port', 'pod', 'id');
	}
	
	public function fkContainer(){
	    return $this->belongsTo('\App\Http\Models\Container', 'container', 'id');
	}
}
