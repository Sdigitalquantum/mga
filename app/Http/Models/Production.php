<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
    protected $fillable = [
	    'material',
	    'no_inc',
	    'nomor',
	    'qty',
	    'created_user',
	    'updated_user'
  	];

	public function fkMaterial(){
	    return $this->belongsTo('\App\Http\Models\Material', 'material', 'id');
	}
}
