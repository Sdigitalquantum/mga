<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Stockin extends Model
{
    protected $fillable = [
	    'transaction',
	    'product',
	    'warehouse',
	    'supplierdetail',
	    'no_inc',
	    'nomor',
	    'date_in',
	    'date_move',
	    'date_out',
	    'date_opname',
	    'noref_in',
	    'noref_out',
	    'price',
	    'qty_bag',
	    'qty_pcs',
	    'qty_kg',
	    'qty_available',
	    'transaction_prod',
	    'status_move',
	    'created_user',
	    'updated_user',
	    'validated_at',
	    'validated_user',
	    'moved_at',
	    'moved_user',
	    'outed_at',
	    'outed_user',
	    'opnamed_at',
	    'opnamed_user'
  	];

  	public function fkTransaction(){
	    return $this->belongsTo('\App\Http\Models\Transaction', 'transaction', 'id');
	}

  	public function fkProduct(){
	    return $this->belongsTo('\App\Http\Models\Product', 'product', 'id');
	}

	public function fkWarehouse(){
	    return $this->belongsTo('\App\Http\Models\Warehouse', 'warehouse', 'id');
	}

	public function fkSupplierdetail(){
	    return $this->belongsTo('\App\Http\Models\Supplierdetail', 'supplierdetail', 'id');
	}

	public function fkTransactionout(){
	    return $this->belongsTo('\App\Http\Models\Transaction', 'transaction_out', 'id');
	}

	public function fkTransactionprod(){
	    return $this->belongsTo('\App\Http\Models\Production', 'transaction_prod', 'id');
	}
}
