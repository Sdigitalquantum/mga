<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = [
	    'no_inc',
	    'code',
	    'name',
	    'export',
	    'local',
	    'internal',
	    'created_user',
	    'updated_user'
  	];
}
