<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    protected $fillable = [
	    'code',
	    'name',
	    'notice',
	    'created_user',
	    'updated_user'
  	];
}
