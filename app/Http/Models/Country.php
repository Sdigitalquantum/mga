<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
	    'no_inc',
	    'code',
	    'name',
	    'mobile',
	    'created_user',
	    'updated_user'
  	];
}
