<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Konversi extends Model
{
    protected $fillable = [
	    'product',
	    'unit_src',
	    'unit_dst',
	    'qty_result',
        'unit',
        'keterangan',
        'keterangan1',
        'keterangan2',
        'qty1',
        'qty2',
	    'general',
	    'status_group',
	    'status_record',
	    'created_user',
	    'updated_user'
  	];

      public function fkProduct(){
	    return $this->belongsTo('\App\Http\Models\product', 'product', 'id');
	}  

	public function fkUnit_src(){
	    return $this->belongsTo('\App\Http\Models\Unit', 'unit_src', 'id');
	}
	public function fkUnit_dst(){
	    return $this->belongsTo('\App\Http\Models\Unit', 'unit_dst', 'id');
	}
}
