<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
	    'department',
	    'accounting',
	    'code',
	    'name',
	    'status_group',
	    'status_record',
	    'created_user',
	    'updated_user'
  	];

  	public function fkDepartment(){
	    return $this->belongsTo('\App\Http\Models\Department', 'department', 'id');
	}

  	public function fkAccount(){
	    return $this->belongsTo('\App\Http\Models\Account', 'accounting', 'id');
	}
}
