<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Quotationdetail extends Model
{
    protected $fillable = [
	    'quotation',
	    'product',
	    'unit',
	    'qty_fcl',
	    'qty_bag',
	    'qty_kg',
	    'qty_pcs',
	    'price',
	    'kurs',
	    'disc_type',
	    'disc_value',
	    'alias_name',
	    'alias_qty',
	    'qty_amount',
	    'created_user',
	    'updated_user'
  	];

  	public function fkQuotation(){
	    return $this->belongsTo('\App\Http\Models\Quotation', 'quotation', 'id');
	}

	public function fkProduct(){
	    return $this->belongsTo('\App\Http\Models\Product', 'product', 'id');
	}

	public function fkUnit(){
	    return $this->belongsTo('\App\Http\Models\Unit', 'unit', 'id');
	}
}
