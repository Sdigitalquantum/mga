<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Stockload extends Model
{
    protected $fillable = [
	    'stuff',
	    'stockin',
	    'no_inc',
	    'nomor',
	    'qty_load',
	    'created_user'
  	];

  	public function fkStuff(){
	    return $this->belongsTo('\App\Http\Models\Stuff', 'stuff', 'id');
	}

	public function fkStockin(){
	    return $this->belongsTo('\App\Http\Models\Stockin', 'stockin', 'id');
	}
}
