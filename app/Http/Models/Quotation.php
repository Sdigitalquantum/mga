<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $fillable = [
	    'customerdetail',
	    'companyaccount',
	    'paymentterm',
	    'container',
	    'pol',
	    'pod',
	    'currency',
	    'sales',
	    'tax',
	    'status_flow',
	    'status_sp',
	    'no_inc',
	    'no_sp',
	    'date_sp',
	    'no_poc',
	    'disc_type',
	    'disc_value',
	    'notice',
	    'etd',
	    'eta',
	    'qty_gross',
	    'qty_cbm',
	    'kurs',
	    'vessel',
	    'created_user',
	    'updated_user'
  	];

  	public function fkCustomerdetail(){
	    return $this->belongsTo('\App\Http\Models\Customerdetail', 'customerdetail', 'id');
	}

	public function fkCompanyaccount(){
	    return $this->belongsTo('\App\Http\Models\Companyaccount', 'companyaccount', 'id');
	}

	public function fkPaymentterm(){
	    return $this->belongsTo('\App\Http\Models\Paymentterm', 'paymentterm', 'id');
	}

	public function fkContainer(){
	    return $this->belongsTo('\App\Http\Models\Container', 'container', 'id');
	}

	public function fkPortl(){
	    return $this->belongsTo('\App\Http\Models\Port', 'pol', 'id');
	}

	public function fkPortd(){
	    return $this->belongsTo('\App\Http\Models\Port', 'pod', 'id');
	}

	public function fkCurrency(){
	    return $this->belongsTo('\App\Http\Models\Currency', 'currency', 'id');
	}

	public function fkSales(){
	    return $this->belongsTo('\App\Http\Models\Sales', 'sales', 'id');
	}
}
