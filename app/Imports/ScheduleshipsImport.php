<?php

namespace App\Imports;

use App\Http\Models\Scheduleship;
use Maatwebsite\Excel\Concerns\ToModel;

class ScheduleshipsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Scheduleship([
            'pol'           => $row[0],
            'pod'           => $row[1],
            'lines'         => $row[2],
            'etd'           => date("Y-m-d", (($row[3] - 25569) * 86400)),
            'eta'           => date("Y-m-d", (($row[4] - 25569) * 86400)),
            'created_user'  => 1
        ]);
    }
}
