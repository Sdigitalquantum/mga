@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Assembly / Production &nbsp;<a href="{{ route('productiondetail.show', $id) }}" class="btn btn-xs btn-primary" style="text-decoration: none;">Detail</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('production.update', $production->id) }}">
				      		@method('PATCH')
				      		@csrf
				      		<div class="row">
				          		<div class="col-md-12">
						      		<div class="form-group">
						              	<label for="material">Material</label>
						              	<select class="form-control select2" name="material" required>
						              		@foreach($materials as $material)
											    @if ($production->material == $material->id)
												    <option value="{{ $material->id }}" selected>{{ $material->fkProduct->code }} - {{ $material->fkProduct->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						      		<div class="form-group">
						              	<label for="qty">Qty (Kg)</label>
						              	<input type="number" class="form-control" name="qty" value="{{ $production->qty }}" required>
						          	</div>
						          	<div class="form-group">
										<button type="submit" class="btn btn-sm btn-success">Update</button>
						          		<a href="{{ url('/production') }}" class="btn btn-sm btn-danger">Cancel</a>
					          		</div>
				          		</div>
			          		</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop