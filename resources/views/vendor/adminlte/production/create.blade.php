@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">New Assembly / Production</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('production.store') }}">
				          	@csrf
				          	<div class="row">
				          		<div class="col-md-12">
				          			<div class="form-group">
						              	<label for="material">Material</label>
						              	<select class="form-control select2" name="material" required>
						              		<option value="{{ old('material') }}">Choose One</option>
					                    	@foreach($materials as $material)
											    <option value="{{ $material->id }}">{{ $material->fkProduct->code }} - {{ $material->fkProduct->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="qty">Qty (Kg)</label>
						              	<input type="number" class="form-control" name="qty" value="{{ old('qty') }}" required>
						          	</div>
						          	<div class="form-group">
						          		<button type="submit" class="btn btn-sm btn-success">Save</button>
						          		<a href="{{ url('/production') }}" class="btn btn-sm btn-danger">Cancel</a>
					          		</div>
			          			</div>
		          			</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop