@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Letter</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('letter.update', $letter->id) }}">
				      		@method('PATCH')
				      		@csrf
				          	<div class="form-group">
				              	<label for="menu">Menu</label>
				              	<input type="hidden" name="menu" value="{{ $letter->menu }}">
				              	<input type="text" class="form-control" value="{{ $letter->menu }}" disabled>
				          	</div>
				          	<div class="form-group">
				              	<label for="company">Company</label>
				              	<input type="text" class="form-control" name="company" value="{{ $letter->company }}" required>
				          	</div>
				          	<div class="form-group">
				              	<label for="code">Code</label>
				              	<input type="text" class="form-control" name="code" value="{{ $letter->code }}" required>
				          	</div>
				          	<div class="form-group">
				          		@if($letter->romawi == 1)
				          			<input type="checkbox" name="romawi" value="1" checked> Romawi &nbsp;&nbsp;&nbsp;
				          		@else
				          			<input type="checkbox" name="romawi" value="1"> Romawi &nbsp;&nbsp;&nbsp;
				          		@endif

				          		@if($letter->year == 1)
				          			<input type="checkbox" name="year" value="1" checked> Year &nbsp;&nbsp;&nbsp;
				          		@else
				          			<input type="checkbox" name="year" value="1"> Year &nbsp;&nbsp;&nbsp;
				          		@endif
				          	</div>
				          	<button type="submit" class="btn btn-sm btn-success">Update</button>
				          	<a href="{{ url('/letter') }}" class="btn btn-sm btn-danger">Cancel</a>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop