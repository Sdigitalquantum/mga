@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Raw Material</a></li>
            <li class="disabled"><a>Excess</a></li>
            <li class="active"><a href="#tab_show" data-toggle="tab">Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_show">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Show Bill of Material 
			    			&nbsp;<a href="javascript:window.print()" class="btn btn-xs btn-default" style="text-decoration: none;"><i class="glyphicon glyphicon-print"></i></a>
			    			&nbsp;<a href="{{ url('/material') }}" class="btn btn-xs btn-danger">Back</a>
			    		</h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div id="printable">
				  			<div class="row">
				  				<center>
				  					<b>{{$company->name}}</b> <br>
					  				BILL OF MATERIAL <br>
					  				DOCUMENT INTERNAL <br>
					  				DATE : {{ date('d F Y', strtotime("now")) }} <br>
					  			</center><br>

				          		<div class="col-md-12">
						  			<div class="form-group">
						              	<label for="product">Product</label>
						              	@foreach($products as $product)
										    @if ($material->product == $product->id)
											    <input type="text" class="form-control" name="product" value="{{ $product->code }} - {{ $product->name }}" disabled>
											@endif
										@endforeach
						          	</div>
							      	<div class="form-group">
						          		<label for="check">&nbsp;</label>
						          		@if($material->status_group == 1)
						          			<input type="checkbox" name="status_group" value="1" checked disabled> Status Group &nbsp;&nbsp;&nbsp;
						          		@else
						          			<input type="checkbox" name="status_group" value="1" disabled> Status Group &nbsp;&nbsp;&nbsp;
						          		@endif
						          		
						          		<br><label for="check">&nbsp;</label>
						          		@if($material->status_record == 1)
						          			<input type="checkbox" name="status_record" value="1" checked disabled> Status Record
						          		@else
						          			<input type="checkbox" name="status_record" value="1" disabled> Status Record
						          		@endif
						          	</div>
					          	</div>
				          	</div>

							<table class="table table-bordered table-hover nowrap" width="100%">
							    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
							        <tr>
							          	<th width="30%">Product</th>
							          	<th width="20%">Unit</th>
							          	<th width="15%">Qty</th>
							          	<th width="20%">Type</th>
							          	<th width="15%">Status</th>
							        </tr>
							    </thead>

							    <tbody>
							    	@foreach($materialdetails as $materialdetail)
							    		<tr>
								            <td>{{$materialdetail->fkProduct->name}}</td>
								            <td>{{$materialdetail->fkUnit->name}}</td>
								            <td style="text-align: right;">{{$materialdetail->qty}} &nbsp;</td>
								            <td>Raw Material</td>
								            <td>@if($materialdetail->status == 1) <font color="blue">Active</font> @else <font color="red">Not Active</font> @endif</td>
								        </tr>
							        @endforeach

							        @foreach($materialothers as $materialother)
							    		<tr>
								            <td>{{$materialother->fkProduct->name}}</td>
								            <td>{{$materialother->fkUnit->name}}</td>
								            <td style="text-align: right;">{{$materialother->qty}} &nbsp;</td>
								            <td>Excess</td>
								            <td>@if($materialother->status == 1) <font color="blue">Active</font> @else <font color="red">Not Active</font> @endif</td>
								        </tr>
							        @endforeach
							    </tbody>
						  	</table>
					  	</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop