@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Approval Stuffing Schedule</h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="15%">Split Proforma</th>
			          	<th width="15%">Schedule</th>
			          	<th width="30%">Stuffing</th>
			          	<th width="30%">EMKL</th>
			          	<th width="5%">Action</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($no = 0)
			    	@foreach($schedulestuffs as $schedulestuff)
			    		@php ($no++)
			        	<tr>
				            <td>{{$no}}</td>
				            <td>{{$schedulestuff->fkSchedulebook->fkQuotationsplit->no_split}}</td>
				            <td>
				            	POL : {{$schedulestuff->fkSchedulebook->pol}} <br>
				            	POD : {{$schedulestuff->fkSchedulebook->pod}} <br>
				            	ETD : {{$schedulestuff->fkSchedulebook->etd}} <br>
				            	ETA : {{$schedulestuff->fkSchedulebook->eta}}
				            </td>
				            <td>
				            	Date : {{$schedulestuff->date}} <br>
				            	PIC &nbsp;: {{$schedulestuff->person}} <br>
				            	Qty Fcl : {{$schedulestuff->qty_approve}} <br>
				            	Note : <pre>{{$schedulestuff->notice}}</pre>
				            </td>
				            <td>
				            	@if($schedulestuff->status_approve == 1 || $schedulestuff->status_reschedule == 1)
				            		Qty Fcl : {{$schedulestuff->qty_approve}}
				            		<br>Freight : {{$schedulestuff->freight}}
				            		<br>Vessel : {{$schedulestuff->vessel}} <br>
			            		@endif
			            		Note : <pre>{{$schedulestuff->reason}}</pre>
				            </td>
							
							@if($schedulestuff->status_approve == 0)
								@if($schedulestuff->status_reschedule == 1)
									<td>
						            	<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal-approve{{$schedulestuff->id}}">Approve</button>

						            	<form action="{{ url('schedulestuff/approve', $schedulestuff->id)}}" method="post">
						                  	@csrf
						                  	@method('DELETE')
						                  	<div class="modal fade bs-example-modal-approve{{$schedulestuff->id}}" tabindex="-1" role="dialog" aria-hidden="true">
			                                    <div class="modal-dialog modal-sm">
			                                        <div class="modal-content">
			                                            <div class="modal-header btn-success">
			                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
			                                                </button>
			                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Approve</h4>
			                                            </div>
			                                            <div class="modal-body">
			                                            	Are you sure to approve this data?
			                                            	<input type="hidden" name="status_reschedule" value="1">
			                                            	<br><textarea class="form-control" name="reason" cols="30" rows="3" maxlength="150" required></textarea>
			                                            </div>
			                                            <div class="modal-footer">
			                                                <button type="submit" class="btn btn-success">Yes</button>
			                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
						                </form>
					            	</td>
				            	@else
				            		<td>
						            	<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal-approve{{$schedulestuff->id}}">Approve</button> &nbsp;
						                <button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal-reject{{$schedulestuff->id}}">Reject</button>

						            	<form action="{{ url('schedulestuff/approve', $schedulestuff->id)}}" method="post">
						                  	@csrf
						                  	@method('DELETE')
						                  	<div class="modal fade bs-example-modal-approve{{$schedulestuff->id}}" tabindex="-1" role="dialog" aria-hidden="true">
			                                    <div class="modal-dialog modal-sm">
			                                        <div class="modal-content">
			                                            <div class="modal-header btn-success">
			                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
			                                                </button>
			                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Approve</h4>
			                                            </div>
			                                            <div class="modal-body">
			                                            	Are you sure to approve this data?
			                                            	<br>Qty Fcl : <input type="number" class="form-control" name="qty_approve" value="{{ old('qty_approve') }}">
			                                            	<br><br>Freight : <input type="number" class="form-control" name="freight" value="{{ old('freight') }}">
			                                            	<br><br>Vessel : <input type="text" class="form-control" name="vessel" value="{{ old('vessel') }}" required>
			                                            	<input type="hidden" name="qty_fcl" value="{{ $schedulestuff->qty_fcl }}">
			                                            	<br><br><textarea class="form-control" name="reason" cols="30" rows="3" maxlength="150" required>{{ old('reason') }}</textarea>
			                                            </div>
			                                            <div class="modal-footer">
			                                                <button type="submit" class="btn btn-success">Yes</button>
			                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
						                </form>

						            	<form action="{{ url('schedulestuff/reject', $schedulestuff->id)}}" method="post">
						                  	@csrf
						                  	@method('DELETE')
						                  	<div class="modal fade bs-example-modal-reject{{$schedulestuff->id}}" tabindex="-1" role="dialog" aria-hidden="true">
			                                    <div class="modal-dialog modal-sm">
			                                        <div class="modal-content">
			                                            <div class="modal-header btn-danger">
			                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
			                                                </button>
			                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Reject</h4>
			                                            </div>
			                                            <div class="modal-body">
			                                            	Are you sure to reject this data?
			                                            	<br><textarea class="form-control" name="reason" cols="30" rows="3" maxlength="150" required></textarea>
			                                            </div>
			                                            <div class="modal-footer">
			                                                <button type="submit" class="btn btn-success">Yes</button>
			                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
						                </form>
					            	</td>
				            	@endif
			            	@elseif($schedulestuff->status_approve == 1)
			            		<td><font color="green">Approved <i class="glyphicon glyphicon-ok"></i></font></td>
		            		@elseif($schedulestuff->status_approve == 2)
			            		<td><font color="red">Rejected <i class="glyphicon glyphicon-remove"></i></font></td>
							@endif
				        </tr>
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>Split Proforma</td>
			          	<td>Schedule</td>
			          	<td>Stuffing</td>
			          	<td>EMKL</td>
			          	<td>Action</td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	</div>
@stop