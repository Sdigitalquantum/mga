@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_list" data-toggle="tab">Stuffing</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Stuffing Schedule &nbsp;<a href="{{ url('schedulestuff') }}" class="btn btn-xs btn-primary" style="text-decoration: none;">Booking Schedule</a></h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="15%">Split Proforma</th>
						          	<th width="15%">Schedule</th>
						          	<th width="30%">Stuffing</th>
						          	<th width="30%">EMKL</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($schedulestuffs as $schedulestuff)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$schedulestuff->fkSchedulebook->fkQuotationsplit->no_split}}</td>
							            <td>
							            	POL : {{$schedulestuff->fkSchedulebook->pol}} <br>
							            	POD : {{$schedulestuff->fkSchedulebook->pod}} <br>
							            	ETD : {{$schedulestuff->fkSchedulebook->etd}} <br>
							            	ETA : {{$schedulestuff->fkSchedulebook->eta}}
							            </td>
							            <td>
							            	Date : {{$schedulestuff->date}} <br>
							            	PIC &nbsp;: {{$schedulestuff->person}} <br>
							            	Qty Fcl : {{$schedulestuff->qty_fcl}} <br>
							            	Note : <pre>{{$schedulestuff->notice}}</pre>
							            </td>
							            <td>
							            	@if($schedulestuff->status_approve == 1 || $schedulestuff->status_reschedule == 1)
							            		Qty Fcl : {{$schedulestuff->qty_approve}}
							            		<br>Freight : {{$schedulestuff->freight}}
							            		<br>Vessel : {{$schedulestuff->vessel}} <br>
						            		@endif
						            		Note : <pre>{{$schedulestuff->reason}}</pre>
							            </td>
										
										@if($schedulestuff->status_approve == 0)
											@if($schedulestuff->status_reschedule == 0)
												<td><font color="blue">Waiting Approval</font></td>
											@else
												<td><font color="red">Canceled <i class="glyphicon glyphicon-remove"></i></font></td>
											@endif
						            	@elseif($schedulestuff->status_approve == 1)
						            		<td><font color="green">Approved <i class="glyphicon glyphicon-ok"></i></font></td>
					            		@elseif($schedulestuff->status_approve == 2)
						            		<td><font color="red">Rejected <i class="glyphicon glyphicon-remove"></i></font></td>
										@endif
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Split Proforma</td>
						          	<td>Schedule</td>
						          	<td>Stuffing</td>
						          	<td>EMKL</td>
						          	<td>Action</td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop