@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">New Port</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('port.store') }}">
				          	@csrf
				          	<div class="form-group">
				              	<label for="code">Code</label>
				              	<input type="text" class="form-control" name="code" disabled>
				          	</div>
				          	<div class="form-group">
				              	<label for="name">Name</label>
				              	<input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
				          	</div>
				          	<div class="form-group">
				          		<label for="country">Country</label>
				              	<select class="form-control select2" name="country" required>
				              		<option value="{{ old('country') }}">Choose One</option>
			                    	@foreach($countrys as $country)
									    <option value="{{ $country->id }}">{{ $country->name }}</option>
									@endforeach
			                	</select>
				          	</div>
				          	<button type="submit" class="btn btn-sm btn-success">Save</button>
				          	<a href="{{ url('/port') }}" class="btn btn-sm btn-danger">Cancel</a>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop