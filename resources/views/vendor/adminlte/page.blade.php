@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}} ">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/pnotify.custom.min.css') }}">
    <style type="text/css">
        #printable { visibility: visible; }

        @media print
        {
            body { visibility: hidden; }
            .noprint { visibility: hidden; }
            #printable { visibility: visible; margin-top: -19%; size: 21cm 29.7cm; }
        }

        hr { height: 5px; border: 1; color: #333; background-color: #333; }
        pre { border: 0; background-color: transparent; margin-left: -10px; margin-top: -10px; }
        .border { border: solid 1px #000; }
    </style>

    @stack('css')
    @yield('css')
@stop

@section('body_class', 'skin-' . config('adminlte.skin', 'blue') . ' sidebar-mini ' . (config('adminlte.layout') ? [
    'boxed' => 'layout-boxed',
    'fixed' => 'fixed',
    'top-nav' => 'layout-top-nav'
][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
    <div class="wrapper">
        <header class="main-header">
            @if(config('adminlte.layout') == 'top-nav')
            <nav class="navbar navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="navbar-brand">
                            {!! config('adminlte.logo', '<b>PR. MGA</b>') !!}
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <!-- @each('adminlte::partials.menu-item-top-nav', $adminlte->menu(), 'item') -->
                        </ul>
                    </div>
            @else
            
            @if($disable == 0)
                <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="logo">
                    <span class="logo-mini">{!! config('adminlte.logo_mini', '<b>MGA</b>') !!}</span>
                    <span class="logo-lg" style="font-size:9pt;">{!! config('adminlte.logo', '<b>PR. MGA</b>') !!}</span>
                </a>
            @elseif($disable == 1)
                <a class="logo">
                    <span class="logo-mini">{!! config('adminlte.logo_mini', '<b>MGA</b>') !!}</span>
                    <span class="logo-lg" style="font-size:9pt;">{!! config('adminlte.logo', '<b>PR. MGA</b>') !!}</span>
                </a>
            @endif

            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
                </a>
            @endif
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li>
                            @if($disable == 0)
                                <a href="{{ url('login') }}">
                                    <i class="fa fa-fw fa-power-off"></i> Logout
                                </a>
                            @elseif($disable == 1)
                                <a>
                                    <i class="fa fa-fw fa-power-off"></i> Logout
                                </a>
                            @endif
                        </li>
                    </ul>
                </div>
                @if(config('adminlte.layout') == 'top-nav')
                </div>
                @endif
            </nav>
        </header>

        @if(config('adminlte.layout') != 'top-nav')
        <aside class="main-sidebar">
            <section class="sidebar">
                <ul class="sidebar-menu" data-widget="tree">
                    <!-- @each('adminlte::partials.menu-item', $adminlte->menu(), 'item') -->

                    @if($disable == 0)
                        @foreach($employeeroots as $employeeroot)
                            @if($employeeroot->fkMenuroot->url == '#')
                                <li class="treeview">
                            @else
                                <li class="">
                            @endif
                                <a href="{{ $employeeroot->fkMenuroot->url }}">
                                    <i class="fa fa-fw fa-{{$employeeroot->fkMenuroot->icon}} text-{{$employeeroot->fkMenuroot->icon_color}}"></i>
                                    <span>{{$employeeroot->fkMenuroot->name}}</span>

                                    @if($employeeroot->fkMenuroot->url == '#')
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    @endif
                                </a>
                                
                                <ul class="treeview-menu">
                                    @foreach($employeemenus as $employeemenu)
                                        @if($employeeroot->menuroot == $employeemenu->menuroot)
                                            @if($employeemenu->fkMenu->url == '#')
                                                <li class="treeview">
                                            @else
                                                <li class="">
                                            @endif
                                                <a href="{{ $employeemenu->fkMenu->url }}">
                                                    <i class="fa fa-fw fa-chevron-right text-{{$employeemenu->fkMenu->icon_color}}"></i>
                                                    <span>{{$employeemenu->fkMenu->name}}</span>

                                                    @if($employeemenu->fkMenu->url == '#')
                                                        <span class="pull-right-container">
                                                            <i class="fa fa-angle-left pull-right"></i>
                                                        </span>
                                                    @endif
                                                </a>
                                                
                                                <ul class="treeview-menu">
                                                    @foreach($employeesubs as $employeesub)
                                                        @if($employeemenu->menu == $employeesub->menu)
                                                            <li class="">
                                                                <a href="{{$employeesub->fkMenusub->url}}">
                                                                    <i class="fa fa-fw fa-circle-o text-{{$employeesub->fkMenusub->icon_color}}"></i>
                                                                    <span>{{$employeesub->fkMenusub->name}}</span>
                                                                </a>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                    @elseif($disable == 1)
                        @foreach($employeeroots as $employeeroot)
                            <li>
                                <a>
                                    <i class="fa fa-fw fa-{{$employeeroot->fkMenuroot->icon}} text-{{$employeeroot->fkMenuroot->icon_color}}"></i>
                                    <span>{{$employeeroot->fkMenuroot->name}}</span>

                                    @if($employeeroot->fkMenuroot->url == '#')
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    @endif
                                </a>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </section>
        </aside>
        @endif

        <div class="content-wrapper">
            @if(config('adminlte.layout') == 'top-nav')
            <div class="container">
            @endif

            <section class="content">
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}  
                    </div>
                @elseif(session()->get('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}  
                    </div>
                @elseif ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @yield('content')
            </section>

            @if(config('adminlte.layout') == 'top-nav')
            </div>
            @endif
        </div>
    </div>
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/pnotify.custom.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/jquery.slimscroll.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            var url = window.location;

            $('ul.sidebar-menu a').filter(function() {
                return this.href == url;
            }).parent().addClass('active');

            $('ul.treeview-menu a').filter(function() {
                return this.href == url;
            }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');

            $('#example tfoot td:not(.noShow)').each( function (){
                $(this).html( '<input type="text" placeholder="Search" class="form-control" style="background: #605ca8 !important; color: white; width: 100%;">' );
            });

            var table = $('#example').DataTable({
                responsive: true,
                dom: "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-2'l><'col-sm-3'i><'col-sm-7'p>>"
            });

            table.columns().every( function ( colIdx ){
                var that = this;
         
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                });
            });

            $(".alert").alert();
            window.setTimeout(function () {
                $(".alert").alert('close');
            }, 10000);

            $('.select2').select2();

            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd'
            });

            $('#ajaxcustomerdetail').on('change', function() {
                var url;
                var id_url = $('#id_url').val();
                var id = $(this).val();

                if (id_url === undefined) {
                    url = "0/ajax/"+id;
                } else {
                    url = "ajax/"+id;
                }

                if (id) {
                    $.ajax({
                        type: "get",
                        url: url,
                        dataType: "json",
                        success:function(data) {
                            $('#ajaxnotice').html(data.notice);
                        }
                    });
                } else {
                    $('#ajaxnotice').html('');
                }
            });

            $('#ajaxproduct').on('change', function() {
                var url;
                var id_url = $('#id_url').val();
                var id = $(this).val();

                if (id_url === undefined) {
                    url = "0/ajax/"+id;
                } else {
                    url = "ajax/"+id;
                }

                if (id) {
                    $.ajax({
                        type: "get",
                        url: url,
                        dataType: "json",
                        success:function(data) {
                            if (data) {
                                $('#ajaxprice').html(data.price);
                            } else {
                                $('#ajaxprice').html('0');
                            }
                        }
                    });
                } else {
                    $('#ajaxprice').html('0');
                }
            });

            var notice = $('#notice').val();
            if (notice) {
                new PNotify({
                    title: 'Note',
                    text: notice,
                    styling: 'bootstrap3',
                    icon: 'glyphicon glyphicon-comment',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
            }
        });
    </script>
    
    @stack('js')
    @yield('js')
@stop
