@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
            <li class="disabled"><a>Request</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Purchase Order &nbsp;<a href="{{ url('/purchasedetail/create', $purchaseorder->id) }}" class="btn btn-xs btn-primary" style="text-decoration: none;">Detail</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('purchaseorder.update', $purchaseorder->id) }}">
				      		@method('PATCH')
				      		@csrf
				      		<div class="form-group">
				              	@if($purchaseorder->tax == 1)
				          			<input type="checkbox" name="tax" value="1" checked> Tax
				          		@else
				          			<input type="checkbox" name="tax" value="1"> Tax
				          		@endif
				          	</div>
				      		<div class="form-group">
				          		<label for="date_order">Date</label>
				              	<input type="text" class="form-control datepicker" name="date_order" value="{{ $purchaseorder->date_order }}" required>
				          	</div>
							<div class="form-group">
				          		<label for="notice">Note</label>
				          		<textarea class="form-control" name="notice" maxlength="150" rows="3">{{ $purchaseorder->notice }}</textarea>
				          	</div>
				          	<div class="form-group">
				          		<button type="submit" class="btn btn-sm btn-success">Update</button>
				          		<a href="{{ url('/purchaseorder') }}" class="btn btn-sm btn-danger">Cancel</a>
				          	</div>
						</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop