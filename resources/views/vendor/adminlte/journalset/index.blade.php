@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Journal Process &nbsp;<a href="{{ url('exportjournal')}}" class="btn btn-xs btn-default" style="text-decoration: none;"><i class="fa fa-file-excel-o"></i></a></h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%">No</th>
			          	<th width="15%">Transaction</th>
			          	<th width="10%">Journal</th>
			          	<th width="10%">Accounting</th>
			          	<th width="20%">Name</th>
			          	<th width="5%">Status</th>
			          	<th width="10%">Total</th>
			          	<th width="10%">Information</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($no = 0)
			    	@foreach($journalsets as $journalset)
			    		@php ($no++)
			        	<tr>
				            <td>{{$no}}</td>
				            <td>
				            	No. Inc. : {{$journalset->no_is}} <br>
				            	No. QC : {{$journalset->no_qc}} <br>
				            	No. Inv. : {{$journalset->no_inv}}
				            </td>
				            <td>{{$journalset->code_journal}}</td>
				            <td style="text-align: right;">{{$journalset->code_accounting}} &nbsp;</td>
				            <td>{{$journalset->name_accounting}}</td>
				            <td style="text-align: center;">{{$journalset->status}}</td>
				            <td style="text-align: right;">{{number_format($journalset->price, 0, ',' , '.')}} &nbsp;</td>
				            <td>
				            	{{$journalset->created_at}} <br>
				            	User : {{$journalset->fkEmployee->name}}
				            </td>
				        </tr>
			        @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>Transaction</td>
			          	<td>Journal</td>
			          	<td>Accounting</td>
			          	<td>Name</td>
			          	<td>Status</td>
			          	<td>Total</td>
			          	<td class="noShow"></td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	<div>
@stop