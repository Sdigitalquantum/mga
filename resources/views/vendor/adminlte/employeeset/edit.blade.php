@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Menu</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Menu &nbsp;<a href="{{ url('employeeset')}}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<div class="row">
							<div class="col-md-6">
								<table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="60%">Detail Menu</th>
								          	<th width="40%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($menuroots as $menuroot)
						    				<tr>
						    					<td>
						    						<i class="fa fa-fw fa-{{$menuroot->icon}} text-{{$menuroot->icon_color}}"></i> {{$menuroot->name}}
						    					</td>

						    					<td>
									            	<form method="post" action="{{ route('employeeset.update', $employee->id) }}">
											      		@method('PATCH')
											      		@csrf
											      		<input type="hidden" name="employee" value="{{$employee->id}}">
											      		<input type="hidden" name="menuroot" value="{{$menuroot->id}}">
											      		<input type="hidden" name="nomorroot" value="{{$menuroot->nomor}}">
											      		<input type="hidden" name="menu" value="0">
											      		<input type="hidden" name="menusub" value="0">
											          	<button type="submit" class="btn btn-xs btn-success">Choose</button>
											      	</form>
									            </td>
					    					</tr>

					    					@foreach($menus as $menu)
								    			@if($menuroot->id == $menu->menuroot)
										        	<tr>
											            <td>
											            	<i class="fa fa-fw fa-chevron-right text-{{$menu->icon_color}}" style="margin-left: 10%;"></i> {{$menu->name}}
											            </td>

											            <td>
											            	<form method="post" action="{{ route('employeeset.update', $employee->id) }}">
													      		@method('PATCH')
													      		@csrf
													      		<input type="hidden" name="employee" value="{{$employee->id}}">
													      		<input type="hidden" name="menuroot" value="{{$menu->fkMenuroot->id}}">
													      		<input type="hidden" name="nomorroot" value="{{$menu->fkMenuroot->nomor}}">
													      		<input type="hidden" name="menu" value="{{$menu->id}}">
													      		<input type="hidden" name="nomormenu" value="{{$menu->nomor}}">
													      		<input type="hidden" name="menusub" value="0">
													          	<button type="submit" class="btn btn-xs btn-success">Choose</button>
													      	</form>
									            		</td>
											        </tr>

											        @foreach($menusubs as $menusub)
										    			@if($menu->id == $menusub->menu)
												        	<tr>
													            <td>
													            	<i class="fa fa-fw fa-circle-o text-{{$menusub->icon_color}}" style="margin-left: 20%;"></i> {{$menusub->name}}
													            </td>

													            <td>
													            	<form method="post" action="{{ route('employeeset.update', $employee->id) }}">
															      		@method('PATCH')
															      		@csrf
															      		<input type="hidden" name="employee" value="{{$employee->id}}">
															      		<input type="hidden" name="menuroot" value="{{$menusub->fkMenu->fkMenuroot->id}}">
															      		<input type="hidden" name="nomorroot" value="{{$menusub->fkMenu->fkMenuroot->nomor}}">
															      		<input type="hidden" name="menu" value="{{$menusub->fkMenu->id}}">
															      		<input type="hidden" name="nomormenu" value="{{$menusub->fkMenu->nomor}}">
															      		<input type="hidden" name="menusub" value="{{$menusub->id}}">
															      		<input type="hidden" name="nomorsub" value="{{$menusub->nomor}}">
															          	<button type="submit" class="btn btn-xs btn-success">Choose</button>
															      	</form>
											            		</td>
													        </tr>
												        @endif
										        	@endforeach
										        @endif
								        	@endforeach
								        @endforeach
								    </tbody>
							  	</table>
							</div>

							<div class="col-md-6">
								<table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #00a65a !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="80%">Access Menu</th>
								          	<th width="20%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($employeerootaccs as $employeerootacc)
						    				<tr>
						    					<td>
						    						<i class="fa fa-fw fa-{{$employeerootacc->fkMenuroot->icon}} text-{{$employeerootacc->fkMenuroot->icon_color}}"></i> {{$employeerootacc->fkMenuroot->name}}
						    					</td>

						    					<td>
									            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$employeerootacc->menuroot}}">Delete</button>

									            	<form action="{{ route('employeeset.destroy', $employeerootacc->menuroot)}}" method="post">
									                  	@csrf
									                  	@method('DELETE')
									                  	<div class="modal fade bs-example-modal{{$employeerootacc->menuroot}}" tabindex="-1" role="dialog" aria-hidden="true">
						                                    <div class="modal-dialog modal-sm">
						                                        <div class="modal-content">
						                                            <div class="modal-header btn-danger">
						                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
						                                                </button>
						                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
						                                            </div>
						                                            <div class="modal-body">
						                                                Are you sure to delete this data?
						                                                <input type="hidden" name="employee" value="{{$employee->id}}">
															      		<input type="hidden" name="menuroot" value="{{$employeerootacc->menuroot}}">
															      		<input type="hidden" name="menu" value="0">
															      		<input type="hidden" name="menusub" value="0">
						                                            </div>
						                                            <div class="modal-footer">
						                                                <button type="submit" class="btn btn-success">Yes</button>
						                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
						                                            </div>
						                                        </div>
						                                    </div>
						                                </div>
									                </form>
									            </td>
					    					</tr>

					    					@foreach($employeemenuaccs as $employeemenuacc)
								    			@if($employeerootacc->menuroot == $employeemenuacc->menuroot)
										        	<tr>
											            <td>
											            	<i class="fa fa-fw fa-chevron-right text-{{$employeemenuacc->fkMenu->icon_color}}" style="margin-left: 10%;"></i> {{$employeemenuacc->fkMenu->name}}
											            </td>

											            <td>
											            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$employeemenuacc->menu}}">Delete</button>

											            	<form action="{{ route('employeeset.destroy', $employeemenuacc->menu)}}" method="post">
											                  	@csrf
											                  	@method('DELETE')
											                  	<div class="modal fade bs-example-modal{{$employeemenuacc->menu}}" tabindex="-1" role="dialog" aria-hidden="true">
								                                    <div class="modal-dialog modal-sm">
								                                        <div class="modal-content">
								                                            <div class="modal-header btn-danger">
								                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
								                                                </button>
								                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
								                                            </div>
								                                            <div class="modal-body">
								                                                Are you sure to delete this data?								                                                
																	      		<input type="hidden" name="employee" value="{{$employee->id}}">
																	      		<input type="hidden" name="menuroot" value="{{$employeemenuacc->fkMenuroot->id}}">
																	      		<input type="hidden" name="menu" value="{{$employeemenuacc->menu}}">
																	      		<input type="hidden" name="menusub" value="0">
								                                            </div>
								                                            <div class="modal-footer">
								                                                <button type="submit" class="btn btn-success">Yes</button>
								                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
								                                            </div>
								                                        </div>
								                                    </div>
								                                </div>
											                </form>
											            </td>
											        </tr>

											        @foreach($employeesubaccs as $employeesubacc)
										    			@if($employeemenuacc->menu == $employeesubacc->menu)
												        	<tr>
													            <td>
													            	<i class="fa fa-fw fa-circle-o text-{{$employeesubacc->fkMenusub->icon_color}}" style="margin-left: 20%;"></i> {{$employeesubacc->fkMenusub->name}}
													            </td>

													            <td>
													            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$employeesubacc->menusub}}">Delete</button>

													            	<form action="{{ route('employeeset.destroy', $employeesubacc->menusub)}}" method="post">
													                  	@csrf
													                  	@method('DELETE')
													                  	<div class="modal fade bs-example-modal{{$employeesubacc->menusub}}" tabindex="-1" role="dialog" aria-hidden="true">
										                                    <div class="modal-dialog modal-sm">
										                                        <div class="modal-content">
										                                            <div class="modal-header btn-danger">
										                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
										                                                </button>
										                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
										                                            </div>
										                                            <div class="modal-body">
										                                                Are you sure to delete this data?
										                                                <input type="hidden" name="employee" value="{{$employee->id}}">
																			      		<input type="hidden" name="menuroot" value="{{$employeesubacc->fkMenu->fkMenuroot->id}}">
																			      		<input type="hidden" name="menu" value="{{$employeesubacc->fkMenu->id}}">
																			      		<input type="hidden" name="menusub" value="{{$employeesubacc->menusub}}">
										                                            </div>
										                                            <div class="modal-footer">
										                                                <button type="submit" class="btn btn-success">Yes</button>
										                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
										                                            </div>
										                                        </div>
										                                    </div>
										                                </div>
													                </form>
													            </td>
													        </tr>
												        @endif
										        	@endforeach
										        @endif
								        	@endforeach
								        @endforeach
								    </tbody>
							  	</table>
							</div>
						</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop