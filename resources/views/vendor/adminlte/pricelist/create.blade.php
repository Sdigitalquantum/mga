@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">New Price List</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('pricelist.store') }}">
				          	@csrf
				          	<div class="form-group">
				          		<label for="product">Product</label>
				              	<select class="form-control select2" name="product" required>
				              		<option value="{{ old('product') }}">Choose One</option>
			                    	@foreach($products as $product)
									    <option value="{{ $product->id }}">{{ $product->name }}</option>
									@endforeach
			                	</select>
				          	</div>
				          	<div class="form-group">
				          		<label for="currency">Currency</label>
				              	<select class="form-control select2" name="currency" required>
				              		<option value="{{ old('currency') }}">Choose One</option>
			                    	@foreach($currencys as $currency)
									    <option value="{{ $currency->id }}">{{ $currency->code }}</option>
									@endforeach
			                	</select>
				          	</div>
				          	<div class="form-group">
				              	<label for="price">Price @Kg</label>
				              	<input type="number" class="form-control" name="price" value="{{ old('price') }}" step=".001" required>
				          	</div>
				          	<div class="form-group">
				              	<label for="date">Date</label>
				              	<input type="text" class="form-control datepicker" name="date" value="{{ old('date') }}" required>
				          	</div>
				          	<button type="submit" class="btn btn-sm btn-success">Save</button>
				          	<a href="{{ url('/pricelist') }}" class="btn btn-sm btn-danger">Cancel</a>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop