@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
            <li class="disabled"><a>Loading</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Request Stock</h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="15%">Split Proforma</th>
						          	<th width="20%">Port</th>
						          	<th width="15%">Date</th>
						          	<th width="15%">Container</th>
						          	<th width="10%">Qty (Kg)</th>
						          	<th width="15%">Vehicle</th>
						          	<th width="5%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($stuffs as $stuff)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>

							            @if($stuff->schedulestuff != 0)
							            	<td>{{$stuff->fkSchedulestuff->fkSchedulebook->fkQuotationsplit->no_split}}</td>
						            	@else
						            		<td>{{$stuff->fkQuotationsplit->no_split}}</td>
						            	@endif

							            <td>
							            	POL : {{$stuff->fkPortl->name}} - {{$stuff->fkPortl->fkCountry->name}} <br>
							            	POD : {{$stuff->fkPortd->name}} - {{$stuff->fkPortd->fkCountry->name}}
							            </td>
							            <td>
							            	ETD : {{$stuff->etd}} <br>
							            	ETA : {{$stuff->eta}}
							            </td>
							            <td>
							            	@if($stuff->fkContainer->size == 0)
				                    			{{ $stuff->fkContainer->name }}
										    @else
										    	{{ $stuff->fkContainer->size }}" {{ $stuff->fkContainer->name }}
									    	@endif
							            	
							            	<br>{{$stuff->vehicle}}
							            </td>
							            <td style="text-align: right;">{{$stuff->qty_kg}} &nbsp;</td>
							            <td>
							            	Nopol &nbsp;: {{$stuff->nopol}} <br>
							            	Driver &nbsp;: {{$stuff->driver}} <br>
							            	Mobile : {{$stuff->mobile}}
							            </td>
							            <td style="text-align: center;">
							            	@if($stuff->status == 2 && $stuff->status_load == 1)
							            		<font color="red">Re-Route <i class="glyphicon glyphicon-remove"></i></font>
						            		@elseif($stuff->status == 0 && $stuff->status_load == 0)
							            		<font color="red">Canceled <i class="glyphicon glyphicon-remove"></i></font>
						            		@elseif($stuff->status == 2)
							            		<font color="red">Rejected <i class="glyphicon glyphicon-remove"></i></font>
						            		@elseif($stuff->status == 1)
								            	@if($stuff->status_load == 0)
								            		<a href="{{ url('stockload/create',$stuff->id)}}" class="btn btn-xs btn-success">Loading</a>
							            		@else
							            			<a href="{{ url('stockload/create',$stuff->id)}}" class="btn btn-xs btn-warning">Loaded <i class="glyphicon glyphicon-ok"></i></a>
							            		@endif
						            		@endif
						            	</td>
							        </tr>
						        @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Split Proforma</td>
						          	<td>Port</td>
						          	<td>Date</td>
						          	<td>Container</td>
						          	<td>Qty (Kg)</td>
						          	<td>Vehicle</td>
						          	<td class="noShow"></td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop