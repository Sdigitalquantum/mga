@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_detail" data-toggle="tab">Detail</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_detail">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Detail Purchase General</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('purchasedetailprg.update', $purchasedetailprg->id) }}">
				      		@method('PATCH')
				      		@csrf
				      		<div class="row">
    							<div class="col-md-6">
    								<input type="hidden" name="purchasegeneral" value="{{ $purchasedetailprg->purchasegeneral }}">
						      		<div class="form-group">
						              	<label for="no_prg">No. PR</label>
						              	<input type="text" class="form-control" name="no_prg" value="{{ $purchasedetailprg->fkPurchasegeneral->no_prg }}" disabled>
						          	</div>
						      		<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" required>
						              		@foreach($products as $product)
											    @if ($purchasedetailprg->product == $product->id)
												    <option value="{{ $product->id }}" selected>{{ $product->code }} - {{ $product->name }}</option>
												@else
												    <option value="{{ $product->id }}">{{ $product->code }} - {{ $product->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="supplierdetail">Supplier</label>
						              	<select class="form-control select2" name="supplierdetail" required>
						              		@if ($purchasedetailprg->supplierdetail == 0)
											    <option value="0" selected>All Supplier</option>
											@endif

											@foreach($supplierdetails as $supplierdetail)
											    @if ($purchasedetailprg->supplierdetail == $supplierdetail->id)
												    <option value="{{ $supplierdetail->id }}" selected>{{ $supplierdetail->name }}</option>
												@else
												    <option value="{{ $supplierdetail->id }}">{{ $supplierdetail->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="warehouse">Warehouse</label>
						              	<select class="form-control select2" name="warehouse" required>
						              		@php ($array = [])
									    	@foreach($employeewarehouses as $employeewarehouse)
									    		@php ($array[] = $employeewarehouse->warehouse)
									    	@endforeach
									    	
						              		@if ($purchasedetailprg->warehouse == 0)
											    <option value="0" selected>All Warehouse</option>
											@endif

											@foreach($warehouses as $warehouse)
											    @if ($purchasedetailprg->warehouse == $warehouse->id)
												    <option value="{{ $warehouse->id }}" selected>{{ $warehouse->name }}</option>
												@else
												    @if(in_array($warehouse->id, $array))
												    	<option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
											    	@endif
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_request">Qty Request (Kg)</label>
						              	<input type="number" class="form-control" name="qty_request" value="{{ $purchasedetailprg->qty_request }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="button">&nbsp;</label> <br>
						              	<button type="submit" class="btn btn-sm btn-success">Update</button>
				          				<a href="{{ url('/purchasedetailprg/create', $purchasedetailprg->purchasegeneral) }}" class="btn btn-sm btn-danger">Cancel</a>
						          	</div>
								</div>
							</div>
						</form>

						<br><table class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #001F3F !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="20%">Product</th>
						          	<th width="25%">Supplier</th>
						          	<th width="25%">Warehouse</th>
						          	<th width="10%">Qty (Kg)</th>
						          	<th width="10%">Unit</th>
						          	<th width="10%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@foreach($purchasedetailprgs as $purchasedetailprg)
						    		<tr>
							            <td>{{$purchasedetailprg->fkProduct->name}}</td>
							            <td>
							            	@if($purchasedetailprg->supplierdetail == 0)
							            		All Supplier
							            	@else
							            		{{$purchasedetailprg->fkSupplierdetail->name}}
						            		@endif
						            	</td>
							            <td>
							            	@if($purchasedetailprg->warehouse == 0)
							            		All Warehouse
							            	@else
							            		{{$purchasedetailprg->fkWarehouse->name}}
						            		@endif
						            	</td>
							            <td style="text-align: right;">{{number_format($purchasedetailprg->qty_request, 0, ',' , '.')}} &nbsp;</td>
							            <td>{{$purchasedetailprg->fkProduct->fkUnit->name}}</td>

							            @if($purchasedetailprg->status == 1)
								            <td>
								            	<a href="{{ route('purchasedetailprg.edit',$purchasedetailprg->id)}}" class="btn btn-xs btn-warning">Edit</a>
								            </td>
							            @else
							            	<td>
							            		<font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
							            	</td>
							            @endif
							        </tr>
						        @endforeach
						    </tbody>
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop