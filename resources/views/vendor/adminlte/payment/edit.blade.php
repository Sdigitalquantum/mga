@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Payment</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('payment.update', $payment->id) }}">
				      		@method('PATCH')
				      		@csrf
				          	<div class="form-group">
				          		<label for="value">Value (%)</label>
				              	<input type="number" class="form-control" name="value" value="{{ $payment->value }}" required>
				          	</div>
				          	<div class="form-group">
				              	<label for="condition">Condition</label>
				              	<input type="text" class="form-control" name="condition" value="{{ $payment->condition }}" required>
				          	</div>
				          	<button type="submit" class="btn btn-sm btn-success">Update</button>
				          	<a href="{{ url('/payment') }}" class="btn btn-sm btn-danger">Cancel</a>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop