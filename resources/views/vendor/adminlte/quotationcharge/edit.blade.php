@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
            <li class="disabled"><a>Payment</a></li>
            <li class="active"><a href="#tab_charge" data-toggle="tab">Charges</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_charge">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Quotation Charges <b>{{ $quotationcharge->fkQuotation->no_sp }}</b></h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-3">
				  				<form method="post" action="{{ route('quotationcharge.update', $quotationcharge->id) }}">
						      		@method('PATCH')
						      		@csrf
						      		<input type="hidden" class="form-control" name="quotation" value="{{ $quotationcharge->quotation }}">
						      		<div class="form-group">
						          		<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ $quotationcharge->name }}" required>
						          	</div>
						      		<div class="form-group">
						          		<label for="type">Action</label> <br>
						          		@if($quotationcharge->type == 1)
						          			<input type="radio" name="type" value="1" checked required> Plus &nbsp;&nbsp;&nbsp;
						          			<input type="radio" name="type" value="2"> Minus
						          		@elseif($quotationcharge->type == 2)
						          			<input type="radio" name="type" value="1" required> Plus &nbsp;&nbsp;&nbsp;
						          			<input type="radio" name="type" value="2" checked> Minus
						          		@endif
						          	</div>
						          	<div class="form-group">
						          		<label for="value">Value</label>
						              	<input type="number" class="form-control" name="value" value="{{ $quotationcharge->value }}" step=".01" required>
						          	</div>
						          	<button type="submit" class="btn btn-sm btn-success">Update</button>
					          		<a href="{{ url('/quotationcharge/'.$quotationcharge->quotation) }}" class="btn btn-sm btn-danger">Cancel</a>
						      	</form>
			  				</div>

			  				<div class="col-md-9">
			  					<br><table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="45%">Name</th>
								          	<th width="40%">Value</th>
								          	<th width="15%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($quotationcharges as $quotationcharge)
								    		<tr>
									            <td>{{$quotationcharge->name}}</td>
									            <td style="text-align: right;">
									            	@if($quotationcharge->type == 1) (+)
									            	@elseif($quotationcharge->type == 2) (-)
									            	@endif

									            	{{$quotationcharge->value}} &nbsp;
									            </td>
												<td>
									            	@if($quotationcharge->status == 1) <a href="{{ route('quotationcharge.edit',$quotationcharge->id)}}" class="btn btn-xs btn-warning">Edit</a>
									            	@else <font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
									            	@endif
									            </td>
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
			  				</div>
		  				</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop