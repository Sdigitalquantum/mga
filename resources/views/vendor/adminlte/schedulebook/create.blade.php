@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_book" data-toggle="tab">Booking</a></li>
            <li class="disabled"><a>Schedule</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_book">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Booking Schedule &nbsp;<a href="{{ url('schedulebook') }}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-3">
				  				<form method="post" action="{{ route('schedulebook.store') }}">
						          	@csrf
						          	<input type="hidden" class="form-control" name="scheduleship" value="{{ $id }}">
						          	<div class="form-group">
						              	<label for="quotationsplit">Split Proforma</label>
						              	<select class="form-control select2" name="quotationsplit" required>
						              		<option value="{{ old('quotationsplit') }}">Choose One</option>
					                    	@foreach($quotationsplits as $quotationsplit)
											    <option value="{{ $quotationsplit->id }}">{{ $quotationsplit->no_split }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="notice">Note</label>
						          		<textarea class="form-control" name="notice" maxlength="150" rows="3"></textarea>
						          	</div>
						          	<button type="submit" class="btn btn-sm btn-success">Save</button>
						      	</form>
			  				</div>

			  				<div class="col-md-9">
			  					<br><table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="20%">Split Proforma</th>
								          	<th width="20%">Port</th>
								          	<th width="15%">Date</th>
								          	<th width="10%">Lines</th>
								          	<th width="30%">Note</th>
								          	<th width="5%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($schedulebooks as $schedulebook)
								    		<tr>
									            <td>{{$schedulebook->fkQuotationsplit->no_split}}</td>
									            <td>
									            	POL : {{$schedulebook->pol}} <br> 
									            	POD : {{$schedulebook->pod}}
									            </td>
									            <td>
									            	ETD : {{$schedulebook->etd}} <br>
									            	ETA : {{$schedulebook->eta}}
									            </td>
									            <td>{{$schedulebook->lines}}</td>
									            <td><pre>{{$schedulebook->notice}}</pre></td>

									            @if($schedulebook->status_approve == 0)
										            <td>
										            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$schedulebook->id}}">Delete</button>

										            	<form action="{{ route('schedulebook.destroy', $schedulebook->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<input type="hidden" class="form-control" name="scheduleship" value="{{ $id }}">
										                  	<div class="modal fade bs-example-modal{{$schedulebook->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-danger">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to delete this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
										            </td>
									            @elseif($schedulebook->status_approve == 1)
								            		<td><font color="green">Approved <i class="glyphicon glyphicon-ok"></i></font></td>
							            		@elseif($schedulebook->status_approve == 2)
								            		<td><font color="red">Rejected <i class="glyphicon glyphicon-remove"></i></font></td>
												@endif
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
			  				</div>
		  				</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop