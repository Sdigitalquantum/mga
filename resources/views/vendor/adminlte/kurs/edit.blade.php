@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Kurs</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('kurs.update', $kurs->id) }}">
				      		@method('PATCH')
				      		@csrf
				          	<div class="form-group">
				              	<label for="currency">Currency</label>
				              	<select name="currency" class="form-control select2" required>
				              		@foreach($currencys as $currency)
				              			@if ($kurs->currency == $currency->id)
										    <option value="{{ $currency->id }}" selected>{{ $currency->code }} - {{ $currency->name }}</option>
										@else
										    <option value="{{ $currency->id }}">{{ $currency->code }} - {{ $currency->name }}</option>
										@endif
									@endforeach
								</select>
				          	</div>
				          	<div class="form-group">
				              	<label for="value">Value</label>
				              	<input type="number" class="form-control" name="value" value="{{ $kurs->value }}" required>
				          	</div>
				          	<div class="form-group">
				          		<label for="date_start">Start</label>
				              	<input type="text" class="form-control datepicker" name="start" value="{{ $kurs->start }}" required>
				          	</div>
				          	<div class="form-group">
				          		<label for="date_end">End</label>
				              	<input type="text" class="form-control datepicker" name="end" value="{{ $kurs->end }}" required>
				          	</div>
				          	<button type="submit" class="btn btn-sm btn-success">Update</button>
				          	<a href="{{ url('/kurs') }}" class="btn btn-sm btn-danger">Cancel</a>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop