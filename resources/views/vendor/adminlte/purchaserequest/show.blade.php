@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_show" data-toggle="tab">Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_show">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Show Purchase Request
			    		 	&nbsp;<a href="javascript:window.print()" class="btn btn-xs btn-default" style="text-decoration: none;"><i class="glyphicon glyphicon-print"></i></a>
			    			&nbsp;<a href="{{ url('/purchaserequest') }}" class="btn btn-xs btn-danger">Back</a>
			    		</h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div id="printable">
					      	<div class="row">
					      		<center>
				  					<b>{{$company->name}}</b> <br>
					  				PURCHASE REQUEST <br>
					  				DOCUMENT INTERNAL <br>
					  				DATE : {{ date('d F Y', strtotime("now")) }} <br>
					  			</center><br>

    							<div class="col-md-6">
    								<div class="form-group">
						          		<label for="date_request">Date</label>
						              	<input type="text" class="form-control datepicker" name="date_request" value="{{ $purchaserequest->date_request }}" disabled>
						          	</div>
									<div class="form-group">
						              	<label for="product">Product</label>
						              	@foreach($products as $product)
										    @if ($purchaserequest->product == $product->id)
												<input type="text" class="form-control" name="product" value="{{ $product->code }} - {{ $product->name }}" disabled>
											@endif
										@endforeach
						          	</div>
						          	<div class="form-group">
						              	<label for="supplierdetail">Supplier</label>
						              	@if ($purchaserequest->supplierdetail == 0)
										    <input type="text" class="form-control" name="supplierdetail" value="All Supplier" disabled>
										@endif

										@foreach($supplierdetails as $supplierdetail)
										    @if ($purchaserequest->supplierdetail == $supplierdetail->id)
												<input type="text" class="form-control" name="supplierdetail" value="{{ $supplierdetail->name }}" disabled>
											@endif
										@endforeach
						          	</div>
						          	<div class="form-group">
						              	<label for="warehouse">Warehouse</label>
						              	@if ($purchaserequest->warehouse == 0)
										    <input type="text" class="form-control" name="warehouse" value="All Warehouse" disabled>
										@endif

										@foreach($warehouses as $warehouse)
										    @if ($purchaserequest->warehouse == $warehouse->id)
												<input type="text" class="form-control" name="warehouse" value="{{ $warehouse->name }}" disabled>
											@endif
										@endforeach
						          	</div>
						          	<div class="form-group">
						          		<label for="qty_request">Qty Request</label>
						              	<input type="number" class="form-control" name="qty_request" value="{{ $purchaserequest->qty_request }}" disabled>
						          	</div>
						          	<div class="form-group">
						          		<label for="no_reference">No. Reference</label>
						              	<input type="text" class="form-control" name="no_reference" value="{{ $purchaserequest->no_reference }}" disabled>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						          		<label for="notice">Note</label>
						          		<textarea class="form-control" name="notice" rows="9" disabled>{{ $purchaserequest->notice }}</textarea>
						          	</div>
						          	<div class="form-group">
						          		<label for="reason">Reason</label>
						          		<textarea class="form-control" name="reason" rows="8" disabled>{{ $purchaserequest->reason }}</textarea>
						          	</div>
								</div>
							</div>
			          	</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop