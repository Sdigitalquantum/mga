@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">New Konversi</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('konversi.store') }}">
				          	@csrf
				          	<div class="row">
				          		<div class="col-md-6">
						          	<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" required>
						              		<option value="{{ old('product') }}">Choose One</option>
					                    	@foreach($products as $product)
											    <option value="{{ $product->id }}">{{ $product->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="unit_src">Unit Asal</label>
						              	<select class="form-control select2" name="unit_src">
						              		<option value="{{ old('unit_src') }}">Choose One</option>
					                    	@foreach($units as $unit)
											    <option value="{{ $unit->id }}">{{ $unit->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="unit_dst">Unit Tujuan</label>
						              	<select class="form-control select2" name="unit_dst" required>
						              		<option value="{{ old('unit_dst') }}">Choose One</option>
					                    	@foreach($units as $unit)
											    <option value="{{ $unit->id }}">{{ $unit->name }}</option>
											@endforeach
					                	</select>
									  <div class="form-group">
						          		<label for="qty_result">qty hasil</label>
						              	<input type="number" class="form-control" name="qty_result" value="{{ old('qty_result') }}" required>
						          	</div>
									  <div class="form-group">
						          		<label for="keterangan">keterangan</label>
						              	<input type="text" class="form-control" name="keterangan" value="{{ old('keterangan') }}" required>
						          	</div>
							  </div>
							
						          	<div class="form-group">
						          		<label for="check">&nbsp;</label>
						          		<input type="checkbox" name="general" value="1"> General &nbsp;&nbsp;&nbsp;
						              	<input type="checkbox" name="status_group" value="1"> Status Group &nbsp;&nbsp;&nbsp;
						              	<input type="checkbox" name="status_record" value="1"> Status Record <br>
						          		
						          		<button type="submit" class="btn btn-sm btn-success">Save</button>
						          		<a href="{{ url('/product') }}" class="btn btn-sm btn-danger">Cancel</a>
					          		</div>
			          			</div>
		          			</div>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop