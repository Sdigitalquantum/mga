@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Konversi</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('konversi.update', $konversis->id) }}">
				      		@method('PATCH')
				      		@csrf
							  <div class="row">
				          		<div class="col-md-6">	
				          	<div class="form-group">
							  <label for="product">Product</label>
									<select class="form-control select2" name="product">
										@foreach($products as $product)
											@if ($konversis->product == $product->id)
												<option value="{{ $product->id }}" selected>{{ $product->name }}</option>
											@else
												<option value="{{ $product->id }}">{{ $product->name }}</option>
											@endif
										@endforeach
									</select>
				          	</div>
							<div class="form-group">
								<label for="unit_src">Unit Source</label>
									<select class="form-control select2" name="unit_src">
										@foreach($units as $unit)
											@if ($konversis->unit_src == $unit->id)
												<option value="{{ $unit->id }}" selected>{{ $unit->name }}</option>
											@else
												<option value="{{ $unit->id }}">{{ $unit->name }}</option>
											@endif
										@endforeach
									</select>
							</div>
							<div class="form-group">
								<label for="unit_dst">Unit Destination</label>
									<select class="form-control select2" name="unit_dst">
										@foreach($units as $unit)
											@if ($konversis->unit_dst == $unit->id)
												<option value="{{ $unit->id }}" selected>{{ $unit->name }}</option>
											@else
												<option value="{{ $unit->id }}">{{ $unit->name }}</option>
											@endif
										@endforeach
									</select>
							</div>
							<div class="form-group">
						    	<label for="qty_result">Qty Hasil</label>
						       	<input type="number" class="form-control" name="qty_result" value="{{ $konversis->qty_result }}" required>
						    </div>
							<div class="form-group">
						    	<label for="keterangan">Keterangan</label>
						       	<input type="text" class="form-control" name="keterangan" value="{{ $konversis->keterangan }}" required>
						    </div>
				  	</div>
					  </div>
					  </div>
				          	<button type="submit" class="btn btn-sm btn-success">Update</button>
				          	<a href="{{ url('/konversi') }}" class="btn btn-sm btn-danger">Cancel</a>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop