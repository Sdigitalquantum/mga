@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Quality Control</a></li>
            <li class="disabled"><a>Detail</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Quality Control</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('stockqc.update', $stockqc->id) }}">
				      		@method('PATCH')
				      		@csrf
				      		<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						              	<label for="no_po">No. PO</label>
						              	<input type="hidden" name="purchasedetail" value="{{ $stockqc->purchasedetail }}">
						              	<input type="text" class="form-control" name="no_po" value="{{ $stockqc->no_po }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="qty_po">Qty (Kg)</label>
						              	<input type="text" class="form-control" value="PO : {{ $stockqc->fkPurchasedetail->qty_order+$stockqc->fkPurchasedetail->qty_buffer }} QC : {{ $stockqc->fkPurchasedetail->qty_qc }}" disabled>
						          	</div>
									<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" required>
						              		@foreach($products as $product)
											    @if ($stockqc->product == $product->id)
												    <option value="{{ $product->id }}" selected>{{ $product->code }} - {{ $product->name }}</option>
												@else
												    <option value="{{ $product->id }}">{{ $product->code }} - {{ $product->name }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="warehouse">Warehouse</label>
						              	<select class="form-control select2" name="warehouse" required>
						              		@php ($array = [])
									    	@foreach($employeewarehouses as $employeewarehouse)
									    		@php ($array[] = $employeewarehouse->warehouse)
									    	@endforeach
									    	
						              		@foreach($warehouses as $warehouse)
											    @if ($stockqc->warehouse == $warehouse->id)
												    <option value="{{ $warehouse->id }}" selected>{{ $warehouse->code }} - {{ $warehouse->name }}</option>
												@else
												    @if(in_array($warehouse->id, $array))
												    	<option value="{{ $warehouse->id }}">{{ $warehouse->code }} - {{ $warehouse->name }}</option>
											    	@endif
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="supplierdetail">Supplier</label>
						              	<input type="hidden" name="supplierdetail" value="{{ $supplierdetail->id }}">
						              	<input type="text" class="form-control" value="{{ $supplierdetail->name }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="date_in">Date</label>
						              	<input type="text" class="form-control datepicker" name="date_in" value="{{ $stockqc->date_in }}" required>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="qty_bag">Qty Bag</label>
						              	<input type="number" class="form-control" name="qty_bag" value="{{ $stockqc->qty_bag }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="qty_pcs">Qty Pcs</label>
						              	<input type="number" class="form-control" name="qty_pcs" value="{{ $stockqc->qty_pcs }}" required>
						          	</div>
						          	<div class="form-group">
						              	<label for="qty_kg">Qty Kg</label>
						              	<input type="number" class="form-control" name="qty_kg" value="{{ $stockqc->qty_kg }}" required>
						          	</div>
						          	<div class="form-group">
						          		<label for="notice">Note</label>
						          		<textarea class="form-control" name="notice" rows="5" maxlength="150">{{ $stockqc->notice }}</textarea>
						          	</div>
						          	<div class="form-group">
						          		<label for="button"></label> <br>
						          		<button type="submit" class="btn btn-sm btn-success">Update</button>
				          				<a href="{{ url('/stockqc/create', $stockqc->purchasedetail) }}" class="btn btn-sm btn-danger">Cancel</a>
						          	</div>
								</div>
							</div>
						</form>

						<br><table class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #001F3F !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="30%">Detail PO</th>
						          	<th width="20%">Information</th>
						          	<th width="15%">Quantity</th>
						          	<th width="20%">Note</th>
						          	<th width="10%">Action</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($tot_bag = 0)
						    	@php ($tot_pcs = 0)
						    	@php ($tot_kg = 0)
						    	@php ($tot_avg = 0)
						    	@foreach($stockqcs as $stockqc)
						    		<tr>
							            <td>
							            	Product &nbsp;: {{$stockqc->fkProduct->name}} <br>
							            	Supplier : {{$stockqc->fkSupplierdetail->name}} <br>
						            		Warehouse : {{$stockqc->fkWarehouse->name}}
						            	</td>
							            <td>
							            	No. PO : {{$stockqc->no_po}} <br>
							            	Date : {{$stockqc->date_in}}
						            	</td>
							            <td style="text-align: right;">
							            	Bag : {{number_format($stockqc->qty_bag, 0, ',' , '.')}} <br>
							            	Pcs : {{number_format($stockqc->qty_pcs, 0, ',' , '.')}} <br>
							            	Kg : {{number_format($stockqc->qty_kg, 0, ',' , '.')}} <br>

							            	@if($stockqc->detail_count == 0)
							            		Avg : {{number_format($stockqc->qty_avg, 0, ',' , '.')}}&nbsp;
							            		@php ($tot_avg = $tot_avg + $stockqc->qty_avg)
						            		@else
						            			Avg : {{round($stockqc->detail_total/$stockqc->detail_count,2)}}&nbsp;
						            			@php ($tot_avg = $tot_avg + ($stockqc->detail_total/$stockqc->detail_count))
						            		@endif
							            </td>
						            	<td>
							            	<pre>{{$stockqc->notice}}</pre>

							            	@if($stockqc->no_inc != 0 && $stockqc->status_approve == 1)
							            		<br>Reason : <pre>{{$stockqc->reason}}</pre>
							            	@endif
							            </td>
										<td>
											@if($stockqc->no_inc == 0)
							            		<a href="{{ route('stockqc.edit',$stockqc->id)}}" class="btn btn-xs btn-warning">Edit</a>
						            		@elseif($stockqc->no_inc == 0 && $stockqc->status == 0)
						            			<font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
						            		@endif

							            	@if($stockqc->no_inc != 0 && $stockqc->status_approve == 0)
							            		<a href="{{ route('stockqc.edit',$stockqc->id)}}" class="btn btn-xs btn-warning">Edit</a>
							                	<br><br><font color="blue">Waiting Approval</font>
						                	@elseif($stockqc->no_inc != 0 && $stockqc->status == 0)
						            			<font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
							                @elseif($stockqc->no_inc != 0 && $stockqc->status_approve == 1)
							                	<font color="green">Derivative Approved <i class="glyphicon glyphicon-ok"></i></font>
							                @elseif($stockqc->no_inc != 0 && $stockqc->status_approve == 2)
							                	<font color="red">Derivative Rejected <i class="glyphicon glyphicon-remove"></i></font>
							                @endif
										</td>
							        </tr>

							        @php ($tot_bag = $tot_bag + $stockqc->qty_bag)
							    	@php ($tot_pcs = $tot_pcs + $stockqc->qty_pcs)
							    	@php ($tot_kg = $tot_kg + $stockqc->qty_kg)
						        @endforeach
						    </tbody>
						    	@if($tot_bag != 0)
							    	<tr>
							    		<td style="text-align: right; font-weight: bold;" colspan="2">Total Qty &nbsp;</td>
							    		<td style="text-align: right;">
							    			Bag : {{number_format($tot_bag, 0, ',' , '.')}} <br>
							            	Pcs : {{number_format($tot_pcs, 0, ',' , '.')}} <br>
							            	Kg : {{number_format($tot_kg, 0, ',' , '.')}} <br>
							            	Avg : {{round($tot_avg,2)}} <br>
							    		</td>
						    			<td colspan="2">&nbsp;</td>
						    		</tr>
					    		@endif
					  	</table>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop