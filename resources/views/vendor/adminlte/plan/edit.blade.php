@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_plan" data-toggle="tab">Plan</a></li>
            <li class="disabled"><a>Booking</a></li>
            <li class="disabled"><a>Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_plan">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Delivery Plan &nbsp;<a href="{{ url('plan')}}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
			      		<div class="row">
							<div class="col-md-4">
								<table class="table table-bordered table-hover nowrap" width="100%">
								    <tr style="background: #001F3F !important; font-weight: bold; text-align: center; color: white;">
							          	<th colspan="2">Quotation</th>
							        </tr>
								    <tr>
							            <td width="50%">{{$quotationdetail->fkQuotation->no_sp}}</td>
							            <td width="50%">Date : {{$quotationdetail->fkQuotation->date_sp}}</td>
							        </tr>
							        <tr>
							            <td>{{$quotationdetail->fkQuotation->fkCustomerdetail->name}}</td>
							            <td>
							            	@if(empty($quotationdetail->alias_name))
							            		{{$quotationdetail->fkProduct->name}}
						            		@else
						            			{{$quotationdetail->alias_name}}
						            		@endif
							            </td>
							        </tr>
							        <tr>
							            <td>POL : {{$quotationdetail->fkQuotation->fkPortl->name}} - {{$quotationdetail->fkQuotation->fkPortl->fkCountry->name}}</td>
							            <td>POD : {{$quotationdetail->fkQuotation->fkPortd->name}} - {{$quotationdetail->fkQuotation->fkPortd->fkCountry->name}}</td>
							        </tr>
							        <tr>
							            <td>ETD : {{$quotationdetail->fkQuotation->etd}}</td>
							            <td>ETA &nbsp;: {{$quotationdetail->fkQuotation->eta}}</td>
							        </tr>
							        <tr>
							            <td>Qty Need (Kg) :
							            	{{number_format($quotationdetail->qty_kg, 0, ',' , '.')}}
							            	@if($quotationdetail->qty_amount == 0)
							            		<i class="glyphicon glyphicon-ok"></i>
							            	@endif
							            </td>
							            <td>Qty Remained (Kg) :
							            	@if($quotationdetail->qty_amount == 0)
							            		<font color="green"><b>{{number_format($quotationdetail->qty_amount, 0, ',' , '.')}}</b></font>
							            	@else
							            		<font color="red"><b>{{number_format($quotationdetail->qty_amount, 0, ',' , '.')}}</b></font>
							            	@endif
							            </td>
							        </tr>

								    <input type="hidden" id="notice" value="{{$quotationdetail->fkQuotation->notice}}">
							  	</table>
							</div>

							<div class="col-md-8">
								<table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #00a65a !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="25%" style="text-align: center;">No. Batch</th>
								          	<th width="25%">Warehouse</th>
								          	<th width="30%">Supplier</th>
								          	<th width="10%" style="text-align: center;">Plan (Kg)</th>
								          	<th width="5%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@php ($total = 0)
								    	@foreach($plans as $plan)
								    		<tr>
									            <td style="text-align: center;">{{$plan->no_batch}}</td>
									            <td>{{$plan->fkStockin->fkWarehouse->name}}</td>
									            <td>
									            	{{$plan->fkStockin->fkSupplierdetail->name}} <br>
									            	Incoming Date : {{$plan->fkStockin->date_in}}
									            </td>
									            <td style="text-align: right;">{{number_format($plan->qty_plan, 0, ',' , '.')}} &nbsp;</td>
												<td>
													<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$plan->id}}">Delete</button>

									            	<form action="{{ route('plan.destroy', $plan->id)}}" method="post">
									                  	@csrf
									                  	@method('DELETE')
									                  	<div class="modal fade bs-example-modal{{$plan->id}}" tabindex="-1" role="dialog" aria-hidden="true">
						                                    <div class="modal-dialog modal-sm">
						                                        <div class="modal-content">
						                                            <div class="modal-header btn-danger">
						                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
						                                                </button>
						                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
						                                            </div>
						                                            <div class="modal-body">
						                                                Are you sure to delete this data?
						                                            </div>
						                                            <div class="modal-footer">
						                                                <button type="submit" class="btn btn-success">Yes</button>
						                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
						                                            </div>
						                                        </div>
						                                    </div>
						                                </div>
									                </form>
												</td>
									        </tr>
									        @php ($total = $total + $plan->qty_plan)
								        @endforeach
								        <tr>
								        	<td colspan="3" style="text-align: right; font-weight: bold;">Total Qty</td>
								        	<td style="text-align: right; font-weight: bold;">{{$total}} &nbsp;</td>
								        	<td>&nbsp;</td>
							        	</tr>
								    </tbody>
							  	</table>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="example" class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="5%">No</th>
								          	<th width="20%">Warehouse</th>
								          	<th width="25%">Supplier</th>
								          	<th style="text-align: center;" width="15%">Incoming Date</th>
								          	<th style="text-align: center;" width="15%">Qty Available (Kg)</th>
								          	<th style="text-align: center;" width="15%">Qty Plan (Kg)</th>
								          	<th width="5%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@php ($array = [])
								    	@foreach($employeewarehouses as $employeewarehouse)
								    		@php ($array[] = $employeewarehouse->warehouse)
								    	@endforeach

								    	@php ($no = 0)
								    	@foreach($stockins as $stockin)
								    		@if(in_array($stockin->warehouse, $array))
									    		@php ($no++)
									        	<tr>
										            <td style="text-align: center;">{{$no}}</td>
										            <td>{{$stockin->fkWarehouse->name}}</td>
										            <td>{{$stockin->fkSupplierdetail->name}}</td>
										            <td style="text-align: center;">{{$stockin->date_in}}</td>
										            <td style="text-align: right;">{{number_format($stockin->qty_available, 0, ',' , '.')}} &nbsp;</td>
										            <td>
										            	<form method="post" action="{{ route('plan.update', $id) }}">
												      		@method('PATCH')
												      		@csrf
												      		<input type="hidden" name="quotationdetail" value="{{ $id }}">
												      		<input type="hidden" name="stockin" value="{{ $stockin->id }}">
												      		<input type="number" name="qty_plan" required>
										            </td>
													<td><button type="submit" class="btn btn-xs btn-success">Choose</button></td>
														</form>
										        </tr>
									        @endif
								        @endforeach
								    </tbody>

								    <tfoot>
								        <tr>
								          	<td class="noShow"></td>
								          	<td>Warehouse</td>
								          	<td>Supplier</td>
								          	<td>Date</td>
								          	<td>Qty Available (Kg)</td>
								          	<td class="noShow"></td>
								          	<td class="noShow"></td>
								        </tr>
								    </tfoot>
							  	</table>
							</div>
						</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop