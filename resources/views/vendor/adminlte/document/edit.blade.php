@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="active"><a href="#tab_edit" data-toggle="tab">Edit</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_edit">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Document</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('document.update', $document->id) }}">
				      		@method('PATCH')
				      		@csrf
				          	<div class="form-group">
				              	<label for="code">Code</label>
				              	<input type="text" class="form-control" name="code" value="{{ $document->code }}" disabled>
				          	</div>
				          	<div class="form-group">
				              	<label for="name">Name</label>
				              	<input type="text" class="form-control" name="name" value="{{ $document->name }}" required>
				          	</div>
				          	<div class="form-group">
				          		<label for="flow">Flow</label> <br>
				          		@if($document->export == 1)
				          			<input type="checkbox" name="export" value="1" checked> Export &nbsp;&nbsp;&nbsp;
				          		@else
				          			<input type="checkbox" name="export" value="1"> Export &nbsp;&nbsp;&nbsp;
				          		@endif

				          		@if($document->local == 1)
				          			<input type="checkbox" name="local" value="1" checked> Local &nbsp;&nbsp;&nbsp;
				          		@else
				          			<input type="checkbox" name="local" value="1"> Local &nbsp;&nbsp;&nbsp;
				          		@endif

				          		@if($document->internal == 1)
				          			<input type="checkbox" name="internal" value="1" checked> Internal
				          		@else
				          			<input type="checkbox" name="internal" value="1"> Internal
				          		@endif
				          	</div>
				          	<button type="submit" class="btn btn-sm btn-success">Update</button>
				          	<a href="{{ url('/document') }}" class="btn btn-sm btn-danger">Cancel</a>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop