@extends('adminlte::frontend/page')

@section('content')
    <section class='madeforyou padding-top-100 padding-bottom-0'>
        <div class="angled_down_inside white">
            <div class="slope upleft"></div>
            <div class="slope upright"></div>
        </div>
        
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-area">
                    <h1 class="heading left-align">Profile</h1>
                    <div class="headul left-align"></div>
                    <p>
                        <b>Goals</b> <br>
                        Creating a product that has its own uniqueness , Special products should not be expensive
                        Continuous cooperation with our business partners to achieve mutual prosperity, 
                        Having consumen base and strong market share and stable
                    </p>
                    <p>
                        <b>About Us</b>
                        <ul>
                            <li>We are one of the growing tobacco companies in Kudus - Central Java, was established in early 2005</li>
                            <li>Supported by a team of research and development that reliable and experiences. Our efforts to create a cigarette with the taste, aroma and distinct pleasure, finally yielded satisfactory results</li>
                            <li>Some of our tobacco products became the mainstay and already has a strong customer base.</li>
                        </ul>
                    </p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 slider-area">
                    <ul id="image-slider" class="owl-carousel">
                        <li class="">
                            <img alt="blog-image" class="lazyOwl img-responsive" data-src="{{ asset('vendor/caliber/img/saa1.jpg') }}" src="#">
                        </li>
                        <li class="">
                            <img alt="blog-image" class="lazyOwl img-responsive" data-src="{{ asset('vendor/caliber/img/saa2.jpg') }}" src="#">
                        </li>
                        <li class="">
                            <img alt="blog-image" class="lazyOwl img-responsive" data-src="{{ asset('vendor/caliber/img/saa3.jpg') }}" src="#">
                        </li>
                        <li class="">
                            <img alt="blog-image" class="lazyOwl img-responsive" data-src="{{ asset('vendor/caliber/img/saa4.jpg') }}" src="#">
                        </li>
                        <li class="">
                            <img alt="blog-image" class="lazyOwl img-responsive" data-src="{{ asset('vendor/caliber/img/saa5.jpg') }}" src="#">
                        </li>
                        <li class="">
                            <img alt="blog-image" class="lazyOwl img-responsive" data-src="{{ asset('vendor/caliber/img/saa6.jpg') }}" src="#">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="angled_up_inside white">
            <div class="slope upleft"></div>
            <div class="slope upright"></div>
        </div>
    </section>

    <section class='bg-primary services max-width-100 padding-bottom-0' id='services'>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <h1 class="heading text-white left-align">Services</h1>
                    <div class="headul white left-align"></div>
                    <p><img src="{{ asset('vendor/caliber/img/saa_kelapa.png') }}"></p>
                    <p class="text-white left-align">BEST PRODUCT</p>
                    <p>Cigarettes with special taste</p>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <h1 class="heading text-white left-align">&nbsp;</h1>
                    <div class="white left-align"><br><br><br></div>
                    <p><img src="{{ asset('vendor/caliber/img/saa_customer.png') }}"></p>
                    <p class="text-white left-align">CUSTOMER GLOBAL</p>
                    <p>We open up opportunities for those who want to participate distribute our products with profit share, whether it's Retailer / Shop, Wholeseller or Distributor</p>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <h1 class="heading text-white left-align">&nbsp;</h1>
                    <div class="white left-align"><br><br><br></div>
                    <p><img src="{{ asset('vendor/caliber/img/saa_sosial.png') }}"></p>
                    <!-- <p><img src="{{ asset('vendor/caliber/img/saa_sosial.png')}}"> </p> -->
                    <p class="text-white left-align">COMPANY SOCIAL RESPONSIBILITY</p>
                    <p>We participate and support programs implemented by the government of Indonesia</p>
                </div>
            </div>
        </div>

        <div class="angled_down_outside outside primary lightgray">
            <div class="slope downleft"></div>
            <div class="slope downright"></div>
        </div>
    </section>
@stop
