@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="active"><a href="#tab_list" data-toggle="tab">List</a></li>
          	<li class="disabled"><a>Cut Off</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_list">
				<div class="box box-primary">
				  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
				  		<h4 class="box-title">Cut Off
				  			&nbsp;<a href="{{ route('receivablecutoff.create')}}" class="btn btn-xs btn-success" style="text-decoration: none;">Input Total</a>
				  			&nbsp;<a href="{{ url('exportreceivablecard')}}" class="btn btn-xs btn-default" style="text-decoration: none;"><i class="fa fa-file-excel-o"></i></a>
				  		</h4>
			  		</div>
				  	
				  	<div class="box-body">
					  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
						    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
						        <tr>
						          	<th width="5%">No</th>
						          	<th width="25%">Customer</th>
						          	<th width="10%">Year</th>
						          	<th width="15%">Month</th>
						          	<th width="15%">Total</th>
						          	<th width="15%">Payment</th>
						          	<th width="15%">Saldo</th>
						        </tr>
						    </thead>

						    <tbody>
						    	@php ($no = 0)
						    	@foreach($receivablecards as $receivablecard)
						    		@php ($no++)
						        	<tr>
							            <td>{{$no}}</td>
							            <td>{{$receivablecard->fkCustomerdetail->name}}</td>
							            <td style="text-align: right;">{{$receivablecard->year}} &nbsp;</td>
							            <td>
							            	@if($receivablecard->month == 1) January
											@elseif($receivablecard->month == 2) February
											@elseif($receivablecard->month == 3) March
											@elseif($receivablecard->month == 4) April
											@elseif($receivablecard->month == 5) May
											@elseif($receivablecard->month == 6) June
											@elseif($receivablecard->month == 7) July
											@elseif($receivablecard->month == 8) August
											@elseif($receivablecard->month == 9) September
											@elseif($receivablecard->month == 10) October
											@elseif($receivablecard->month == 11) November
											@elseif($receivablecard->month == 12) Desember
											@endif
							            </td>
							            <td style="text-align: right;">{{number_format($receivablecard->debit, 0, ',' , '.')}} &nbsp;</td>
							            <td style="text-align: right;">{{number_format($receivablecard->kredit, 0, ',' , '.')}} &nbsp;</td>
							            <td style="text-align: right;">{{number_format($receivablecard->debit-$receivablecard->kredit, 0, ',' , '.')}} &nbsp;</td>
						            </tr>
					            @endforeach
						    </tbody>

						    <tfoot>
						        <tr>
						          	<td class="noShow"></td>
						          	<td>Customer</td>
						          	<td>Year</td>
						          	<td>Month</td>
						          	<td>Total</td>
						          	<td>Payment</td>
						          	<td>Saldo</td>
						        </tr>
						    </tfoot>
					  	</table>
				  	</div>
				<div>
            </div>
        </div>
  	</div>
@stop