@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Detail</a></li>
            <li class="active"><a href="#tab_payment" data-toggle="tab">Payment</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_payment">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Edit Quotation Payment <b>{{ $quotationpayment->fkQuotation->no_sp }}</b></h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-3">
				  				<form method="post" action="{{ route('quotationpayment.update', $quotationpayment->id) }}">
						      		@method('PATCH')
						      		@csrf
						      		<input type="hidden" class="form-control" name="quotation" value="{{ $quotationpayment->quotation }}">
						      		<div class="form-group">
						              	<label for="payment">Payment</label>
						              	<select class="form-control select2" name="payment" required>
						              		@foreach($payments as $payment)
											    @if ($quotationpayment->payment == $payment->id)
												    <option value="{{ $payment->id }}" selected>{{ $payment->value }}% - {{ $payment->condition }}</option>
												@else
												    <option value="{{ $payment->id }}">{{ $payment->value }}% - {{ $payment->condition }}</option>
												@endif
											@endforeach
					                	</select>
						          	</div>
						          	<button type="submit" class="btn btn-sm btn-success">Update</button>
					          		<a href="{{ url('/quotationpayment/'.$quotationpayment->quotation) }}" class="btn btn-sm btn-danger">Cancel</a>
						      	</form>
			  				</div>

			  				<div class="col-md-9">
			  					<br><table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="20%">Value</th>
								          	<th width="40%">Condition</th>
								          	<th width="25%">Payment</th>
								          	<th width="15%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@php ($payment = 0)
								    	@foreach($quotationpayments as $quotationpayment)
								    		<tr>
									            <td>{{$quotationpayment->fkPayment->value}}%</td>
									            <td>{{$quotationpayment->fkPayment->condition}}</td>
									            <td style="text-align: right;">
									            	@php ($total = 0)
					    							@foreach($quotationdetails as $quotationdetail)
					    								@if($quotationdetail->disc_type == 0)
										            		@php ($total = $total + ($quotationdetail->price*$quotationdetail->kurs*$quotationdetail->qty_kg))
										            	@elseif($quotationdetail->disc_type == 2)
										            		@php ($total = $total + ($quotationdetail->price*$quotationdetail->kurs*$quotationdetail->qty_kg) - ($quotationdetail->disc_value*$quotationdetail->fkQuotation->kurs))
										            	@else
										            		@php ($total = $total + ($quotationdetail->price*$quotationdetail->kurs*$quotationdetail->qty_kg) - ($quotationdetail->disc_value/100*($quotationdetail->price*$quotationdetail->kurs*$quotationdetail->qty_kg)))
									            		@endif
					    							@endforeach

					    							Rp. {{number_format($total*$quotationpayment->fkPayment->value/100, 0, ',' , '.')}} &nbsp;
									            </td>
												<td>
									            	@if($quotationpayment->status == 1) <a href="{{ route('quotationpayment.edit',$quotationpayment->id)}}" class="btn btn-xs btn-warning">Edit</a>
									            	@else <font color="red">Not Active <i class="glyphicon glyphicon-remove"></i></font>
									            	@endif
									            </td>

									            @if($quotationpayment->status == 1)
									            	@php ($payment = $payment + ($total*$quotationpayment->fkPayment->value/100))
								            	@endif
									        </tr>
								        @endforeach

								        <tr>
								        	<td colspan="2" style="text-align: right; font-weight: bold;">Total Payment &nbsp;</td>
								        	<td style="text-align: right;">Rp. {{number_format($payment, 0, ',' , '.')}} &nbsp;</td>
								        	<td>&nbsp;</td>
							        	</tr>
								    </tbody>
							  	</table>
			  				</div>
		  				</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop