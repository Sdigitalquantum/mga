@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="disabled"><a>Account</a></li>
            <li class="active"><a href="#tab_show" data-toggle="tab">Show</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_show">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">Show Company 
			    			&nbsp;<a href="javascript:window.print()" class="btn btn-xs btn-default" style="text-decoration: none;"><i class="glyphicon glyphicon-print"></i></a>
			    			&nbsp;<a href="{{ url('/company') }}" class="btn btn-xs btn-danger">Back</a>
			    		</h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div id="printable">
					      	<div class="row">
					      		<center>
				  					<b>{{$company->name}}</b> <br>
					  				COMPANY ACCOUNT <br>
					  				DOCUMENT INTERNAL <br>
					  				DATE : {{ date('d F Y', strtotime("now")) }} <br>
					  			</center><br>

    							<div class="col-md-6">
    								<div class="form-group">
						              	<label for="code">Code</label>
						              	<input type="text" class="form-control" name="code" value="{{ $company->code }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="name">Name</label>
						              	<input type="text" class="form-control" name="name" value="{{ $company->name }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="address">Address</label>
						              	<input type="text" class="form-control" name="address" value="{{ $company->address }}" disabled>
						          	</div>
						          	<div class="form-group">
						              	<label for="city">City</label>
						              	@foreach($citys as $city)
										    @if ($company->city == $city->id)
											    <input type="text" class="form-control" name="city" value="{{ $city->name }} - {{ $city->fkCountry->name }}" disabled>
											@endif
										@endforeach
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						          		<label for="email">Email</label>
						              	<input type="text" class="form-control" name="email" value="{{ $company->email }}" disabled>
						          	</div>
						          	<div class="form-group">
						          		<label for="mobile">Mobile</label>
						              	<input type="number" class="form-control" name="mobile" value="{{ $company->mobile }}" disabled>
						          	</div>
						          	<div class="form-group">
						          		<label for="fax">Fax</label>
						              	<input type="number" class="form-control" name="fax" value="{{ $company->fax }}" disabled>
						          	</div>
						          	<div class="form-group">
						          		<label for="status">Status</label> <br>
						              	@if ($company->status == 1)
						              		<font color="blue"><label for="status">Active</label></font>
						              	@else
						              		<font color="red"><label for="status">Not Active</label></font>
						              	@endif
						          	</div>
								</div>
							</div>

							<table class="table table-bordered table-hover nowrap" width="100%">
							    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
							        <tr>
							        	<th width="20%">Bank</th>
							          	<th width="25%">Account</th>
							          	<th width="20%">Name</th>
							          	<th width="20%">Alias</th>
							          	<th width="15%">Action</th>
							        </tr>
							    </thead>

							    <tbody>
							    	@foreach($companyaccounts as $companyaccount)
							    		<tr>
								            <td>
								            	{{$companyaccount->fkBank->name}} <br>
								            	@if($companyaccount->export == 1) Export <i class="glyphicon glyphicon-ok"></i> @else Export <i class="glyphicon glyphicon-remove"></i> @endif <br>
								            	@if($companyaccount->local == 1) Local <i class="glyphicon glyphicon-ok"></i> @else Local <i class="glyphicon glyphicon-remove"></i> @endif <br>
								            	@if($companyaccount->internal == 1) Internal <i class="glyphicon glyphicon-ok"></i> @else Internal <i class="glyphicon glyphicon-remove"></i> @endif
								            </td>
								            <td>
								            	{{$companyaccount->fkCurrency->code}} : {{$companyaccount->account_no}} <br>
								            	Swift : {{$companyaccount->account_swift}}
								            </td>
								            <td>
								            	{{$companyaccount->account_name}} <br>
								            	{{$companyaccount->account_address}} <br>
								            	{{$companyaccount->fkCity->name}} - {{$companyaccount->fkCity->fkCountry->name}}
								            </td>
								            <td>{{$companyaccount->alias}}</td>
								            <td>@if($companyaccount->status == 1) <font color="blue">Active</font> @else <font color="red">Not Active</font> @endif</td>
								        </tr>
							        @endforeach
							    </tbody>
						  	</table>
					  	</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop