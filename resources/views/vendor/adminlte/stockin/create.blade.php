@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">New</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">New Incoming Stock</h4>
			    	</div>
			    	
				  	<div class="box-body">
				      	<form method="post" action="{{ route('stockin.store') }}">
				          	@csrf
				          	<div class="row">
    							<div class="col-md-6">
    								<div class="form-group">
						              	<label for="transaction">Transaction</label>
						              	<select class="form-control select2" name="transaction" required>
						              		<option value="{{ old('transaction') }}">Choose One</option>
					                    	@foreach($transactions as $transaction)
											    <option value="{{ $transaction->id }}">{{ $transaction->code }} - {{ $transaction->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="product">Product</label>
						              	<select class="form-control select2" name="product" required>
						              		<option value="{{ old('product') }}">Choose One</option>
					                    	@foreach($products as $product)
											    <option value="{{ $product->id }}">{{ $product->code }} - {{ $product->name }}</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="warehouse">Warehouse</label>
						              	<select class="form-control select2" name="warehouse" required>
						              		@php ($array = [])
									    	@foreach($employeewarehouses as $employeewarehouse)
									    		@php ($array[] = $employeewarehouse->warehouse)
									    	@endforeach

						              		<option value="{{ old('warehouse') }}">Choose One</option>
					                    	@foreach($warehouses as $warehouse)
					                    		@if(in_array($warehouse->id, $array))
											    	<option value="{{ $warehouse->id }}">{{ $warehouse->code }} - {{ $warehouse->name }}</option>
										    	@endif
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="supplierdetail">Supplier</label>
						              	<select class="form-control select2" name="supplierdetail" required>
						              		<option value="{{ old('supplierdetail') }}">Choose One</option>
					                    	@foreach($supplierdetails as $supplierdetail)
											    <option value="{{ $supplierdetail->id }}">{{ $supplierdetail->name }}</option>
											@endforeach
					                	</select>
						          	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
						              	<label for="date_in">Date</label>
						              	<input type="text" class="form-control datepicker" name="date_in" value="{{ old('date_in') }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="qty_bag">Qty Bag</label>
						              	<input type="number" class="form-control" name="qty_bag" value="{{ old('qty_bag') }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="qty_pcs">Qty Pcs</label>
						              	<input type="number" class="form-control" name="qty_pcs" value="{{ old('qty_pcs') }}">
						          	</div>
						          	<div class="form-group">
						              	<label for="qty_kg">Qty Kg</label>
						              	<input type="number" class="form-control" name="qty_kg" value="{{ old('qty_kg') }}">
						          	</div>
								</div>
							</div>

				          	<button type="submit" class="btn btn-sm btn-success">Save</button>
			          		<a href="{{ url('/stockin') }}" class="btn btn-sm btn-danger">Cancel</a>
				      	</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop