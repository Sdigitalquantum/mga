@extends('adminlte::page')

@section('content')
	<div class="box box-primary">
	  	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
	  		<h4 class="box-title">Monitoring Stock - Per Product</h4>
  		</div>
	  	
	  	<div class="box-body">
		  	<table id="example" class="table table-bordered table-hover nowrap" width="100%">
			    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
			        <tr>
			          	<th width="5%" rowspan="2" style="vertical-align: top; text-align: center;">No</th>
			          	<th width="25%" rowspan="2" style="vertical-align: top;">Product</th>
			          	<th width="10%" rowspan="2" style="vertical-align: top; text-align: center;">Year</th>
			          	<th width="60%" colspan="12" style="text-align: center;">Month</th>
			        </tr>
			        <tr>
			          	<th width="5%" style="text-align: center;">Jan</th>
			          	<th width="5%" style="text-align: center;">Feb</th>
			          	<th width="5%" style="text-align: center;">Mar</th>
			          	<th width="5%" style="text-align: center;">Apr</th>
			          	<th width="5%" style="text-align: center;">May</th>
			          	<th width="5%" style="text-align: center;">Jun</th>
			          	<th width="5%" style="text-align: center;">Jul</th>
			          	<th width="5%" style="text-align: center;">Aug</th>
			          	<th width="5%" style="text-align: center;">Sep</th>
			          	<th width="5%" style="text-align: center;">Oct</th>
			          	<th width="5%" style="text-align: center;">Nov</th>
			          	<th width="5%" style="text-align: center;">Des</th>
			        </tr>
			    </thead>

			    <tbody>
			    	@php ($no = 0)
			    	@foreach($stocks as $stock)
			    		@php ($no++)
			        	<tr>
				            <td style="text-align: center;">{{$no}}</td>
				            <td>[{{$stock->fkProduct->code}}] {{$stock->fkProduct->name}}</td>
				            <td style="text-align: right;">{{$stock->year}} &nbsp;</td>
				            <td>
				            	@foreach($stockcards as $stockcard)
				            		@if($stock->product == $stockcard->product && $stock->year == $stockcard->year)
						            	@if($stockcard->month == 1)
						            		In &nbsp;&nbsp;&nbsp;: {{number_format($stockcard->qty, 0, ',' , '.')}} <br>
						            		Out : {{number_format($stockcard->qty_out, 0, ',' , '.')}}
						            	@endif
					            	@endif
				            	@endforeach
				            </td>
				            <td>
				            	@foreach($stockcards as $stockcard)
				            		@if($stock->product == $stockcard->product && $stock->year == $stockcard->year)
						            	@if($stockcard->month == 2)
						            		In &nbsp;&nbsp;&nbsp;: {{number_format($stockcard->qty, 0, ',' , '.')}} <br>
						            		Out : {{number_format($stockcard->qty_out, 0, ',' , '.')}}
						            	@endif
					            	@endif
				            	@endforeach
				            </td>
				            <td>
				            	@foreach($stockcards as $stockcard)
				            		@if($stock->product == $stockcard->product && $stock->year == $stockcard->year)
						            	@if($stockcard->month == 3)
						            		In &nbsp;&nbsp;&nbsp;: {{number_format($stockcard->qty, 0, ',' , '.')}} <br>
						            		Out : {{number_format($stockcard->qty_out, 0, ',' , '.')}}
						            	@endif
					            	@endif
				            	@endforeach
				            </td>
				            <td>
				            	@foreach($stockcards as $stockcard)
				            		@if($stock->product == $stockcard->product && $stock->year == $stockcard->year)
						            	@if($stockcard->month == 4)
						            		In &nbsp;&nbsp;&nbsp;: {{number_format($stockcard->qty, 0, ',' , '.')}} <br>
						            		Out : {{number_format($stockcard->qty_out, 0, ',' , '.')}}
						            	@endif
					            	@endif
				            	@endforeach
				            </td>
				            <td>
				            	@foreach($stockcards as $stockcard)
				            		@if($stock->product == $stockcard->product && $stock->year == $stockcard->year)
						            	@if($stockcard->month == 5)
						            		In &nbsp;&nbsp;&nbsp;: {{number_format($stockcard->qty, 0, ',' , '.')}} <br>
						            		Out : {{number_format($stockcard->qty_out, 0, ',' , '.')}}
						            	@endif
					            	@endif
				            	@endforeach
				            </td>
				            <td>
				            	@foreach($stockcards as $stockcard)
				            		@if($stock->product == $stockcard->product && $stock->year == $stockcard->year)
						            	@if($stockcard->month == 6)
						            		In &nbsp;&nbsp;&nbsp;: {{number_format($stockcard->qty, 0, ',' , '.')}} <br>
						            		Out : {{number_format($stockcard->qty_out, 0, ',' , '.')}}
						            	@endif
					            	@endif
				            	@endforeach
				            </td>
				            <td>
				            	@foreach($stockcards as $stockcard)
				            		@if($stock->product == $stockcard->product && $stock->year == $stockcard->year)
						            	@if($stockcard->month == 7)
						            		In &nbsp;&nbsp;&nbsp;: {{number_format($stockcard->qty, 0, ',' , '.')}} <br>
						            		Out : {{number_format($stockcard->qty_out, 0, ',' , '.')}}
						            	@endif
					            	@endif
				            	@endforeach
				            </td>
				            <td>
				            	@foreach($stockcards as $stockcard)
				            		@if($stock->product == $stockcard->product && $stock->year == $stockcard->year)
						            	@if($stockcard->month == 8)
						            		In &nbsp;&nbsp;&nbsp;: {{number_format($stockcard->qty, 0, ',' , '.')}} <br>
						            		Out : {{number_format($stockcard->qty_out, 0, ',' , '.')}}
						            	@endif
					            	@endif
				            	@endforeach
				            </td>
				            <td>
				            	@foreach($stockcards as $stockcard)
				            		@if($stock->product == $stockcard->product && $stock->year == $stockcard->year)
						            	@if($stockcard->month == 9)
						            		In &nbsp;&nbsp;&nbsp;: {{number_format($stockcard->qty, 0, ',' , '.')}} <br>
						            		Out : {{number_format($stockcard->qty_out, 0, ',' , '.')}}
						            	@endif
					            	@endif
				            	@endforeach
				            </td>
				            <td>
				            	@foreach($stockcards as $stockcard)
				            		@if($stock->product == $stockcard->product && $stock->year == $stockcard->year)
						            	@if($stockcard->month == 10)
						            		In &nbsp;&nbsp;&nbsp;: {{number_format($stockcard->qty, 0, ',' , '.')}} <br>
						            		Out : {{number_format($stockcard->qty_out, 0, ',' , '.')}}
						            	@endif
					            	@endif
				            	@endforeach
				            </td>
				            <td>
				            	@foreach($stockcards as $stockcard)
				            		@if($stock->product == $stockcard->product && $stock->year == $stockcard->year)
						            	@if($stockcard->month == 11)
						            		In &nbsp;&nbsp;&nbsp;: {{number_format($stockcard->qty, 0, ',' , '.')}} <br>
						            		Out : {{number_format($stockcard->qty_out, 0, ',' , '.')}}
						            	@endif
					            	@endif
				            	@endforeach
				            </td>
				            <td>
				            	@foreach($stockcards as $stockcard)
				            		@if($stock->product == $stockcard->product && $stock->year == $stockcard->year)
						            	@if($stockcard->month == 12)
						            		In &nbsp;&nbsp;&nbsp;: {{number_format($stockcard->qty, 0, ',' , '.')}} <br>
						            		Out : {{number_format($stockcard->qty_out, 0, ',' , '.')}}
						            	@endif
					            	@endif
				            	@endforeach
				            </td>
			            </tr>
		            @endforeach
			    </tbody>

			    <tfoot>
			        <tr>
			          	<td class="noShow"></td>
			          	<td>Product</td>
			          	<td>Year</td>
			          	<td colspan="12" class="noShow"></td>
			        </tr>
			    </tfoot>
		  	</table>
	  	</div>
	<div>
@stop