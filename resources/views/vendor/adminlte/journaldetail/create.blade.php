@extends('adminlte::page')

@section('content')
	<div class="nav-tabs-custom" style="background: #d2d6de !important;">
        <ul class="nav nav-tabs">
          	<li class="disabled"><a>List</a></li>
            <li class="disabled"><a>New</a></li>
            <li class="disabled"><a>Edit</a></li>
            <li class="active"><a href="#tab_new" data-toggle="tab">Detail</a></li>
        </ul>
            
        <div class="tab-content">
            <div class="tab-pane active" id="tab_new">
			    <div class="box box-primary">
			    	<div class="box-header with-border" style="background: #00c0ef !important; font-weight: bold;">
			    		<h4 class="box-title">New Journal Detail &nbsp;<a href="{{ url('journal')}}" class="btn btn-xs btn-success" style="text-decoration: none;">Complete</a></h4>
			    	</div>
			    	
				  	<div class="box-body">
				  		<div class="row">
			          		<div class="col-md-4">
						      	<form method="post" action="{{ route('journaldetail.store') }}">
						          	@csrf
						          	<div class="form-group">
						              	<label for="journal">Journal</label>
						              	<select class="form-control select2" name="journal" required>
						              		@foreach($journals as $journal)
											    <option value="{{ $journal->id }}">{{ $journal->code }} [{{ $journal->name }}]</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						              	<label for="account">Accounting</label>
						              	<select class="form-control select2" name="account" required>
						              		<option value="{{ old('account') }}">Choose One</option>
					                    	@foreach($accounts as $account)
											    <option value="{{ $account->id }}">{{ $account->code }} [{{ $account->name }}]</option>
											@endforeach
					                	</select>
						          	</div>
						          	<div class="form-group">
						          		<label for="debit">&nbsp;</label>
						              	<input type="radio" name="debit" value="1"> Debit &nbsp;&nbsp;&nbsp;
						              	<input type="radio" name="debit" value="0"> Kredit
					              	</div>
					              	<div class="form-group">
						          		<label for="check">&nbsp;</label>
						              	<input type="checkbox" name="status_group" value="1"> Status Group &nbsp;&nbsp;&nbsp;
						              	<input type="checkbox" name="status_record" value="1"> Status Record
					              	</div>						          		
					          		<div class="form-group">
						          		<button type="submit" class="btn btn-sm btn-success">Save</button>
					          		</div>
						      	</form>
		          			</div>

		          			<div class="col-md-8">
		          				<table class="table table-bordered table-hover nowrap" width="100%">
								    <thead style="background: #ff851b !important; font-weight: bold; text-align: center; color: white;">
								        <tr>
								          	<th width="20%">Journal</th>
								          	<th width="20%">Accounting</th>
								          	<th width="40%">Name</th>
								          	<th width="5%">Status</th>
								          	<th width="15%">Action</th>
								        </tr>
								    </thead>

								    <tbody>
								    	@foreach($journaldetails as $journaldetail)
								    		<tr>
									            <td>{{$journaldetail->fkJournal->code}}</td>
									            <td>{{$journaldetail->fkAccount->code}}</td>
									            <td>{{$journaldetail->fkAccount->name}}</td>
												<td style="text-align: center;">
									            	@if($journaldetail->debit == 1) D
									            	@else K
									            	@endif
									            </td>

									            @if($journaldetail->status == 1)
										            <td>
										            	<a href="{{ route('journaldetail.edit',$journaldetail->id)}}" class="btn btn-xs btn-warning">Edit</a> &nbsp;
										            	
										            	<button type="button" class="btn btn-xs btn-danger" data-toggle='modal' data-target=".bs-example-modal{{$journaldetail->id}}">Delete</button>

										            	<form action="{{ route('journaldetail.destroy', $journaldetail->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$journaldetail->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-danger">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Warning</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to delete this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
										            </td>
									            @else
									            	<td>
									            		<button type="button" class="btn btn-xs btn-success" data-toggle='modal' data-target=".bs-example-modal{{$journaldetail->id}}">Activated</button>

									            		<form action="{{ route('journaldetail.destroy', $journaldetail->id)}}" method="post">
										                  	@csrf
										                  	@method('DELETE')
										                  	<div class="modal fade bs-example-modal{{$journaldetail->id}}" tabindex="-1" role="dialog" aria-hidden="true">
							                                    <div class="modal-dialog modal-sm">
							                                        <div class="modal-content">
							                                            <div class="modal-header btn-success">
							                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
							                                                </button>
							                                                <h4 class="modal-title" id="myModalLabel2"><i class="glyphicon glyphicon-info-sign"></i> &nbsp;Confirmation</h4>
							                                            </div>
							                                            <div class="modal-body">
							                                                Are you sure to active this data?
							                                            </div>
							                                            <div class="modal-footer">
							                                                <button type="submit" class="btn btn-success">Yes</button>
							                                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
										                </form>
									            	</td>
									            @endif
									        </tr>
								        @endforeach
								    </tbody>
							  	</table>
	          				</div>
	          			</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
@stop